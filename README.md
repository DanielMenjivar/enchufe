# enchufe
This [modular WordPress plugin and custom theme](https://dmblog.com/enchufe) will mostly benefit WordPress developers looking for [example code](https://dmblog.com/search/code). *Everything is commented extensively.*

It’s no secret that having too many plugins can slow WordPress to a crawl, so by putting everything into a single *modular* plugin, it’s not only much more efficient and quicker to load, it’s also a lot easier to maintain. It’s like having many plugins in one but without having to repeat yourself with any duplicate code. Adding more functionality is as easy as dropping a PHP class into the classes/modules folder and it’ll autoload whenever you need it.

### Included Modules
* **Archives** — generate a JSON feed of archives
* **Cache** — cache WordPress content and send cache headers
* **Comments** — generate comments as standalone HTML, load them via Ajax
* **DM Blog** — miscellaneous fixes and optimizations to WordPress
* **Feed** — change the default RSS feed
* **GeoJSON** — generate a GeoJSON feed of blog posts
* **Headlines** — generate a JSON feed of the latest posts
* **Metabox** — add a metadata box to the post edit screen
* **Minify** — minify HTML to remove whitespace
* **Settings** — add a settings/options page for the plugin to the WordPress admin
* **ShortCodes** — easily add new shortcodes to WordPress using simple HTML+PHP templates
* **ShortLinks** — handles shortlinks without the need of query strings
* **Sitemap** — generate a simple sitemap.xml file whenever a post is modified
* **Subscriptions** — allow people to subscribe to email updates

I hope this helps! I used this plugin and theme on [my blog](https://dmblog.com/) from 2009 up until early 2016. I’m now in the process of creating my own serverless static generator on AWS so I likely won’t be working with WordPress much anymore…