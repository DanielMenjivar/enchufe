<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 */

global $enchufe;

// redirect based on what type of archive was requested
$hash		= '';

if (is_day() OR is_month()) :
	$hash	= get_the_date('Y-m');
elseif (is_year()):
	$hash	= get_the_date('Y');
endif;

$location	= $enchufe->home_url.'#archives/'.$hash;

header('HTTP/1.1 301 Moved Permanently');
header('Location: '.$location);
exit;

// since we called exit above, NOTHING BELOW WILL RUN — it’s only still there for archival pursposes…
get_header();?>

	<section id="main" role="main">
		<h6 class="assistive-text">Main Content</h6><?php

	/* Queue the first post, that way we know
	 * what date we’re dealing with (if that is the case).
	 *
	 * We reset this later so we can run the loop
	 * properly with a call to rewind_posts().
	 */
	if (have_posts()) : the_post();?>

		<header class="page-header">
			<h3 class="archive-title"><?php

				if (is_day()) :
					echo 'Daily archive for '.get_the_date();
				elseif (is_month()) :
					echo 'Monthly archive for '.get_the_date('F Y');
				elseif (is_year()) :
					echo 'Yearly archive for '.get_the_date('Y');
				elseif (is_category()) :
					echo single_cat_title('',FALSE).' Category';
				elseif (is_tag()) :
					echo 'Articles tagged "'.single_tag_title('',FALSE).'"';
				elseif (is_author()) :
					global $post; $article = Article::factory($post);
					echo 'Articles by '.$article->author_name('display');
				else :
					echo 'Blog Archives';
				endif; ?></h3>
		</header><?php

		/* Since we called the_post() above, we need to
		 * rewind the loop back to the beginning that way
		 * we can run the loop properly, in full.
		 */
		rewind_posts();

		/* Run the loop for the archives page to output the posts.
		 * If you want to overload this in a child theme then include a file
		 * called loop-archives.php and that will be used instead.
		 */
		 get_template_part('loop','archive');

		 if (is_author()):?>

		 <a class="metadata" href="<?php echo get_the_author_meta('googleplus');?>?rel=author" rel="me" title="<?php echo $article->author_name('display');
		 	?> on Google Plus"><?php echo $article->author_name();?> on Google+</a><?php
		 endif;
	endif; ?>

	</section>
<?php
	get_sidebar();
	get_footer();