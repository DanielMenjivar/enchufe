<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The template for displaying Search Results pages.
 */

get_header();?>

	<section id="main" role="main">
		<h6 class="assistive-text">Main Content</h6><?php

	/* If there are no posts to display, such as an empty archive page */
	if ( ! have_posts()) :?>

		<h3 class="archive-title">No Articles Found</h3>
		<div id="search-text">
			<p>Sorry, no articles matched your criteria.</p>
			<p>Try finding what you’re looking for on the <a href="/">home</a> page.</p>
		</div><?php
	else : ?>

		<h3 class="archive-title">Search Results</h3>
		<div id="search-text">
			<p>Your search for <mark><?php echo get_search_query();?></mark> returned <strong><?php echo $wp_query->found_posts;?></strong> results.</p>
		</div><?php

		/* Run the loop for the search to output the results.
		 * If you want to overload this in a child theme then include a file
		 * called loop-search.php and that will be used instead.
		 */
		get_template_part('loop','search');
	endif; ?>

	</section>
<?php
	get_sidebar();
	get_footer();