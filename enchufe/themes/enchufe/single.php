<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The Template for displaying all single posts.
 */

/* if we have a redirect_url in our metadata, then just redirect now */
$redirect_url = Article::factory($post)->metadata()->redirect_url();
if (! empty($redirect_url)) :
	wp_redirect($redirect_url);
endif;

get_header();?>

	<section id="main" role="main">
		<h6 class="assistive-text">Main Content</h6><?php

		/* Start our loop */
		if (have_posts()) :

			/* Run the loop for the search to output the results. */
			get_template_part('loop','single');

		endif; // end of the loop. ?>

	</section>
<?php
	get_sidebar();
	get_footer();