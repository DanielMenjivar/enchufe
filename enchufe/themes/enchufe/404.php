<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

			<section id="main" role="main">
				<h6 class="assistive-text">Main Content</h6>
				<article id="post-0" class="post error404 no-results not-found">
					<header class="entry-header">
						<h3 class="entry-title">Not Found</h3>
					</header>
					<div class="entry-content">
						<p>Sorry, the file you requested could not be found.</p>
						<p>Try finding what you’re looking for on the <a href="/">home</a> page.</p>
					</div>
				</article>
			</section>
<?php
get_sidebar();
get_footer();