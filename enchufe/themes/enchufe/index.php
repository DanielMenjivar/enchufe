<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

	<section id="main" role="main">
		<h6 class="assistive-text">Main Content</h6><?php

		// Run the loop to output the posts.
		get_template_part('loop','index');?>

	</section>
<?php
	get_sidebar();
	get_footer();