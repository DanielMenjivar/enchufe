<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * theme functions and definitions
 */

/**
 * Set the content width based on the theme’s design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset($content_width))
{
	$content_width = 614;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
add_action('after_setup_theme',function() {
	// adds RSS feed links to <head> for posts and comments
	add_theme_support('automatic-feed-links');

	// this theme uses post thumbnails
	add_theme_support('post-thumbnails');

	// this theme uses wp_nav_menu() in one location
	register_nav_menus(array('primary' => 'Primary Navigation'));
});

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
add_filter('wp_page_menu_args',function($args) {
	$args['show_home'] = true;
	return $args;
});

/* Register widgetized areas */
/*
add_action('widgets_init',function() {
	register_sidebar(array(
		'name'			=> 'Main Sidebar',
		'id'			=> 'primary-widget-area',
		'description'	=> 'The widget area on the side…',
		'before_widget'	=> '<aside id="%2$s" class="widget">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h3 class="widget-title">',
		'after_title'	=> '</h3>',
	));
});
*/