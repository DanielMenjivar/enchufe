<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 */

if (! $enchufe->is_ajax()):
	get_header();?>
	<section id="main" role="main">
		<h6 class="assistive-text">Main Content</h6><?php
endif;

	if (have_posts()) : while (have_posts()) : the_post();
		$article = Article::factory($post); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<h3 class="entry-title"><?php the_title();?></h3>
				<time class="entry-date metadata" datetime="<?php
					echo $article->post_date()->format('c')?>"><?php echo $article->post_date()->format('l, F j, Y \a\t g:ia T');?></time> 
				<abbr class="published metadata" title="<?php echo $article->post_date()->format('c')?>"><?php echo $article->post_date()->format('r')?></abbr><?php
					// edit_post_link('✻ Edit this entry ✻', '<p>', '</p>');
				?>
			</header>
			<div class="entry-content">
				<?php the_content();

				/*
				if ($article->post_name() == 'about')
				{
					$disclaimer = get_page_by_path('disclaimer');

					echo '<h2>Disclaimer</h2><div class="disclaimer">'.$disclaimer->post_content.'</div>';
				}
				*/ ?>

			</div><?php

			wp_link_pages(array('before' => '<p>Pages: ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			<footer class="entry-meta">
				<?php
					// edit_post_link('✻ Edit this entry ✻', '<p>', '</p>');
				?>
				<abbr class="updated metadata" title="<?php echo $article->modified_date()->format('c');?>"><?php echo $article->modified_date()->format('r');?></abbr> 
				<span class="modified">Last Modified: <time datetime="<?php echo $article->modified_date()->format('c');?>"><?php echo $article->modified_date()->format('F j, Y @ g:i:sa T');?></time></span>
			</footer>
		</article><?php
	endwhile; endif;

if (! $enchufe->is_ajax()):?>
	</section><?php
	get_sidebar();
	get_footer();
endif;