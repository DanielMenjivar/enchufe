<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /about'); }
/**
 * Template Name: Disclaimer Page
 *
 * A custom page template for the disclaimer page (ajax only).
 */

if (! $enchufe->is_ajax()):
	// if not an ajax request, then 302 (temporary) redirect this page to the /about/ page
	wp_redirect(home_url('/about'));
else:
	if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<h3 class="entry-title"><?php the_title();?></h3>
			</header>
			<div class="entry-content">
				<div class="disclaimer">
					<?php the_content(); ?>
				</div>
				<div class="legal">
					<p>
						By continuing to read this article you agree that you have read the above disclaimer in full, you understand 
						that this blog is strictly <strong><em>for entertainment purposes only</em></strong>, and you hereby disclaim 
						and waive any rights and claims you may have against the owner of this blog with respect to any of the opinions 
						expressed herein.
					</p>
					<p id="agree-buttons">
						<button type="submit" name="agree" id="agree" title="Continue to read the article.">Yes, I agree.</button>
						<button type="submit" name="disagree" id="disagree" title="Exit to the home page.">Cancel</button>
					</p>
				</div>
			</div>
			<footer class="entry-meta"></footer>
		</article><?php

	endwhile; endif;
endif;