<?php
/**
 * The template for displaying search forms
 */
global $enchufe;
?>

<form method="get" id="search-form" action="/">
	<fieldset>
		<label for="s" class="assistive-text">Search for:</label>
		<input type="search" value="<?php the_search_query();?>" name="s" id="s" placeholder="Search <?php echo $enchufe->blog_name_lower;?>" />
		<button type="submit" id="search-submit">Search</button>
	</fieldset>
</form>