<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up until <section id="content">
 */

global $enchufe, $post, $paged;

// whether or not to allow robots to index the page
$index = (is_404() OR is_search() OR $paged > 1) ? 'noindex' : 'index';

?><!doctype html>
<html dir="ltr" lang="en-CA">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="robots" content="<?php echo $index;?>,follow">
	<title><?php echo wp_title('•',false,'right');?></title>
	<meta name="og:site_name" content="<?php echo $enchufe->blog_name;?>"><?php

if (is_home()) :?>
	<meta name="og:title" content="<?php echo $enchufe->blog_name;?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="og:description" content="<?php bloginfo('description'); ?>">
	<meta name="og:image" content="https://example.com/a/u/dm.png"><?php
endif;

if (is_single()) :
	// see https://dmblog.com/custom-iphone-icons
	$article = Article::factory($post);?>
	<meta name="og:title" content="<?php echo esc_attr(the_title_attribute('echo=0'));?>">
	<meta name="description" content="<?php echo esc_attr(strip_tags(get_the_excerpt())); ?>">
	<meta name="og:description" content="<?php echo esc_attr(strip_tags(get_the_excerpt())); ?>">
	<meta name="og:image" content="https://example.com<?php echo $article->thumbnail();?>"><?php
endif; ?>
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@twitter">
	<meta name="twitter:creator" content="@twitter">
	<meta name="theme-color" content="#282a30">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
	<link rel="sitemap" type="application/xml" title="Sitemap" href="/sitemap.xml">
	<link rel="shortcut icon" href="/a/u/favicon.ico">
	<link rel="mask-icon" href="/a/u/pin.svg" color="#003c5a">
	<link rel="apple-touch-icon" href="/a/u/apple-touch-icon.png">
	<link rel="stylesheet" href="/a/u/b/b.css"><?php
	// wordpress head
	wp_head(); ?>
</head>
<body <?php
	/*
		// if this is a mobile device, then let’s add the mobile class
		$body_classes	= ($enchufe->is_mobile()) ? 'mobile' : NULL;
	*/
		$body_classes	= null;
	// don’t show our recent posts on the first home page
	$body_classes	= ($paged < 2 AND is_home()) ? $body_classes.' no-recents' : $body_classes;
	body_class($body_classes); ?>>
	<div id="top">
		<header id="main-header">
			<div id="nav-bar">
				<h1><a href="/" rel="home"><?php echo $enchufe->blog_name;?></a></h1><?php

				// start an output buffer to make all of our links relative – we’re starting this here because we don’t want to affect document head, plus we need the first link (in the h1) to be complete to use it in javascript
				$enchufe->minify->start_buffer('relative_links');?>

				<h2><?php bloginfo('description'); ?></h2>
				<nav id="nav-main">
					<h6 class="assistive-text">Main Navigation</h6>
					<span class="skip-link assistive-text"><a href="#content" title="Skip to content">Skip to content</a></span><?php
					/*  Our navigation menu. If one isn’t filled out, wp_nav_menu falls back to wp_page_menu.
					 * The menu assigned to the primary position is the one used. If none is assigned, the menu with the lowest ID is used.
					 */
					wp_nav_menu(array('container' => false,'theme_location' => 'primary'));?>
				</nav>
			</div>
		</header>
		<section id="content">
			<h6 class="assistive-text">Content</h6><?php
			ob_flush();
			flush();?>