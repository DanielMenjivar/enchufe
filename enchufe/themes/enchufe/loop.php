<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/* The loop that displays posts. */

/* Display navigation to next/previous pages when applicable */
if ($wp_query->max_num_pages > 1) :?>

	<nav id="nav-above" class="posts-nav">
		<h6 class="assistive-text">Article navigation</h6>
		<div class="previous"><?php previous_posts_link('<span class="meta-nav">&larr;</span> Newer articles');?></div>
		<div class="next"><?php next_posts_link('Older articles <span class="meta-nav">&rarr;</span>');?></div>
	</nav><?php
endif;

if (is_single()):?>

	<nav id="nav-above" class="posts-nav">
		<h6 class="assistive-text">Article navigation</h6>
		<div class="previous"><?php next_post_link('%link','<span class="meta-nav">&larr;</span> %title');?></div>
		<div class="next"><?php previous_post_link('%link', '%title <span class="meta-nav">&rarr;</span>');?></div>
	</nav><?php
endif;

/* If there are no posts to display, such as an empty archive page */
if ( ! have_posts()) :?>

		<article id="post-0" class="post no-results not-found">
			<header class="entry-header">
				<h3 class="entry-title">No Posts Found</h3>
			</header>
			<div class="entry-content">
				<p>Sorry, no posts matched your criteria.</p>
				<p>Try finding what you’re looking for on the <a href="/">home</a> page.</p>
			</div>
		</article>
<?php endif;

/* initialize some variables */
global $enchufe, $paged;

/*
when there is only one page, $paged = 0, but otherwise, $paged = current page number
we only want to know the first post on the first page
*/
$first_post	= ($paged < 2);

/* Start the Loop. */
while (have_posts()) :
	the_post(); $article = Article::factory($post);

	// initialize our array of classes
	$post_classes	= array();

	// add “first” to the list of classes on our first post
	$post_classes[]	= ($first_post == true) ? 'first' : null;

	// if we have comments, let’s add a has-comments class
	$post_classes[]	= (get_comments_number() > 0) ? 'has-comments' : null;

	// If comments are open or we have at least one comment, load up the comment template
	$post_classes[] = (comments_open() OR get_comments_number() > 0) ? 'load-comments' : null;

	// add the dm_class custom metadata (if there is any) to the list of classes
	$post_classes[] = $article->metadata()->classes();

	// if we have a tweet ID for this post, also include the appropriate class
	$post_classes[] = ($article->metadata()->tweet()) ? 'has-tweet' : null;

	// now actually generate the list of classes to add - array filter removes all of our null/false values
	$post_classes	= implode(' ',array_unique(array_filter($post_classes)));?>

	<article id="post-<?php the_ID();?>" <?php post_class($post_classes);?>>
		<header class="entry-header">
			<a href="<?php echo $article->permalink();?>" title="<?php the_title_attribute();?>" rel="bookmark">
				<div class="post-thumbnail">
					<img src="<?php echo $article->thumbnail();?>" alt="<?php the_title_attribute();?> – Post Thumbnail" />
				</div>
				<div class="title-wrap">
					<h3 class="entry-title"><?php the_title();?></h3>
				</div>
			</a><?php

			// only display the category on posts (not pages)
			if ( get_post_type() == 'post') :?>
				<div class="cat-links"><?php the_category(', ');?></div> <?php
			endif;?>

			<div class="header-meta">
				<time class="entry-date" datetime="<?php echo $article->post_date()->format('c');?>"><?php
					// for single posts, include the weekday
					if (is_single()) :?>
						<span class="weekday"><?php echo $article->post_date()->format('l');?>, </span><?php
					endif;

					echo $article->post_date()->format('F j, Y');

					// for single posts, include the time
					if (is_single()) :?>
						<span class="post-time"> at <?php echo $article->post_date()->format('g:ia T');?></span><?php
					endif;?>
				</time>
				 <abbr class="published metadata" title="<?php echo $article->post_date()->format('c')?>"><?php echo $article->post_date()->format('r')?></abbr> <?php

				// only display the geodata if we have it
				if ($article->is_geotagged()) :?>
					<span class="geo">
						✻ <a class="place" href="//a.tiles.mapbox.com/v3/MAPBOX-MAPID-HERE/page.html#15/<?php
							echo $article->post_meta('geo_latitude').'/'.$article->post_meta('geo_longitude');?>" title="<?php
							echo $article->post_meta('geo_address');?>" rel="external"><?php echo $article->post_meta('geo_address');?> 
							<span class="metadata">(
								<span class="latitude"><?php echo $article->post_meta('geo_latitude');?></span>;
								<span class="longitude"><?php echo $article->post_meta('geo_longitude');?></span>
							)</span>
						</a>
					</span> <?php
				endif;

				// display a link to retweet the original tweet for this post, if it’s a single post with a tweet attached
				if (is_single() AND $article->metadata()->tweet()) :?>
					<span class="retweet-this">
						✻ <a href="https://twitter.com/intent/retweet?tweet_id=<?php echo $article->metadata()->tweet();?>&amp;related=<?php
							the_author_meta('twitter',$article->post_author());?>" rel="external">Retweet</a>
					</span> <?php
				endif;

				// display the author ?>
				<span class="by-author">by 
					<span class="author vcard">
						<a class="url fn n" href="/" rel="author"><?php
							echo $article->author_name('display');?>
						</a>
					</span>
				</span>
			</div><?php

			// our edit link
			// edit_post_link('✻ Edit this entry ✻', '<p class="edit-link">', '</p>');
			?>
		</header><?php

		// only display excerpts for archives and search
		if (is_archive() || is_search() || is_home()) :

			// only display excerpts for posts (not pages)
			if ( ! (is_search() && ($article->post_type()=='page'))) :?>

			<div class="entry-summary">
				<p class="excerpt">
					<a href="<?php echo $article->permalink();?>">
						<?php
							/*
								// instead of loading the post excerpts for posts 2+ on mobile, just link to the title (hidden by css anyways)
								if ($enchufe->is_mobile() AND $first_post == false) :
									the_title();
								else:
									echo strip_tags(get_the_excerpt());
								endif;
							*/
							echo strip_tags(get_the_excerpt()); ?>
					</a>
				</p>
			</div><?php
			endif;
		else:?>

			<div class="entry-content"><?php
				the_content();?>
			</div><?php

		endif;?>

		<footer class="entry-meta"><?php
			// only display this information on single posts
			if (is_single()):?>
				<p class="article-info">
					<a href="<?php echo $article->permalink();?>"><strong><?php the_title();?></strong></a> 
					was published <?php

					// show the date posted and the post’s category
					?>on <time class="entry-date" datetime="<?php echo $article->post_date()->format('c');?>"><?php
						echo $article->post_date()->format('l, F j, Y \a\t g:ia T');?></time> in the <span class="category"><?php
							the_category(', ');?></span> category<?php

					// only display the geodata if we have it
					if ($article->is_geotagged()) :?> and geotagged <a href="https://maps.apple.com/?q=<?php echo urlencode($article->post_meta('geo_address'));?>&amp;ll=<?php
							echo $article->post_meta('geo_latitude').','.$article->post_meta('geo_longitude');?>&amp;z=14" class="place" rel="external"><?php
							echo $article->post_meta('geo_address');?></a><?php
					endif;
					?> by <a href="/" rel="author"><?php echo $article->author_name('display');?></a><?php

					// show the tags list
					?> and tagged <?php the_tags('<span class="tag-links">',', ','</span>.');?>.<?php

					// if we have a tweet for this post, include it
					if ($article->metadata()->tweet()) :
						?> You can <a href="https://twitter.com/statuses/<?php echo $article->metadata()->tweet();
							?>">follow the conversation on Twitter</a>.<?php
					endif;

				?></p><?php
			else:
				// for posts in the loop, show the tag list
				the_tags('<span class="tag-links">Tagged: ',', ','</span>');
			endif;?>

			<p class="metadata">
				Updated on: <abbr class="updated" title="<?php echo $article->modified_date()->format('c');?>"><?php
					echo $article->modified_date()->format('r');?></abbr><br />
				<span class="modified">Last Modified: <time datetime="<?php echo $article->modified_date()->format('c');?>"><?php
					echo $article->modified_date()->format('F j, Y @ g:i:sa T');?></time></span>
			</p><?php

			// our edit link
			// edit_post_link('✻ Edit this entry ✻', '<p class="edit-link">','</p>');

			// display our author info on single posts if a user has filled out their description and this is a multi-author blog
			if (is_single() AND get_the_author_meta('description') AND is_multi_author()) :?>
				<div id="author-info"><div class="avatar-wrap"><?php
					echo get_avatar(get_the_author_meta('user_email'),128); ?></div>

					<h5>About <?php echo $article->author_name();?></h5><?php

					the_author_meta('description');?>

					<p id="author-link"><a href="<?php echo $article->author_url();?>" rel="author">View all posts by <?php
						echo $article->author_name('display');?> &rarr;</a>
					</p>
				</div><?php
			endif;

			// if we have a tweet for this post, show it
			if (is_single() AND $article->metadata()->tweet()) :?>
			<div id="tweet-comments">
				<blockquote class="twitter-tweet tw-align-center" data-cards="hidden">
					<p>
						<?php the_title();?>
						<br /><br />
						<a href="<?php echo $article->permalink();?>"><?php echo $article->permalink();?></a>
					</p>
					&mdash; <?php echo $article->author_name();?> (@<?php the_author_meta('twitter',$article->post_author());?>) 
					<a href="https://twitter.com/<?php
						the_author_meta('twitter',$article->post_author());?>/status/<?php echo $article->metadata()->tweet();?>"><?php
						echo $article->post_date()->format('F j, Y');?>
					</a>
				</blockquote>
			</div><?php
			endif;
			 ?>
		</footer>
	</article><?php

	// FOR TESTING!
	/* 	comments_template('',true); */

	// now let’s not forget to say this isn’t our first post
	$first_post = FALSE;
endwhile;

if (is_home()) :?>
	<section id="archives"><span id="load-archives"><a class="button" href="#">More…</a></span></section><?php
endif;

/* Display navigation to next/previous pages when applicable */
if ($wp_query->max_num_pages > 1) :?>

	<nav id="nav-below" class="posts-nav">
		<h6 class="assistive-text">Article navigation</h6>
		<div class="previous"><?php previous_posts_link('<span class="meta-nav">&larr;</span> Newer articles');?></div>
		<div class="next"><?php next_posts_link('Older articles <span class="meta-nav">&rarr;</span>');?></div>
	</nav><?php
endif;

if (is_single()):?>

	<nav id="nav-below" class="posts-nav">
		<h6 class="assistive-text">Article navigation</h6>
		<div class="previous"><?php next_post_link('%link','<span class="meta-nav">&larr;</span> %title');?></div>
		<div class="next"><?php previous_post_link('%link', '%title <span class="meta-nav">&rarr;</span>');?></div>
	</nav><?php
endif;