<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /subscribe'); }
/**
 * Template Name: Subscribe Page
 *
 * A custom page template for the subscribe page (ajax only).
 *
 */
global $enchufe;
$content = '';

// VERIFY A SUBSCRIBER
if (isset($_GET['verify'])):
	// verify our subscriber
	$verified_email = $enchufe->subscriptions->verify($_GET['verify']);

	if ($verified_email):
		$content = '<h4>Email Subscription Confirmed!</h4>
					<p>A message will be delivered to <strong>'.$verified_email.'</strong> whenever there’s new content on '.$enchufe->blog_name.'.</p>';
	else:
		// redirect the user to the verify error page so that we can track this better
		wp_redirect(home_url('/subscribe/?verify_error='.$_GET['verify']));
	endif;

// VERIFICATION FAILED
elseif (isset($_GET['verify_error'])) :
	$content = '<h4>Email Subscription Error!</h4>
				<p class="error">Sorry, your email subscription could not be verified.</p>
				<p>You can try <a href="#subscribe">subscribing again</a> or <a href="mailto:%22'.str_replace(' ','%20',$enchufe->blog_name_lower).'%22%20%3C'
					.get_option('admin_email').'%3E?subject=Subscription%20Verification%20Error">email me</a> with details so I can fix the problem. Thanks!</p>';

// CANCEL A SUBSCRIBER (VERIFY CANCELLATION)
elseif (isset($_GET['cancel'])):
	// get our subscribers email address
	$guid			= esc_attr(sanitize_text_field($_GET['cancel']));
	$subscriber		= $enchufe->subscriptions->get_subscriber_by_guid($guid);
	$email_address	= ($subscriber != NULL) ? '<strong>'.$subscriber->email.'</strong>' : 'yourself';

	// ask subscriber to confirm that they want to cancel
		$content = '<h4>Cancel Email Subscription</h4>
					<p>Are you sure you want to unsubscribe '.$email_address.' from '.$enchufe->blog_name_lower.'?</p>
					<form action="/subscribe" method="post" id="cancel-sub-form">
						<fieldset>
							<p>
								<input type="hidden" value="'.$guid.'" name="cancel">
							</p>
							<button type="submit" name="submit" id="submit">Yes, unsubscribe me now.</button>
						</fieldset>
					</form>';

// CANCEL A SUBSCRIBER (VERIFIED)
elseif (isset($_POST['cancel']) AND $enchufe->is_ajax()):
	// cancel our subscriber
	$cancelled	= $enchufe->subscriptions->cancel($_POST['cancel']);
	
	if ($cancelled):
		$content = '<h4>Email Subscription Cancelled</h4>
					<p>You have successfully unsubscribed <strong class="email">'.$cancelled.'</strong> from '
					.$enchufe->blog_name_lower.' email updates. <strong><em>We’re sorry to see you go!</em></strong></p>
					<p>You can <a href="#subscribe">subscribe again</a> at any time if you change your mind.</p>';
	else:
		$content = '<h4>Email Subscription Cancellation Failed!</h4>
					<p class="error">Sorry, your email subscription could not be cancelled.</p>
					<p>Please <a href="mailto:%22'.str_replace(' ','%20',$enchufe->blog_name_lower).'%22%20%3C'.get_option('admin_email')
						.'%3E?subject=Subscription%20Cancellation%20Error">email me</a> with details so I can fix the problem. Thanks!</p>';
	endif;

	// since this is an ajax submitted form, we just want to return the minified content
	echo $enchufe->minify->minify_html($content);
	return;

// ADD A NEW SUBSCRIBER
elseif (isset($_POST['email']) AND $enchufe->is_ajax()):
	// check our email address first
	$email = is_email(trim($_POST['email']));

	if (! $email):
		// invalid email message
		$content = '<h4>Error!</h4>
					<p class="error">Sorry, the email address you have entered appears to be invalid. Please enter a working email address.</p>';
	else:
		// add our subscriber
		$added	 = $enchufe->subscriptions->add($email);

		if ($added):
			$content = '<h4>Email Subscription Requested</h4>
						<p>Please check your <strong class="email">'.$added.'</strong> inbox for a verification message from '.$enchufe->blog_name_lower.'. You will need to click a link in that message to verify your email address and activate your subscription. If you don’t get a confirmation email within the next five minutes, please check your junk/spam folder.</p>
						<p>Thanks!</p>';
		else:
			$content = '<h4>Email Subscription Failed!</h4>
						<p class="error">Sorry, your subscription request could not be completed.</p>
						<p>Please verify your email address and try again, or <a href="mailto:%22'
							.str_replace(' ','%20',$enchufe->blog_name_lower).'%22%20%3C'.get_option('admin_email')
							.'%3E?subject=New%20Subscription%20Error">email me</a> with details so I can fix the problem. Thanks!</p>';
		endif;
	endif;

	// since this is an ajax submitted form, we just want to return the minified content
	echo $enchufe->minify->minify_html($content);
	return;

// DEFAULT PAGE CONTENT - AJAX ONLY
elseif ($enchufe->is_ajax()):
			$email_address = (isset($_POST['email'])) ? esc_attr($_POST['email']) : '';
			$content = '<div id="email-subscriptions">
							<div id="sub-text">
								<h4>Email Updates</h4>
								<p>Get an email whenever there’s new content on '.$enchufe->blog_name.'. In 2015, subscribers received around <em>one email per month</em> and never more than one per week. You can easily unsubscribe at any time if you change your mind.</p>
							</div>
							<form action="/subscribe" method="post" id="subscribe-form">
								<fieldset>
									<p>
										<label for="email">Email address:</label>
										<input type="email" name="email" id="email" value = "'.$email_address.'" size="30" />
									</p>
									<button type="submit" name="submit" id="submit">Subscribe</button>
								</fieldset>
							</form>
						</div>
						<h4>Blog Feeds</h4>
						<p>Follow new content on '.$enchufe->blog_name.' through the <a href="/feed">RSS feed</a> or by following <a href="https://twitter.com/intent/user?user_id='.$enchufe->settings->get('twitter.twitter_id').'">@twitter on Twitter</a>.</p>
						<div id="feeds">
							<a id="posts-feed" href="/feed"><span>Main </span>RSS Feed</a>
							<a id="twitter-feed" href="https://twitter.com/intent/user?user_id='.$enchufe->settings->get('twitter.twitter_id').'"><span>Blog Posts </span>Twitter Feed</a>
						</div>';

// DEFAULT ACTION - NO AJAX MEANS REDIRECT
else:
	// if not an ajax request, then 302 (temporary) redirect this page to the home page with the #subscribe hash
	wp_redirect(home_url('/#subscribe'));
endif;

if (! $enchufe->is_ajax()):
	get_header();?>
	<section id="main" role="main">
		<h6 class="assistive-text">Main Content</h6><?php
endif;

if (have_posts()):
	while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<h3 class="entry-title"><?php the_title();?></h3><?php
				// edit_post_link('✻ Edit this entry ✻', '<p>', '</p>');
				?>
			</header>
			<div class="entry-content"><?php
				echo $content;
				if ($enchufe->is_ajax()):
					the_content();
				endif;?>
			</div>
			<footer class="entry-meta">
				<?php
				// edit_post_link('✻ Edit this entry ✻', '<p>', '</p>');
				?>

			</footer>
		</article><?php
	endwhile;
endif;

if (! $enchufe->is_ajax()):?>
	</section><?php
	get_sidebar();
	get_footer();
endif;