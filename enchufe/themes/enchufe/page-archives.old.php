<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /archives'); }
/**
 * Template Name: Archives
 *
 * A custom page template for the archives page.
 * 
 * See https://dmblog.com/wordpress-tag-cloud-with-post-counts
 */

get_header();?>

	<section id="main" role="main">
		<h6 class="assistive-text">Main Content</h6><?php

	if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('archives-list'); ?>>
			<header class="entry-header">
				<h3 class="entry-title"><?php the_title();?></h3><?php
				edit_post_link('✻ Edit this entry ✻', '<p>', '</p>');?>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<nav id="archives-nav">
				<ul>
					<li class="best-of"><a href="#archives-best">Best Of</a></li> 
					<li class="best-of"><a href="#archives-map">Locations</a></li> 
					<li><a href="#archives-monthly">Monthly</a></li> 
					<li><a href="#search">Search</a></li>
				</ul>
			</nav>
			<div id="archives-best"><?php

				// get all our featured posts
				$args	= array(
					'numberposts'	=> 99999,
					'meta_key'		=> $enchufe->metabox->featured_meta
				);
				$posts	= get_posts($args);

				// only display content if we have posts
				if ($posts) :?>
					<h2>Best of <?php echo $enchufe->blog_name;?></h2>
					<ul class="archives best"><?php

					// loop through all our posts to display them
					foreach ($posts as $post):
						$article = Article::factory($post);?>

						<li>
							<a href="<?php echo $article->permalink();?>" title="<?php echo esc_attr($article->post_title());?>" rel="bookmark">
								<div class="post-thumbnail">
									<img src="<?php echo $article->thumbnail();?>" alt="<?php echo esc_attr($article->post_title());?> – Article Thumbnail" />
								</div>
								<h4><?php echo $article->post_title();?></h4>
								<time class="entry-date" datetime="<?php echo $article->post_date()->format('c');?>"><?php
									echo $article->post_date()->format('F j, Y');?></time>
							</a>
						</li><?php

					endforeach;?>

					</ul><?php
				endif;?>
			</div>
			<div id="archives-map">
				<h4>Locations</h4>
				<div id="archives-map-container"></div>
			</div>
			<div id="archives-left">
				<h4 id="archives-monthly">Monthly</h4>
				<ul class="archives years"><?php
					// get our months as a string of links in the format "<a href='/2011/12' title='December 2011'>December 2011</a>&nbsp;(1)"
					$months	= wp_get_archives('type=monthly&show_post_count=1&echo=0&format=custom');

					// convert our string to an array
					$months	= explode(chr(10).'	',trim($months));

					// initialize some variables
					$current_year	= 0;
					$first			= ' class="expand"';

					// loop through our months
					foreach ($months as $month):
						// first let’s get our count for this month
						$count	= trim(substr($month,strpos($month,'(')),'()');

						// for months with only one post, append a single class
						$class	= ($count == 1) ? ' class="single"' : '';

						// convert each month into an xml object
						$xml	= simplexml_load_string(substr($month,0,strpos($month,'&')));
						$month	= (string) $xml[0];

						// get our year
						$year	= date('Y',strtotime($month));

						// if this is a new year
						if ($year != $current_year) :
							// if it’s not the first year, we can clear our “first” class and close the current year first
							if ($current_year != 0) :
								// reset our “first” class
								$first = '';

								// now close the previous year first ?>
									</ul>
								</li><?php
							endif;

								// now create the new year ?>
								<li id="<?php echo $year;?>"<?php echo $first;?>>
									<h5><a href="#<?php echo $year;?>">&rarr; <?php echo $year;?></a></h5>
									<ul class="archives months"><?php
						endif;

						// now output the month?>
										<li><a href="<?php echo $xml->attributes()->href;?>"><?php echo $month;
											?></a> <span<?php echo $class;?>><?php echo $count;?></span></li><?php

						// change our current year now
						$current_year = $year;
					endforeach;?>
									</ul>
								</li>
				</ul>
			</div>
			<div id="archives-right">
				<h2 id="archives-categories">Categories</h2>
				<ul class="archives categories">
				<?php
					// get our categories as a string with list items
					// "<li class="cat-item cat-item-3"><a href="/category/blog/" title="Blog Articles">Blog</a> (177)</li>
					$categories	= wp_list_categories('show_count=1&echo=0&title_li=');

					// now replace the brackets with <span> tags
					$categories	= strtr($categories,array(
						'('				=> '<span>',
						')'				=> '</span>',
						chr(10).'</li>'	=> '</li>'
					));

					echo $categories;?>
				</ul>
				<h2 id="archives-authors">Authors</h2>
				<ul class="archives authors"><?php
					// get our authors from the database
					global $wpdb;
					$authors = $wpdb->get_results("SELECT DISTINCT COUNT($wpdb->posts.ID) as count, user_email as email, user_nicename as slug, display_name as name FROM $wpdb->posts INNER JOIN $wpdb->users ON $wpdb->posts.post_author = $wpdb->users.ID WHERE post_type = 'post' and post_status = 'publish' GROUP BY post_author;");

					// loop through our authors to generate the HTML
					foreach ($authors as $author):?>
						<li>
							<a href="/author/<?php echo $author->slug;?>" title="Articles by <?php echo $author->name;?>">
								<?php echo get_avatar($author->email,48) . $author->name;
							?></a> 
							<span><?php echo $author->count;?></span>
						</li><?php
					endforeach;?>
				</ul>
			</div>
			<div id="search">
				<h4>Search</h4><?php
				get_search_form();?>
			</div>
			<footer class="entry-meta"><?php
				edit_post_link('✻ Edit this entry ✻', '<p>', '</p>');?>
			</footer>
		</article><?php

	endwhile; endif;?>

	</section>
<?php
	get_sidebar();
	get_footer();