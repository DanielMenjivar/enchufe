<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /archives'); }
/**
 * Template Name: Archives
 *
 * A custom page template for the archives page.
 */

get_header();?>

	<section id="main" role="main">
		<h6 class="assistive-text">Main Content</h6><?php

	if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('archives-list'); ?>>
			<header class="entry-header">
				<h3 class="entry-title"><?php the_title();?></h3>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<div id="archives-all"><?php
				$posts			= get_posts('numberposts=999');
				$year_month		= '';
				$year			= '';
				$month			= '';
				$close_month	= '';
				$index			= 0;
				$lazy_load_threshold = $enchufe->settings->get('media.lazy_load');

				foreach ($posts as $post):
					// let’s use our article object…
					$article	= Article::factory($post);

					// lazy load our post thumbnails
					if ($index >= $lazy_load_threshold):
						$lazy_class = ' lazy-load';
						// $img_src	= $enchufe->upload_url_path.'/article_thumb.png';
						$img_src	= '/a/u/dm.png';
						$data_src	= '" data-src="'.$article->thumbnail();
					else:
						$lazy_class = '';
						$img_src	= $article->thumbnail();
						$data_src	= '';
					endif;

					// if our year and month don’t match
					if ($article->post_date()->format('Y-m') != $year_month):
						$year_month	= $article->post_date()->format('Y-m');

						// if the year doesn’t match
						if ($article->post_date()->format('Y') != $year):
							$year	= $article->post_date()->format('Y');

							echo $close_month;?>

							<h4 id="<?php echo $year;?>"><?php echo $year;?></h4><?php

							$close_month	= ''; // since we already closed it
						endif;

						// otherwise, it’s just the month that doesn’t match
						$month		= $article->post_date()->format('F Y');

						echo $close_month;
						$close_month	= '				</ul>';?>

						<h5 id="<?php echo $year_month;?>"><?php echo $month;?></h5>
						<ul><?

					endif;?>

					<li>
						<a href="<?php echo $article->permalink();?>">
							<div class="post-thumbnail<?php echo $lazy_class;?>">
								<img src="<?php echo $img_src.$data_src;?>" alt="<?php
									echo esc_attr($article->post_title());?>" />
							</div>
							<h6><?php echo $article->post_title();?></h6>
						</a>
					</li><?php
					$index++;
				endforeach;
				echo $close_month;?>
			</div>
		</article><?php
	endwhile; endif;?>
	</section><?php
get_sidebar();
get_footer();