<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * The template for displaying Comments.
 *
 * see https://dmblog.com/html5-threaded-comments
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by the main enchufe plugin.
 */

	/* don’t show comments if the post is password protected */
	if (post_password_required()):?>

	<p class="no-password">This article is password protected. Enter the password to view any comments.</p><?php
		/* Stop the rest of comments.php from being processed,
		 * but don’t kill the script entirely — we still have
		 * to fully load the template.
		*/
		return;
	endif;

	/* if we have comments */
	if (have_comments()) :?>

	<section id="comments">
		<h4 id="comments-title"><?php comments_number('Be the first to submit a comment on ','One response to ','% Responses on '); the_title();?></h4><?php

		// if we have comments pages to navigate through (probably not)
		if (get_comment_pages_count() > 1 && get_option('page_comments')) :?>

			<nav id="nav-comments-above" role="navigation">
				<h6 class="assistive-text">Comment navigation</h6>
				<div class="previous"><?php previous_comments_link('<span class="meta-nav">&larr;</span> Older comments');?></div>
				<div class="next"><?php next_comments_link('Newer comments <span class="meta-nav">&rarr;</span>');?></div>
			</nav><?php
		endif;

		// Loop through and list the comments. ?>
		<ol class="comment-list"><?php
			wp_list_comments(array(
				'style'			=> 'ol',
				'callback'		=> function($comment, $args, $depth) { // use this anonymous function for displaying our comments
					$GLOBALS['comment'] = $comment;
					switch ($comment->comment_type) :
						case 'pingback' :
						case 'trackback':?>
							<li <?php comment_class();?> id="li-comment-<?php comment_ID();?>">
								<article id="comment-<?php comment_ID();?>">
									<header class="comment-meta">
										<div class="comment-author"><?php echo ucwords($comment->comment_type);?>: <cite><?php comment_author_link(); ?></cite></div>
										<span class="comment-time"><time datetime="<?php
											comment_date('c');?>"><?php echo get_comment_date().' at '.get_comment_time();?></time></span><?php
										edit_comment_link('✻ Edit ✻',' <span class="edit-link">','</span>');?>
									</header>
								</article><?php
							break;
						default:?>
							<li <?php comment_class();?> id="li-comment-<?php comment_ID();?>">
								<article id="comment-<?php comment_ID();?>">
									<header class="comment-meta">
										<div class="comment-author vcard">
											<div class="avatar-wrap">
												<?php echo get_avatar($comment,96);?>
											</div>
											<div class="author"><cite class="fn"><?php comment_author_link();?></cite></div>
										</div>
										<span class="comment-time"><a href="<?php echo esc_url(get_comment_link($comment->comment_ID));?>"><time datetime="<?php
											comment_date('c');?>"><?php echo get_comment_date().' at '.get_comment_time();?></time></a>
										</span>
										<?php edit_comment_link('✻ Edit ✻',' <span class="edit-link">','</span>');?>
									</header><?php
										// if the comment is not yet approved
										if ($comment->comment_approved == '0') :?>
											<p class="pending-comment">Your comment is awaiting moderation. 
												<span>You can follow the <a href="/comments/feed">comments feed</a> for comment updates.</span>
											</p><?php
										endif;
									/* comment text */?>
									<section class="comment-content"><?php comment_text();?></section>
									<footer id="reply-<?php comment_ID();?>" class="reply"><?php
										comment_reply_link(array_merge($args,array(
											'reply_text' => 'Reply to '.get_comment_author().' <span>&darr;</span>',
											 'add_below' => 'reply', 'depth' => $depth,'max_depth' => $args['max_depth']
										)));?>
									</footer>
								</article><?php
						break;
					endswitch;
			}));?>
		</ol><?php

		// if we have comments pages to navigate through (probably not)
		if (get_comment_pages_count() > 1 && get_option('page_comments')) :?>

			<nav id="nav-comments-below">
				<h6 class="assistive-text">Comment navigation</h6>
				<div class="previous"><?php previous_comments_link('<span class="meta-nav">&larr;</span> Older comments');?></div>
				<div class="next"><?php next_comments_link('Newer comments <span class="meta-nav">&rarr;</span>');?></div>
			</nav><?php
		endif;?>

	</section><?php
		/* If comments are closed, let’s leave a note.
		 * But we only want the note on posts and pages that had comments in the first place.
		 */
		if ( ! comments_open() AND get_comments_number()) :?>
	<p class="no-comments">Comments are now closed for this article.</p><?php
		endif;
	endif;

/* include the comments form */
if (comments_open()) :?>

	<div id="respond">
		<header>
			<h5 id="reply-title"><?php comment_form_title();?></h5>
			<span class="cancel-comment-reply"><?php cancel_comment_reply_link();?></span>
		</header><?php

	if (get_option('comment_registration') && !is_user_logged_in()) :?>

		<p>You must be <a href="<?php echo wp_login_url(get_permalink());?>">logged in</a> to submit a comment.</p><?php

	else :?>

		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform"><fieldset>
			<p class="comment-form-comment">
				<label for="comment">Comment</label>
				<textarea name="comment" id="comment" cols="45" rows="8" placeholder="Enter your comment here…" required></textarea>
				<span class="comments-message">Your comments can contain code and may be edited for spelling, grammar and formatting.</span>
			</p>
			<div class="comment-form-fields"><?php

		if (is_user_logged_in()) :
			global $user_email;
			echo '<div class="comment-form-avatar">'.
					get_avatar($user_email,96).
				 '</div>';?>
				<p class="logged-in-as">Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. 
					<a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p><?php
		else :?>
				<p class="comment-form-author">
					<label for="author">Name<?php if ($req) echo ' <span class="required">(required)</span>';?></label>
					<input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="30" <?php if ($req) echo "required"; ?> />
				</p>
				<div class="comment-form-avatar">
					<a href="https://gravatar.com/site/signup/" title="This picture will show whenever you leave a comment. Click to customize it." rel="external">
						<?php echo get_avatar(NULL,64);?>
					</a>
				</div>
				<p class="comment-form-email">
					<label for="email">Email<?php if ($req) echo ' <span class="required">(required)</span>';
						?><span class="nopublish">(Never made public)</span>
					</label>
					<input type="email" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="30" <?php if ($req) echo "required"; ?> />
				</p>
				<p class="comment-form-url">
					<label for="url">Website</label>
					<input type="url" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="30" />
				</p>
				<div id="comment-form-subscriptions">
					<p class="comment-form-subscribe">
						<input type="checkbox" name="subscribe" id="comment-subscribe" value="subscribe" checked />
						<label for="comment-subscribe">Notify me of new articles via email.</label>
					</p>
				</div><?php
		endif; ?>
				<button type="submit" name="submit" id="submit">Submit Comment</button>
				<?php comment_id_fields();
				do_action('comment_form', $post->ID); ?>
			</div>
		</fieldset></form><?php
	endif;?>

	</div><?php
endif;