<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main section, the page footer and the closing of id=top div as well as javascripts to include last.
 *
 */

global $enchufe; ?>
		</section>
		<footer id="main-footer">
			<p>
				<span id="back-to-top"><a href="#top">back to top &uarr;</a></span>
				<small>&copy; 2004 – <?php echo date('Y');?> <a href="/"><?php echo $enchufe->blog_name;?></a> — All Rights Reserved.</small>
			</p>
		</footer>
	</div>
	<script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
	<script defer src="/a/u/b/b.js"></script>

	<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	 wp_footer(); ?>
</body>
</html>