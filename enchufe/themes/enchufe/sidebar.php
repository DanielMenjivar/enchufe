<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The Sidebar containing the widget area
 */ ?>

<section id="side">
	<h6 class="assistive-text">Supplementary Content</h6><?php

	/* When we call the dynamic_sidebar() function, it’ll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn’t exist, so we’ll hard-code in
	 * some default sidebar stuff just in case.
	*/

	if (! dynamic_sidebar('primary-widget-area')):
		// ONLY if the sidebar doesn’t exist, or has no widgets, we’ll show this by default…
	endif;

	// include placeholders for the ads that will be filled with javascript ?>
	<aside id="itunes-ad" class="widget">
		<h6 class="assistive-text">Ad for iTunes Music</h6>
	</aside>
</section>