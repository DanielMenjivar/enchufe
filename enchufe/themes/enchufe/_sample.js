// see also https://dmblog.com/html5-audio-with-jquery

// ! create these variables first so we don't get errors later
window.___fourSq = {'uid':'55555555',"secure":true};
var _gas	= 'send',
	_gae	= 'event',
	_gapv	= 'pageview',
	_gap	= 'UA-555555555-5',
	_ds		= 'ga-disable-' + _gap,
	hash	= window.location.hash;

/* now run our jquery stuff in here */
$(document).ready(function() {
	// ! var dmblog – create some variables and methods that we can use later
	var dmblog	= {
		homeURL			: '',
		isMobile		: false,
		cdn				: '//example.com/path/',
		months			: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		// ! dmblog.templates
		templates		: {
			quicktime		: '<object width="{width}" height="{height}" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab"><param name="src" value="{source}"><param name="controller" value="true"><param name="kioskmode" value="true"><param name="showlogo" value="false"><param name="autoplay" value="true"><embed width="{width}" height="{height}" src="{source}" type="{type}/mp4" controller="true" kioskmode="true" showlogo="false" autoplay="true" pluginspage="http://www.apple.com/quicktime/" /></object>',
			html5audio		: '<audio controls><source src="{source}.m4a" type="audio/mp4" /><source src="{source}.oga" type="audio/ogg" /></audio>',
			html5video		: '<video controls width="{width}" height="{height}" poster="{source}.jpg"><source src="{source}.mp4" type="video/mp4" /><source src="{source}.ogv" type="video/ogg" /></video>',
			youtube			: '//www.youtube.com/embed/{id}?html5=1&autohide=1&autoplay=1&showinfo=0&rel=0&hd=1',
			dinesafe		: {
				json		: 'http://app.toronto.ca/DineSafe/getEstablishmentDetails.json?callback=?',
				details			: '<div class="dm-ajax"><article class="page dinesafe"><div class="dinesafe-details"><span class="type">{type}</span><span class="minimum">Minimum number of inspections per year: {minimum}</span><div class="dinesafe-inspections">{inspections}</div><div class="permalink"><a href="http:{permalink}" class="button" rel="external">View More Details on DineSafe</a></div></div></article></div>',
				inspections : {
					noinfo		: '<p>There is currently no information for this establishment.</p>',
					status		: [0,'pass','conditional','closed'],
					severity	: ['Municpal Code','Crucial','Significant','Minor'],
					inspection	: '<li class="{class}"><span class="date">{date}</span><span class="status">{status}</span>{infractions}</li>',
					infraction	: '<li><span class="severity">{severity}</span><span class="action">{action}</span><span class="details">{details}</span></li>'
				}
			},
			recentPosts	: {
				widget		: '<aside id="recent-posts"><h3>Most Recent Articles</h3><ul>{posts}</ul></aside>',
				post		: '<li><a href="{permalink}" title="{title}" rel="bookmark"><div class="post-thumbnail"><img src="{thumb}" alt="{title}" /></div><h4>{title}</h4><time class="entry-date" datetime="{datetime}">{date}</time></a></li>'
			},
			comments	: {
				loading		: '<section id="loading-comments"><p>...loading comments...</p></section>',
				status		: '<div id="comment-status"></div>',
				processing	: '<p>Processing comment...</p>',
				error		: '<p class="error"><strong>Error:</strong> You might have left one of the fields blank, or be posting too quickly.</p>',
				pending		: '<p class="pending-comment">Your comment is awaiting moderation. <span>You can follow the <a href="/comments/feed">comments feed</a> for comment updates.</span></p>',
				wait		: '<p class="error">Please wait a while before posting your next comment. Thanks!</p>'
			},
			subscriptionErrors	: {
				subscribe	: '<h2>Email Subscription Failed!<h2><p class="error">Sorry, there was an error processing your subscription request.</p>',
				cancel		: '<h2>Email Subscription Cancellation Failed!</h2><p class="error">Sorry, there was an error processing your cancellation request.</p>'
			},
			mapbox		: {
				singleMap		: '<div id="single-post-map" style="min-width:250px;min-height:135px;"></div>',
				locationTitle	: '<h1 class="marker-title">{title}</h1>',
				description		: '<div class="marker-description{single}"><ul>{posts}</ul></div>',
				post			: '<li><a href="{permalink}" title="{title}"><div class="post-thumbnail"><img src="{thumb}" alt="{title} – Article Thumbnail" /></div><h4>{title}</h4><span class="entry-date">{date}</span></a></li>'
			},
			iTunesAd	: '<iframe src="//banners.itunes.apple.com/banner.html?partnerId=&aId=______&bt=promotional&at=Music&st=apple_music&c=ca&l=en-GB&w={width}&h={height}&rs=1" frameborder=0 style="overflow-x:hidden;overflow-y:hidden;width:{width}px;height:{height}px;border:0"></iframe>'
		},
		// ! dmblog.defaults
		defaults		: {
			scripts			: {
				twitter			: '//platform.twitter.com/widgets.js',
				foursquare		: '//platform-s.foursquare.com/js/widgets.js',
				md5				: '//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js',
				code			: '//example.com/js/c.js'	// prism.js
			},
			mapbox			: {
				mapID			: 'map',
				mapContainer	: '#archives-map-container',
				mapboxID		: 'MAPBOX-MAPID-HERE',
				retinaID		: 'MAPBOX-RETINA-MAPID-HERE',
				stylesheet		: '//api.tiles.mapbox.com/mapbox.js/v1.3.1/mapbox.css',
				script			: '//api.tiles.mapbox.com/mapbox.js/v1.3.1/mapbox.js'
			},
			fancybox	: {
				autoResize	: true,
				autoCenter	: true
			}
		},
		// ! dmblog.init
		init			: function () {
			// save the value of our home URL
			this.homeURL = $('#main-header h1 a').attr('href');

			// is this a mobile browser?
			this.isMobile = ($('#side').width() == 320);

			// change the default getScript method to add a third parameter - whether to allow caching or not (default/blank means no)
			$.getScript = function(url, callback, cache) {
				$.ajax({
					type	: 'GET',
					url		: url,
					success	: callback,
					dataType: 'script',
					cache	: cache
				});
			};

			// add a lazy load function to jquery to lazy load our images see https://github.com/luis-almeida/unveil
			$.fn.lazyLoad = function(threshold) {
				var $w		= $(window),
					images	= this,
					loaded,
					visible,
					source;

				this.one('lazyLoad', function() {
					// get our source from the data-source attribute and if it exists, set the src attribute to this
					source = $(this).data('src');
					if (source) {
						$(this).attr('src',source);
						$(this).parents('figure').removeClass('lazy-load');
					}
				});

				function lazyLoad() {
					// determine which images in the collection are visible
					visible = images.filter(function() {
						return dmblog.isVisible(this,threshold);
					});

					// lazy load our visible images and set our collection to not include these anymore
					loaded	= visible.trigger('lazyLoad');
					images	= images.not(loaded);
				}

				// bind our lazyLoad to the scroll and resize window events
				$w.scroll(lazyLoad);
				$w.resize(lazyLoad);

				lazyLoad();

				return this;
			};

			// change some default settings for fancybox
			var fancyboxDefaults		= this.defaults.fancybox,
				dimensions				= {
					// set our minimum height lower (for audio tracks to load properly - it needs to be higher for audio tracks)
					minHeight	: (this.isMobile) ? 45 : 25,

					// increase our top margin to account for our main navigation bar, which is always visible
					margin		: (this.isMobile) ? [30+25,20,15,20] : [40+25,20,20,20],
					padding		: (this.isMobile) ? 5 : 15
				};
			// add our dimensions properties to the defaults
			$.extend(fancyboxDefaults,dimensions);

			// add our new defaults to fancybox
			$.extend($.fancybox.defaults, fancyboxDefaults);
		},
		// ! dmblog.fancyContent
		fancyContent	: function (page,modal) {
			/* http://api.jquery.com/jQuery.get/ */
			$.get(page,function(data) {
				var options = {
					wrapCSS	: 'fancybox-transparent',
					padding : 0,
					modal	: modal,
					content	: '<div class="dm-ajax">'+data+'</div>'
				};

				// if this should be modal, then also change the overlay
				options.helpers = (modal) ? {
					overlay : {
						css : { 'background' : 'rgb(0,0,0)' }
					}
				} : {};

				// if this is for the subscribe page then also include a callback to process our subscription form submissions
				if (data.indexOf('subscribe-form') > -1)
				{
					options.beforeShow = dmblog.submitSubscribe;
				}
				$.fancybox(options);
			});
		},
		// ! dmblog.formatDate
		formatDate		: function (input) {
			var parts	= input.match(/(\d+)/g),
				date	= new Date(parts[0],parts[1]-1,parts[2]), //months are 0-based in javascript
				day		= date.getDate(),
				month	= date.getMonth(),
				year	= date.getFullYear();

			return this.months[month]+' '+day+', '+year;
		},
		// ! dmblog.html5tests
		html5tests		: {
			audio			: function () {
				var audioTest		= $('<audio></audio>'),
					html5rocks		= false;
				// if our browser can play HTML5 audio, and can play at least one of these types of audio (ogg or AAC)...
				if (audioTest.get(0).canPlayType && (audioTest.get(0).canPlayType('audio/ogg; codecs="vorbis"').replace(/no/, '') || audioTest.get(0).canPlayType('audio/mp4; codecs="mp4a.40.2"').replace(/no/, '')))
				{
					html5rocks = true;
				}

				// remove our test <audio> tag from the DOM
				audioTest.remove();

				return html5rocks;
			},
			video			: function () {
				var videoTest		= $('<video></video>'),
					html5rocks		= false;
				// if our browser can play HTML5 video, and can play at least one of these types of video (vorbis or MP4)...
				if (videoTest.get(0).canPlayType && (videoTest.get(0).canPlayType('video/mp4; codecs="avc1.42401E, mp4a.40.2"').replace(/no/, '') || videoTest.get(0).canPlayType('video/ogg; codecs="theora, vorbis"').replace(/no/, '')))
				{
					html5rocks = true;
				}

				// remove our test <video> tag from the DOM
				videoTest.remove();

				return html5rocks;
			}
		},
		// ! dmblog.html5media
		html5media		: function (type,source,title,width,height) {
			var extension	= (type == 'audio') ? '.m4a' : '.mp4',
				html5rocks	= this.html5tests[type](),
				source		= (html5rocks) ? source	: source + extension,
				height		= (html5rocks) ? height	: height + 16,
				html		= (html5rocks) ? this.templates['html5'+type] : this.templates.quicktime;

			// load our values into the template
			html	= html
						.replace(/\{width\}/g, width)
		 				.replace(/\{height\}/g, height)
		 				.replace(/\{source\}/g, source)
		 				.replace(/\{type\}/g, type);

			// now load our content using fancybox
			$.fancybox({
				beforeLoad	: function() {
					this.type			= 'html',
					this.title			= title,
					this.content		= html,
					this.aspectRatio	= (type == 'video');

					// let's track this event with Google Analytics
					var action = 'Play ' + type.charAt(0).toUpperCase() + type.slice(1);
					ga(_gas, _gae, 'Media', action, title);
				},
				beforeShow	: function() {
					// play our HTML5 media track (quicktime objects already have the autoplay attribute set)
					if ($(type).length)
					{
						$(type).get(0).play();
					}
				}
			});
		},
		// ! dmblog.isVisible
		isVisible		: function (element,threshold) {
			var $w				= $(window),
				$e				= $(element),
				th				= threshold || $('#nav-bar').height() + 32,
				windowTop		= $w.scrollTop(),
				windowBottom	= windowTop + $w.height(),
				elementTop		= $e.offset().top,
				elementBottom	= elementTop + $e.height();

			// if our element is display: none then it's not visible
			if ($e.is(':hidden')) {
				return false;
			}

			return elementBottom >= windowTop - th && elementTop <= windowBottom + th;
		},
		// ! dmblog.runWhenVisible
		runWhenVisible	: function (element,method) {
			var $w				= $(window),
				namespace		= element.replace('#','.'),
				handler			= function() {
					// when our element becomes visible
					if (dmblog.isVisible(element))
					{
						// unbind all handlers in this namespace to prevent it from re-firing multiple times
						$w.unbind(namespace);

						// run our requested method
						dmblog[method]();
					}
				};

				// bind our handler to the scroll and resize events on window – with an appended namespace
				$w.bind('scroll'+namespace, handler);
				$w.bind('resize'+namespace, handler);
		},
		// ! dmblog.loadAds
		loadAds			: function () {
			var $itunes		= $('#itunes-ad'),
				template	= this.templates.iTunesAd,
				width		= (this.isMobile) ? 320 : 300,
				height		= (this.isMobile) ? 50	: 250;

			// load our values into the template
			template	= template
							.replace(/\{width\}/g, width)
							.replace(/\{height\}/g, height);

			// append our html to the iTunes ad element
		 	$itunes.append(template);
		},
		// ! dmblog.loadComments
		loadComments	: function () {
			// remove our class to load comments
			$('article.post').removeClass('load-comments');

			// we're adding a timestamp to the query string to prevent caching, otherwise, everything is messed ;-)
			$.get(window.location.href.split('#')[0]+'/comments',{'_' : $.now()},function(data) {
				$('#loading-comments').replaceWith(data);

				if (data.indexOf('<code') > -1 ) {
					$.getScript(dmblog.defaults.scripts.code,function() {
						// color our code
						Prism.highlightAll();
					},true);
				}

				// if we have a specific comment requested, scroll to it
				if (hash.indexOf('comment-') > -1 && $(hash).length) {
					$('html, body').animate({
						scrollTop: $(hash).offset().top - ($('#nav-bar').height() + 40)
					}, 1000);
				}

				dmblog.formatComments();
			});
		},
		// ! dmblog.formatComments
		formatComments	: function () {
			var $cff			= $('.comment-form-fields'),
				$respond		= $('#respond'),
				$gravatar		= $('.comment-form-avatar img'),
				inputs			= 'input:not(:checkbox),textarea',
				gravatar_path	= '//secure.gravatar.com/avatar/';

			// move our form labels over the inputs and hide them
			$cff.addClass('label-over-input').hide();

			// show our form fields when the form get's focus
			$respond.on('click','#comment',function(event) {
				// make our comment textarea bigger
				$('#comment').animate({height: 100}, 'slow');

				// show our form fields
				$cff.show();
			});

			// load our md5 script
			$.getScript(dmblog.defaults.scripts.md5,function() {
				// if our inputs have values already (cookie) then hide the labels
				$(inputs,$respond).each(function() {
					if ($(this).val() != '')
					{
						var label = $('label[for="'+$(this).attr('id')+'"]');
						// hide our label
						label.addClass('hide-label');
	
						if($(this).attr('type') == 'email')
						{	// load our avatar if there's an email value
							var gravatar = gravatar_path + CryptoJS.MD5($(this).val()) + '?s=64&d=mm';
							$gravatar.attr('src',gravatar);
						}
					}
				});
			},true);

			// hide our label while the user is entering information
			$respond.on('focus',inputs,function(event) {
				$('label[for="'+$(this).attr('id')+'"]').addClass('hide-label');

				// hide the mobile ad while entering information since it gets in the way
				$('#mobile-ad').hide();
			});

			// hide our labels when the inputs change or when it loses focus
			$respond.on('blur',inputs,function(event) {
				var label = $('label[for="'+$(this).attr('id')+'"]');

				// show the mobile ad again...
				$('#mobile-ad').show();

				//if we have something entered in our input box
				if ($(this).val() != '')
				{
					// hide our label
					label.addClass('hide-label');
					
					if($(this).attr('type') == 'email')
					{	// load our avatar if there's an email value
						var gravatar = gravatar_path + CryptoJS.MD5($(this).val()) + '?s=64&d=mm';
						$gravatar.attr('src',gravatar);
					}
				} else {
					// show the label if the value is now blank
					label.removeClass('hide-label');

					if($(this).attr('type') == 'email') {
						// remove our avatar
						var gravatar = gravatar_path + '0?s=64&d=mm';
						$gravatar.attr('src',gravatar);
					}
				}
			});

			// prepare to submit our comments via ajax
			this.submitComments();
		},
		// ! dmblog.submitComments
		submitComments	: function () {
			var $commentForm	= $('#commentform')
				templates		= this.templates.comments;

			// add an info panel before the form to provide feedback/errors
			$('#respond header').append(templates.status);
			var $statusDIV		= $('#comment-status');

			// when someone submits the comment form
			$commentForm.submit(function() {
				var data		= $commentForm.serialize(),
					url			= $commentForm.attr('action');

				// add a status message
				$statusDIV.html(templates.processing);

				// post the form using ajax
				$.ajax({
					type	: 'post',
					url		: url,
					data	: data,
					error	: function(XMLHttpRequest, textStatus, errorThrown) {
								$statusDIV.html(templates.error);
					},
					success	: function(data,textStatus) {
						if (data == 'success')
						{
							$statusDIV.html(templates.pending);

							// remove the comment text
							$commentForm.find('textarea[name=comment]').val('');

							// move the form back to the bottom
							$('a#cancel-comment-reply-link').trigger('click');
						} else {
							$statusDIV.html(templates.wait);
						}
					}
				});

				return false;
			});
		},
		// ! dmblog.submitSubscribe
		submitSubscribe : function () {
			var $subscribeForm	= $('#subscribe-form');

			// Submit our new subscription form via ajax
			$subscribeForm.submit(function() {
				/* console.log('submitted'); */
				var data	= $subscribeForm.serialize(),
					url		= $subscribeForm.attr('action');

				// post the form using ajax
				$.ajax({
					type	: 'post',
					url		: url,
					data	: data,
					error	: function(XMLHttpRequest, textStatus, errorThrown) {
								$('#sub-text').html(dmblog.templates.subscriptionErrors.subscribe);
					},
					success	: function(data,textStatus) {
								if (data.indexOf('error') > -1)
								{
									// if we have an error, just display the error but leave the form in place
									$('#sub-text').html(data);
								} else {
									// if there's no error, there's no need to keep the form
									$('#email-subscriptions').html(data);

									// track this with google analytics
									var email = $('.entry-content .email').text();
									ga(_gas, _gae, 'Subscribe', 'Requested Subscription', email);
								}
					}
				});

				return false;
			});
		},
		// ! dmblog.loadMap
		loadMap			: function (element,location,customZoom) {
			var defaults	= this.defaults.mapbox,
				template	= this.templates.mapbox,
				isMobile	= this.isMobile,
				element		= (element)		? element		: defaults.mapContainer,
				customZoom	= (customZoom)	? customZoom	: 15; // only used if we have a location specified

			// load our mapbox stylesheet first
			$('head').append('<link rel="stylesheet" type="text/css" href="'+defaults.stylesheet+'">');

			// load our mapbox script (allow caching)
			$.getScript(defaults.script,function() {
				// include mapbox' fullscreen control js here https://github.com/mapbox/Leaflet.fullscreen
				L.Control.Fullscreen=L.Control.extend({options:{position:'topleft',title:'View Fullscreen'},onAdd:function(c){var a=L.DomUtil.create('div','leaflet-control-fullscreen leaflet-bar leaflet-control'),b=L.DomUtil.create('a','leaflet-control-fullscreen-button leaflet-bar-part',a);this._map=c;b.href='#';b.title=this.options.title;L.DomEvent.on(b,'click',this._click,this);return a},_click:function(a){L.DomEvent.stopPropagation(a);L.DomEvent.preventDefault(a);this._map.toggleFullscreen()}});L.Map.include({isFullscreen:function(){return this._isFullscreen},toggleFullscreen:function(){var a=this.getContainer();if(this.isFullscreen()){if(document.exitFullscreen){document.exitFullscreen()}else{if(document.mozCancelFullScreen){document.mozCancelFullScreen()}else{if(document.webkitCancelFullScreen){document.webkitCancelFullScreen()}else{L.DomUtil.removeClass(a,'leaflet-pseudo-fullscreen');this.invalidateSize();this._isFullscreen=false;this.fire('fullscreenchange')}}}}else{if(a.requestFullscreen){a.requestFullscreen()}else{if(a.mozRequestFullScreen){a.mozRequestFullScreen()}else{if(a.webkitRequestFullscreen){a.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT)}else{L.DomUtil.addClass(a,'leaflet-pseudo-fullscreen');this.invalidateSize();this._isFullscreen=true;this.fire('fullscreenchange')}}}}},_onFullscreenChange:function(){var a=document.fullscreenElement||document.mozFullScreenElement||document.webkitFullscreenElement;if(a===this.getContainer()){this._isFullscreen=true;this.fire('fullscreenchange')}else{if(this._isFullscreen){this._isFullscreen=false;this.fire('fullscreenchange')}}}});L.Map.mergeOptions({fullscreenControl:false});L.Map.addInitHook(function(){if(this.options.fullscreenControl){this.fullscreenControl=new L.Control.Fullscreen();this.addControl(this.fullscreenControl)}var a;if('onfullscreenchange' in document){a='fullscreenchange'}else{if('onmozfullscreenchange' in document){a='mozfullscreenchange'}else{if('onwebkitfullscreenchange' in document){a='webkitfullscreenchange'}}}if(a){this.on('load',function(){L.DomEvent.on(document,a,this._onFullscreenChange,this)});this.on('unload',function(){L.DomEvent.off(document,a,this._onFullscreenChange)})}});L.control.fullscreen=function(a){return new L.Control.Fullscreen(a)};

				// create our map container
				var $mapElement = $('<div id="'+defaults.mapID+'"></div>');

				// add our map element to the page
				$(element).append($mapElement);

				// load our map
				var map = L.map(defaults.mapID, {
					center					: [43.670295,-79.391525],
					zoom					: 11,
					zoomAnimationThreshold	: 15,
					attributionControl		: false,
					fullscreenControl		: true
					}).addLayer(L.mapbox.tileLayer(defaults.mapboxID, {
						detectRetina	: true,
						retinaVersion	: defaults.retinaID,
						markerLayer		: false
					})),
					markerLayer	= L.mapbox.markerLayer().addTo(map);

				// add custom popups
				markerLayer.on('layeradd', function(e) {
					var marker			= e.layer,
						feature			= marker.feature,
						properties		= feature.properties,
						popupContent	= '',
						single			= '',
						posts			= '';

					// add our title for the location if we have one
					if(properties.title)
					{
						popupContent	+= template.locationTitle.replace('{title}',properties.title);
					}

					// if we only have a single post for this location, we'll show a narrower box
					single				 = (properties.posts.length == 1) ? ' single' : '';

					// add our description and start our list of posts
					popupContent		+= template.description.replace('{single}',single);

					// loop through our posts to add them to the marker
					$.each(properties.posts, function(index, post) {
						posts			+= template.post
											.replace('{permalink}',post.permalink)
											.replace('{thumb}',post.thumbnail)
											.replace('{date}', post.date)
											.replace(/\{title\}/g, post.title);
					});

					// now replace our posts in the template
					popupContent		= popupContent.replace('{posts}',posts);

					// add our popup content
					marker.bindPopup(popupContent, {
						maxWidth : 425,
						minWidth : 204
					});
				})

				// load our markers/posts
				.loadURL('/geojson')

				// when our marker layer is loaded and ready...
				.on('ready', function(e) {
					// only if we have a location...
					if (location) {
						// if this is a mobile device, open the map fullscreen
						if (isMobile) {
							map.toggleFullscreen();

							// on mobile, also close fancybox when closing fullscreen
							$('a.leaflet-control-fullscreen-button').click(function (event) {
								$.fancybox.close(true);
							});
						}

						// move to our coordinates and zoom in
						map.setView(location,customZoom,{animate:true})
							// wait until the map has finished moving/zooming in/out
							.on('zoomend',function() {
								// loop through all our markers to find the right one
								markerLayer.eachLayer(function(marker) {
									var coordinates = marker.feature.geometry.coordinates,
												  p	= 9;
									// round to 9 digits to compare the coordinates since leaflet rounds our coordinates
									if (coordinates[0].toPrecision(p) == location.lng.toPrecision(p) && coordinates[1].toPrecision(p) == location.lat.toPrecision(p)) {
										// open the popup at this location
										marker.openPopup();
									}
								});
							});
					}

					// redraw/resize fancybox
					$.fancybox.update();
					$.fancybox.reposition();
				})

				// show tooltips on hover
				.on('mouseover', function(e) {
					e.layer.openPopup();
				});
				// we could close the popout when unhovering, but it makes it harder to click the posts in the tooltip
				/*
					.on('mouseout', function(e) {
						e.layer.closePopup();
					})
				*/
			},true);
		}
	};

	// ! run our initialize scripts
	dmblog.init();

	// ! if the hash exists on the page, scroll to it
	if ($(hash).length) {
		$('html, body').animate({
			scrollTop: $(hash).offset().top - ($('#nav-bar').height() + 32)
		}, 1000);
	}

	// ! for single posts, load some scripts
	if ($('body').hasClass('single'))
	{
		// this is is the post's container
		var $article		= $('article.post'),
			codeElements	= 'article.post code', // no need to check for code in comments, since we check for it when loading comments
			authorInfo		= '.article-info';

		// only load our comments if this isn't a draft post
		if ($article.hasClass('load-comments') && ! $article.hasClass('status-draft'))
		{
			/* append our comments loading placeholder */
			$('#nav-below').before(dmblog.templates.comments.loading);

			// ! if author info is visible, load comments
			if (dmblog.isVisible(authorInfo) || hash.indexOf('comment-') > -1)
			{
				dmblog.loadComments();
			} else {
				dmblog.runWhenVisible(authorInfo,'loadComments');
			}
		}

		// ! show disclaimer if the requested
		if ($article.hasClass('show-disclaimer'))
		{
			dmblog.fancyContent('/disclaimer',true);
		}

		// ! if we have a tweet, load twitter's js
		if ($article.hasClass('has-tweet'))
		{
			$.getScript(dmblog.defaults.scripts.twitter,null,true);
		}

		// ! if we have a code element, load the code coloring js
		if ($(codeElements).length)
		{
			$.getScript(dmblog.defaults.scripts.code,function(){
				// color our code
				Prism.highlightAll();
			},true);
		}

		// ! if we have a review, load foursquare's js
				if ($('.hreview').length)
		{
			$.getScript(dmblog.defaults.scripts.foursquare,null,true);
		}		
	}

	// ! if the affiliate ad element is visible, load the ad
	var adsElement	= '#side';
	if ($(adsElement).is(':visible') && dmblog.isVisible(adsElement))
	{
		dmblog.loadAds();
	} else {
		dmblog.runWhenVisible(adsElement,'loadAds');
	}

	// ! Open external links in a new tab/window (Anything with "http" in it, but not from this domain)
	$('body').on('click','a[href^="http"]:not([href^="'+dmblog.homeURL+'"]):not(article.post .geo a.place):not(.dm-maplink):not(.hreview .dinesafe a)',function(event) {

		$(this).attr('target','_blank');

		// track this with google analytics
		var href	= $(this).attr('href');
		ga(_gas, _gae, 'Link', 'External Link', href, {'nonInteraction': 1});
	});

	// ! when someone clicks on agree or disagree buttons
	$('body').on('click','#agree',function () {
		//track this with Google Analytics
		ga(_gas, _gae, 'Disclaimer', 'Agree');

		// close our fancybox window
		$.fancybox.close();
	});
	$('body').on('click','#disagree',function () {
		//track this with Google Analytics
		ga(_gas, _gae, 'Disclaimer', 'Disagree');

		// redirect them to main page
		window.location.href = '/';
	});

	// ! Bind FancyBox onto the #subscribe anchor
	$('.menu-item-9 a, a[href="#subscribe"]').click(function(event) {
		event.preventDefault();

		// if the user clicked this while the disclaimer is up, redirect to home
		if ($('.dm-ajax .disclaimer').length)
		{
			window.location.href = '/#subscribe';
		} else {
			dmblog.fancyContent('/subscribe',false);

			// track this with google analytics
			ga(_gas, _gae, 'Subscribe', 'Clicked Subscribe');
		}
	});
	if (hash == '#subscribe'){ $('.menu-item-9 a').trigger('click'); }

	// ! Bind FancyBox onto the #about and #domains anchor
	$('.menu-item-6 a, .menu-item-8 a, a[href="#domains"], a[href="#about"]').click(function(event) {
		// only do this if the disclaimer isn't up and not on mobile
		if ($('.dm-ajax .disclaimer').length == 0 && ! dmblog.isMobile)
		{
			var page = $(this).attr('href');

			event.preventDefault();

			dmblog.fancyContent(page,false);

			// let google analytics know about this page view
			ga(_gas, _gapv, page);
		}
	});
	if (hash == '#about'){ $('.menu-item-6 a').trigger('click'); }
	if (hash == '#domains'){ $('.menu-item-8 a').trigger('click'); }

	// ! Open/Close Our Archive Years on click
	$('ul.archives.years h3 a').click(function (event) {
		event.preventDefault();

		var expanded	= ($(this).parents('li').hasClass('expand'));

		$(this).parents('li').toggleClass('expand');
		$(this).parents('li').toggleClass('collapse',expanded);
	});
	// add the expand class to our year if it's loaded by the url hash
	if ($(hash).length && $('.archives').length) { $(hash).addClass('expand'); }

	// ! open any google map links in a fancybox
	$('a[href*="maps.google.c"]').fancybox({
		beforeLoad	: function() {
			this.type	= 'iframe',
			this.width	= 640,
			this.height	= 480,
			this.href	= $(this.element).attr('href') + '&t=h&output=embed';
		}
	});
	// ! lazy load images
	// lazy load our photos when they're close to being visible – for mobile devices, load them sooner since they may take longer to load
	// 120px height + 14px padding x 2 rows of photos + 16px margin in between = 284
	var threshold = (dmblog.isMobile) ? 480 : 284;
	$('.lazy-load img').lazyLoad(threshold);

	// ! loop through our photos to add an attribute so that they open in a gallery - this won't apply to anything added via ajax (like comments)
	$('.dm-photo a').each(function () {
		$(this).attr('rel','dm');

		// since we know that javascript is enabled (if we're running this), it's OK to change the href to the large image to allow preloading images
		// if this image is supposed to be lazy loaded, still let fancybox show the large image
		var src = ($(this).parents('figure').hasClass('lazy-load'))
					? $('img',this).data('src')
					: $('img',this).attr('src'),
			ext = '.'+src.split('.').pop();

		// our large images just have _large appended before the extension
		$(this).attr('href',src.replace(ext,'_large'+ext));
	});

	$('.dm-photo a').fancybox({
		afterLoad	: function() {
			// track every image viewed with google analytics
			var photo_title = this.title + ' (on ' + $('.entry-title').text()+')';
			ga(_gas, _gae, 'Media', 'View Photo', photo_title);
		}
	});

	// ! play audio on click & hash load
	$('.dm-audio').click(function(event) {
		event.preventDefault();
		var source	= dmblog.cdn + $(this).data('file'),
			title	= $(this).attr('title'),
			width	= 600,
			height	= 16;

		dmblog.html5media('audio',source,title,width,height);
	});
	if ($(hash).length && $('.dm-audio').length) { $(hash).trigger('click'); }

	// ! play video on click & hash load
	$('.dm-video a').click(function(event) {
		event.preventDefault();
		var source	= $('img',this).attr('src').replace('.jpg',''),
			title	= $(this).attr('title'),
			width	= $(this).data('width'),
			height	= $(this).data('height');

		dmblog.html5media('video',source,title,width,height);
	});
	if ($(hash).length && $('.dm-video').length) { $(hash + ' a').trigger('click'); }

	// ! Open YouTube Videos using fancybox
	$('.dm-youtube a').fancybox({
		beforeLoad	: function() {
			this.type	= 'iframe',
			this.width	= 1280,
			this.height	= 720,
			this.href	= dmblog.templates.youtube.replace('{id}',$(this.element).attr('href').replace('#dm-youtube-','')),
			this.aspectRatio	= true;
		},
		afterLoad	: function() {
			// track our youtube video plays with google analytics
			var title = $(this.element).attr('title');
			ga(_gas, _gae, 'Media', 'View YouTube Video', title);
		}
	});

	// ! Load our recent posts into the sidebar
	if ((! dmblog.isMobile) && $('.no-recents').length == 0)
	{
		$.getJSON('/headlines',function(data) {
			var template	= dmblog.templates.recentPosts,
				html		= template.widget,
				posts		= '';

				// loop through our posts to generate our HTML
				$.each(data, function(index, post) {
					posts	+= template.post
					.replace(/\{title\}/g, post.title)
					.replace('{datetime}', post.date)
					.replace('{date}',dmblog.formatDate(post.date))
					.replace('{thumb}', post.thumbnail)
					.replace('{permalink}', post.permalink);
				});

			// now append our posts to our widget
			html = html.replace('{posts}', posts);

			// and add it to our page
			$('#side').prepend(html);
		});
	}

	// ! Display our Dine Safe Information
	$('.dinesafe a').click(function(event) {
		event.preventDefault();
		var permalink	= $(this).attr('href'),
			template	= dmblog.templates.dinesafe,
			html		= template.details,
			inspectionsList;
		// get the restaurant inspection details
		$.getJSON(template.json,{
			ESTABLISHMENT_ID : $(this).data('establishment')
		},function(data) {
			// start generating our HTML
			html = html
					.replace('{type}', data.type)
					.replace('{minimum}', data.minimumInspections)
					.replace('{permalink}', permalink);

			// if we don't have inspections to show, show a message
			if (data.inspections.length == 0)
			{
				inspectionsList = template.inspections.noinfo;
			} else {
				// open a list of inspections
				inspectionsList = '<ul class="inspections">';

				// loop through our inspections and add them to the list
				$.each(data.inspections, function(index, inspection) {
					// add the inspection details
					var inspectionDetails = template.inspections.inspection
						.replace('{date}',dmblog.formatDate(inspection.date))
						.replace('{status}',template.inspections.status[inspection.status])
						.replace('{class}',template.inspections.status[inspection.status]);

					var infractions = '';

					// only show the infractions if the inspection didn't pass...
					if (template.inspections.status[inspection.status] != 'pass')
					{
						// start our list of infractions for this inspection
						infractions ='<ul class="infractions">';
						// loop through each category of infractions
						$.each(inspection.infractions, function (category,details) {
							// in each category of infractions, we have mulitple (maybe) infractions
							$.each(details,function(index,value) {
								infractions += template.inspections.infraction
									.replace('{severity}',template.inspections.severity[value.severity])
									.replace('{action}',value.action)
									.replace('{details}',value.details);
							});
						});

						// now close our infractions list
						infractions += '</ul>';
					}

					// append our infractions to the inspection details
					inspectionDetails = inspectionDetails.replace('{infractions}',infractions);

					// and append our inspection details to the inspections List
					inspectionsList += inspectionDetails;
				});

				// now close our list of inspections
				inspectionsList += '</ul>';
			}

			// now append our inspections list to the restaurant's details
			html = html.replace('{inspections}',inspectionsList);

			// ...and we're done generating our HTML! so now we need to display it
			$.fancybox({
				beforeLoad	: function() {
					this.type		= 'html',
					this.width		= 'auto',
					this.maxHeight	= 640,
					this.title		= $('.hreview .item .fn').text() + ' on DineSafe',
					this.content	= html;
				},
				afterLoad	: function() {
					// track everytime someone clicks our dinesafe button with google analytics
					var restaurant	= $('.hreview .item .fn').text();
					ga(_gas, _gae, 'Review', 'Viewed DineSafe', restaurant);
				}
			});
		});
	});

	// ! load our map on the archives page
	if ($('#archives-map-container').length)
	{
		dmblog.loadMap();

		// track our map loads with google analytics (don't count it as a not bounced though)
		ga(_gas, _gae, 'Map', 'Load Archives Map', {'nonInteraction': 1});
	}

	// ! load map when someone clicks on a geo link
	$('article.post .geo a.place').click(function(event) {
		event.preventDefault();

		// get our coordinates from the link
		var latitude	= parseFloat($('.latitude',this).text()),
			longitude	= parseFloat($('.longitude',this).text()),
			LatLng		= {lat: latitude, lng: longitude},
			$e			= $(dmblog.templates.mapbox.singleMap),
			id			= '#'+$e.attr('id'),
			title		= $(this).attr('title');

		// load our fancybox
		$.fancybox({
			beforeLoad	: function() {
				this.type			= 'html',
				this.title			= title,
				this.content		= $e;
			},
			afterLoad	: function() {
				// track our map views with google analytics
				ga(_gas, _gae, 'Map', 'View Article Location', title);
			},
			beforeShow	: function() {
				// load our map into fancybox
				dmblog.loadMap(id,LatLng);
			}
		});
	});

	// when someone clicks the location in the post footer, use the info from the header's link
	$('.article-info a.place').click(function(event) {
		event.preventDefault();
		$('article.post .geo a.place').trigger('click');
	});

	// ! load map when someone clicks on a map link within a post
	$('.dm-maplink').click(function(event) {
		event.preventDefault();

		// get our coordinates from the link
		var href		= $(this).attr('href'),
			location	= href.substring(href.indexOf('#')+1).split('/'),
			zoom		= parseFloat(location[0]),
			latitude	= parseFloat(location[1]),
			longitude	= parseFloat(location[2]),
			LatLng		= {lat: latitude, lng: longitude},
			$e			= $(dmblog.templates.mapbox.singleMap),
			id			= '#'+$e.attr('id'),
			title		= $(this).attr('title');
		// load our fancybox
		$.fancybox({
			beforeLoad	: function() {
				this.type			= 'html',
				this.title			= title,
				this.content		= $e;
			},
			afterLoad	: function() {
				// track our map views with google analytics
				ga(_gas, _gae, 'Map', 'View Map Link', title);
			},
			beforeShow	: function() {
				// load our map into fancybox
				dmblog.loadMap(id,LatLng,zoom);
			}
		});
	});

	// ! Submit our subscription cancellation form via ajax
	$('#cancel-sub-form').submit(function() {
		var data	= $(this).serialize(),
			url		= $(this).attr('action');

		// post the form using ajax
		$.ajax({
			type	: 'post',
			url		: url,
			data	: data,
			error	: function(XMLHttpRequest, textStatus, errorThrown) {
						$('.entry-content').html(dmblog.templates.subscriptionErrors.cancel);
			},
			success	: function(data,textStatus) {
						$('.entry-content').html(data);

						// if the subscriber was successfully cancelled, then track this with analytics
						if (data.indexOf('error') == -1)
						{
							var email = $('.entry-content .email').text();
							// track this with google analytics
							ga(_gas, _gae, 'Subscribe', 'Cancelled Subscription', email);
						}
			}
		});

		return false;
	});
});

// ! include (/wp-includes/js/comment-reply.js) here - which contains the functions for our comment reply link - rather than loading a separate file
addComment={moveForm:function(d,f,i,c){var m=this,a,h=m.I(d),b=m.I(i),l=m.I('cancel-comment-reply-link'),j=m.I('comment_parent'),k=m.I('comment_post_ID');if(!h||!b||!l||!j){return}m.respondId=i;c=c||false;if(!m.I('wp-temp-form-div')){a=document.createElement('div');a.id='wp-temp-form-div';a.style.display='none';b.parentNode.insertBefore(a,b)}h.parentNode.insertBefore(b,h.nextSibling);if(k&&c){k.value=c}j.value=f;l.style.display='';l.onclick=function(){var n=addComment,e=n.I('wp-temp-form-div'),o=n.I(n.respondId);if(!e||!o){return}n.I('comment_parent').value='0';e.parentNode.insertBefore(o,e);e.parentNode.removeChild(e);this.style.display='none';this.onclick=null;return false};try{m.I('comment').focus()}catch(g){}return false},I:function(a){return document.getElementById(a)}};

// ! load adsense js
(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t),s=s[s.length - 2];g.async=true;g.src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js';s.parentNode.insertBefore(g,s)}(document,'script'));

// ! disable analytics tracking if the opt-out cookie exists.
if (document.cookie.indexOf(_ds + '=true') > -1) { window[_ds] = true; }

// ! load google analytics tracking last
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create',_gap,{'siteSpeedSampleRate':50});ga('require','displayfeatures');ga(_gas,_gapv);