<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * The template for displaying attachments – we’re just going to redirect this to the main post
 *
 */
header('HTTP/1.1 301 Moved Permanently');
header('Location: '.get_permalink($post->post_parent));
?>