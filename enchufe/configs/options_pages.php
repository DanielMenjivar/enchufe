<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

return array(
	// pages
	'enchufe'	=> array(
		'page_title'	=> 'Enchufe Settings',
		'menu_title'	=> 'Enchufe',
		'help_text'		=> 'These are the main settings for the enchufe plugin.',
		'sections'		=> array(
			'twitter' => array(
				'title'		=> 'Main Twitter Account',
				'help_text'	=> "Whenever an article is tweeted by someone, the tweet details will include a twitter card with attribution for the site, as well as the article’s author. The author’s twitter handle is specified in the user settings, but this setting changes the twitter ID for the site. By using the ID# instead of the twitter @username, we can be sure that it will always link correctly even if the username is changed.",
				'fields'	=> array(
					'twitter_id'	=> array(
						'title'			=> 'Site Twitter ID#',
						'type'			=> 'text',
						'callback'		=> 'intval',
						'default_value'	=> 605951669
					),
					'consumer_key'	=> array(
						'title'			=> 'Consumer Key',
						'type'			=> 'text',
						'callback'		=> 'trim',
						'default_value'	=> ''
					),
					'consumer_secret'=> array(
						'title'			=> 'Consumer Secret',
						'type'			=> 'text',
						'callback'		=> 'trim',
						'default_value'	=> ''
					),
					'access_token'	=> array(
						'title'			=> 'Access Token',
						'type'			=> 'text',
						'callback'		=> 'trim',
						'default_value'	=> ''
					),
					'access_token_secret'=> array(
						'title'			=> 'Access Token Secret',
						'type'			=> 'text',
						'callback'		=> 'trim',
						'default_value'	=> ''
					)
				)
			),
			'media'	=> array (
				'title'		=> 'Media Settings',
				'help_text'	=> "",
				'fields'	=> array(
					'lazy_load'		=> array(
						'title'			=> '# of images to load before applying lazy loading to the rest',
						'type'			=> 'text',
						'callback'		=> 'intval',
						'default_value'	=> 12
					)
				)
			)
		)
	)
);

/**
 * End of file options_pages.php
 * Location: ./wp-content/plugins/enchufe/configs/options_pages.php
 **/