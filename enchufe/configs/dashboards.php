<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/* these are the settings we use for the plugin */
return array(
	// dashboards
	'enchufe'	=> array(
		'page_title'	=> 'Enchufe Dashboard',
		'menu_title'	=> 'Enchufe',
		'capability'	=> 'edit_others_posts',
		'data_callback'	=> 'main_dashboard'
	)
);

/**
 * End of file dashboards.php
 * Location: ./wp-content/plugins/enchufe/configs/dashboards.php
 **/