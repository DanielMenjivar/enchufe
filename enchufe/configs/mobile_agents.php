<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

return array(
	'google wireless transcoder',	// google wireless transcoder
	'iPhone',						// Apple iPhone
	'iPod',							// Apple iPod touch
	'Android',						// 1.5+ Android
	'blackberry',					// Storm
	'dream',						// Pre 1.5 Android
	'CUPCAKE',						// 1.5+ Android
	'webOS',						// Palm Pre Experimental
	'incognito',					// Other iPhone browser
	'webmate',						// Other iPhone browser
	's8000',						// Samsung Dolphin browser
	'bada',							// Samsung Dolphin browser
	'mobileexplorer',
	'openwave',
	'opera mini',
	'operamini',
	'elaine',
	'palmsource',
	'digital paths',
	'avantgo',
	'xiino',
	'palmscape',
	'nokia',
	'ericsson',
	'motorola',
	'Googlebot-Mobile'
);

/**
 * End of file mobile_agents.php
 * Location: ./wp-content/plugins/enchufe/configs/mobile_agents.php
 **/