<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/* these are the only valid shortcodes for the plugin, and their defaults */
return array(
	'audio' => array(
		'file'			=> NULL,
		'title'			=> 'Audio Track',
		'link_text'		=> 'play the song'
	),
	'cdn'	=> array(
		'protocol'		=> NULL
	),
	'code'	=> array(
		'language'		=> 'markup',
		'highlight'		=> NULL,
		'offset'		=> NULL,
		'block'			=> false
	),
	'donate' => array(
		'text'			=> 'I respect your privacy and don’t inundate you with obtrusive ads. If you found this useful, please consider making a donation.'
	),
	'image'	=> array(
		'file'			=> NULL,
		'align'			=> 'full',
		'alt'			=> NULL,
		'caption'		=> NULL,
		'url'			=> NULL
	),
	'maplink' => array(
		'lat'			=> NULL,
		'long'			=> NULL,
		'zoom'			=> 17,
		'title'			=> 'Map',
		'link_text'		=> 'a map of the area'
	),
	'photo'	=> array(
		'file'			=> NULL,
		'extension'		=> 'jpg',
		'align'			=> 'full',
		'title'			=> 'Photo',
		'caption'		=> NULL,
		'occurrences'	=> 0
	),
	'place' => array(
		'name'			=> NULL,
		'url'			=> NULL,
		'address'		=> NULL,
		'city'			=> NULL,
		'state'			=> NULL,
		'phone'			=> NULL,
		'lat'			=> NULL,
		'long'			=> NULL,
		'zoom'			=> 17
	),
	'review' => array(
		'rating'		=> 5.0,
		'name'			=> NULL,
		'url'			=> NULL,
		'address'		=> NULL,
		'city'			=> NULL,
		'state'			=> NULL,
		'phone'			=> NULL,
		'zomato_id'		=> NULL,
		'dinesafe_id'	=> NULL
	),
	'stars' => array(
		'rating'		=> 2.5,		// see https://dmblog.com/dm-rating-stars-wordpress-plugin
		'type'			=> 'Overall'
	),
	'video' => array(
		'file'			=> NULL,
		'align'			=> 'full',
		'title'			=> 'Video',
		'width'			=> 1280,
		'height'		=> 720,
		'caption'		=> NULL
	),
	'youtube' => array(
		'id'			=> NULL,
		'align'			=> 'full',
		'title'			=> 'YouTube Video',
		'width'			=> 600,
		'height'		=> 450,
		'caption'		=> NULL
	)
);

/**
 * End of file shortcodes.php
 * Location: ./wp-content/plugins/enchufe/configs/shortcodes.php
 **/