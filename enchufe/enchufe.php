<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/*
Plugin Name: Enchufe
Plugin URI: https://dmblog.com/search/WordPress
Description: Optimizations and customizations for WordPress.
Version: 3.0
Author: Daniel Menjívar
Author URI: https://danielmenjivar.com/
*/

// see also https://dmblog.com/dm-blog-customizations

/**
 * Provide auto-loading support of classes.
 *
 * Class names are converted to file names by making the class name
 * lowercase and converting underscores to slashes:
 *
 *		// loads classes/my/class/name.php or /classes/modules/my/class/name.php
 *		My_Class_Name::get_instance();
 */
spl_autoload_register(function ($class) {
	// transform the class name into a path
	$file		= str_replace('_', '/', strtolower($class));

	// all classes are in the classes folder, append PHP file extensions
	$filename	= dirname(__FILE__).'/classes/'.$file.'.php';

	// our modules are in the modules subfolder
	$module		= dirname(__FILE__).'/classes/modules/'.$file.'.php';

	try {
		if (file_exists($filename))
		{
			// load the class file
			require $filename;

			// class has been found
			return true;
		} elseif (file_exists($module)) {
			// load the module class
			require $module;

			// class has been found
			return true;
		}

		// class is not in the filesystem
		return false;
	} catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
		die;
	}
});

// load our plugin and store the instance in a variable to be retrieved later
$enchufe = Enchufe::get_instance();

/**
 * End of file enchufe.php
 * Location: ./wp-content/plugins/enchufe/enchufe.php
 **/