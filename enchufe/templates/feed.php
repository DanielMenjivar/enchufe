<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * RSS2 Feed Template for displaying RSS2 Posts feed.
 * see /wp-includes/feed-rss2.php for the original
 */

echo '<?xml version="1.0" encoding="UTF-8"?>';?>

<rss version="2.0" 
	xmlns:dc="http://purl.org/dc/elements/1.1/" 
	xmlns:atom="http://www.w3.org/2005/Atom" 
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" 
	xmlns:webfeeds="http://webfeeds.org/rss/1.0">
<channel>
	<title><?php wp_title_rss();?></title>
	<atom:link href="<?php echo $enchufe->home_url;?>blog/feed" rel="self" type="application/rss+xml" />
	<link><?php echo $enchufe->home_url;?>blog/</link>
	<description><?php bloginfo_rss("description");?></description>
	<lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
	<language>en-CA</language>
	<sy:updatePeriod>weekly</sy:updatePeriod>
	<sy:updateFrequency>2</sy:updateFrequency>
	<generator><?php echo $enchufe->blog_name;?></generator>
	<image>
		<title><?php wp_title_rss();?></title>
		<url><?php echo $enchufe->home_url;?>a/u/dm.png</url>
		<link><?php echo $enchufe->home_url;?>blog/</link>
		<width>128</width>
		<height>128</height>
		<description><?php bloginfo_rss("description");?></description>
	</image>
	<webfeeds:related layout="card" target="browser" />
	<webfeeds:icon><?php echo $enchufe->home_url;?>a/u/dm.png</webfeeds:icon>
	<webfeeds:logo><?php echo $enchufe->home_url;?>a/u/dm.png</webfeeds:logo>
	<webfeeds:accentColor>00C3FF</webfeeds:accentColor><?php

	/* Start the Loop. */
	while (have_posts()) :
		the_post(); global $post; $article = Article::factory($post);?>

		<item>
			<title><?php the_title_rss() ?></title>
			<dc:creator><![CDATA[<?php echo $article->author_name('display');?>]]></dc:creator>
			<pubDate><?php echo $article->post_date()->format('r');?></pubDate>
			<link><?php echo $article->permalink();?></link>
			<guid><?php echo $article->permalink();;?></guid>
			<description><![CDATA[<img width="128" height="128" src="<?php
				echo $enchufe->home_url;
				echo $article->thumbnail();?>" class="webfeedsFeaturedVisual" alt="<?php
				echo esc_attr($article->post_title());
				?>" style="float:left;margin-right:1em;border-radius:4px;width:128px;height:128px" /><p><?php
				the_excerpt_rss(); ?></p>]]></description>
		</item><?php
	endwhile; ?>
</channel>
</rss>