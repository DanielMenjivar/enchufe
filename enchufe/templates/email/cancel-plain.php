<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); } ?>
Hello, 

You have successfully unsubscribed from <?php echo $enchufe->blog_name_lower;?> email updates.

We’re sorry to see you go!


----- 
Was this a mistake?
Did you forward your email to a friend and they accidentally clicked the unsubscribe link?  If this was a mistake, you can subscribe again at <?php
	echo $enchufe->home_url;?>#subscribe<?php

/**
 * End of file cancel-plain.php
 * Location: ./wp-content/plugins/enchufe/templates/email/cancel-plain.php
 **/