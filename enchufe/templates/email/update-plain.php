<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }?>
✻✻ <?php echo strtoupper($article->post_title());?> ✻✻
<?php echo $article->post_date()->format('F j, Y');

// only display the geodata if we have it
if ($article->is_geotagged()) :
	?> ✻ <?php echo $article->post_meta('geo_address');?><?php
endif;

/*
// display the author
	?> ✻ by <?php echo $article->author_name('display');
*/?>


<?php echo strip_tags($article->post_excerpt());?>


Continue reading at: <?php echo $article->permalink();?>



----- 
You received this message because you are subscribed to email updates from <?php echo $enchufe->blog_name_lower;?>. To stop receiving these emails, you can unsubscribe by copying and pasting this link into your browser: <?php echo $enchufe->home_url;?>subscribe/?cancel=<?php
	echo $guid.'&utm_source='.str_replace(' ','+',$enchufe->blog_name).'+Updates&utm_medium=email&utm_content=cancel&utm_campaign='.$article->post_name();

/**
 * End of file update-plain.php
 * Location: ./wp-content/plugins/enchufe/templates/email/update-plain.php
 **/