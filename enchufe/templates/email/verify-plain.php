<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); } ?>
Hello, 

You recently requested an email subscription to <?php echo $enchufe->blog_name_lower;?>. Updates are sent whenever there’s new content on <?php echo $enchufe->blog_name_lower;?> including blog posts, upcoming events and charts. In 2015, subscribers received around one email per month and never more than one per week. You can easily unsubscribe at any time if you change your mind.

Please copy and paste the following link into your browser to verify your email address and activate your subscription:

<?php
	echo $enchufe->home_url;?>subscribe/?verify=<?php
	echo $guid.'&utm_source='.$enchufe->blog_name_lower.'+Verify&utm_medium=email&utm_content=button&utm_campaign=Verify+Subscription'; ?>



----- 
You received this message because you requested an email subscription to <?php echo $enchufe->blog_name_lower;?>. If you received this in error, please disregard.<?php

/**
 * End of file verify-plain.php
 * Location: ./wp-content/plugins/enchufe/templates/email/verify-plain.php
 **/