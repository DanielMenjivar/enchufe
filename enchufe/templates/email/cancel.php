<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); } ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no,width=device-width" />
	<style>
		a{
			color:#0088cc;
			text-decoration:none;
		}
	</style>
</head>
<body style="
	margin:0;
	padding:0;
	font:100% 'HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue','Helvetica-Light','Helvetica Light',Helvetica,Arial,sans-serif;
	-webkit-text-size-adjust:100%;
	-ms-text-size-adjust:100%;
	color:#000;
	background:#fff">
	<header style="
		margin:0;
		padding:0;
		width:100%;
		z-index:9999;
		position:absolute">
		<h1 style="
			float:left;
			margin:0 10px;
			font:20px/40px 'HelveticaNeue-UltraLight','Helvetica Neue UltraLight','HelveticaNeue-Light','Helvetica Neue Light','Helvetica-Light','Helvetica Light','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-weight:100
		"><a style="color:#404040" href="<?php echo $enchufe->home_url.'?utm_source='.
				$enchufe->blog_name_lower.'+Cancel&utm_medium=email&utm_content=logo&utm_campaign=Cancel+Subscription';?>" title="<?php echo $enchufe->blog_name;?>"><?php
			echo $enchufe->blog_name_lower;?>
		</a></h1> 
		<h2 style="
			float:right;
			color:#404040;
			margin:0 10px;
			font:14px/40px 'HelveticaNeue-CondensedBlack','Helvetica Neue Condensed Black','Helvetica-Condensed-Black','Helvetica Condensed Black','Helvetica Neue Bold','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-weight:bold;
			text-transform:uppercase">Subscription Cancelled</h2> 
	</header>
	<article style="
		z-index:1;
		clear:both;
		padding:40px 10px 1em 10px">
		<p>Hello, </p>
		<p>You have successfully unsubscribed from <a href="<?php echo $enchufe->home_url.'?utm_source='.
				$enchufe->blog_name_lower.'+Cancel&utm_medium=email&utm_content=body&utm_campaign=Cancel+Subscription';?>" style="text-decoration:none;color:#0088cc"><?php echo $enchufe->blog_name_lower;?></a> email updates. </p>
		<p><strong><em>We’re sorry to see you go!</em></strong></p> 
	</article>
	<footer style="
		margin:0;
		padding:10px;
		color:#fff;
		background:#404040;
		text-shadow:1px 1px 1px rgba(0,0,0,0.25)">
		<hr style="
			border:0;
			height:1px;
			margin:-10px -10px 1.25em -10px;
			border-top:2px solid #333;
			padding:0;
			box-shadow:0 10px 10px rgba(0,0,0,0.5)" />
		<small>
			<p>
				<strong><em>Was this a mistake?</em></strong><br />
				Did you forward your email to a friend and they accidentally clicked the unsubscribe link? If this was a mistake, you can <a href="<?php
					echo $enchufe->home_url.'?utm_source='.
				$enchufe->blog_name_lower.'+Cancel&utm_medium=email&utm_content=logo&utm_campaign=Cancel+Subscription';?>#subscribe" style="text-decoration:none;color:#00c3ff" title="Subscribe to email updates from <?php
					echo $enchufe->blog_name;?>"><strong>subscribe again</strong></a>.
			</p>
		</small>
	</footer>
	<style>
		@media only screen and (min-width:480px){
			h1,h2,article{
				margin:0 20px !important;
			}
		}
	</style>
</body>
</html><?php

/**
 * End of file cancel.php
 * Location: ./wp-content/plugins/enchufe/templates/email/cancel.php
 **/