<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); } ?>
Hello, 

Some time ago you requested an email subscription to <?php echo $enchufe->blog_name_lower;?> but never verified your email address. This email is just a friendly reminder to verify your free subscription. 

Updates are sent whenever there’s new content on <?php echo $enchufe->blog_name_lower;?> including blog posts, upcoming events and charts. In 2015, subscribers received around *one email per month* and never more than one per week. You can easily unsubscribe at any time if you change your mind. And if you don’t want to verify your subscription that’s OK too, **this will be the last email you receive.** 

Please copy and paste the following link into your browser to verify your email address and activate your subscription: 

<?php
	echo $enchufe->home_url;?>subscribe/?verify=<?php
	echo $guid.'&utm_source='.$enchufe->blog_name_lower.'+Verify&utm_medium=email&utm_content=plain+text&utm_campaign=Remind+Subscription'; ?>



----- 
You received this message because you requested an email subscription to <?php echo $enchufe->blog_name_lower;?>. If you received this in error, please disregard.<?php

/**
 * End of file verify-plain.php
 * Location: ./wp-content/plugins/enchufe/templates/email/verify-plain.php
 **/