<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); } ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no,width=device-width" />
	<style>
		a{
			color:#00c3ff;
			text-decoration:none;
		}
	</style>
</head>
<body style="
	margin:0;
	padding:0;
	font:100% 'HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue','Helvetica-Light','Helvetica Light',Helvetica,Arial,sans-serif;
	-webkit-text-size-adjust:100%;
	-ms-text-size-adjust:100%;
	color:#000;
	background:#fff">
	<header style="
		margin:0;
		padding:0;
		width:100%;
		z-index:9999;
		position:absolute">
		<h1 style="
			float:left;
			margin:0 10px;
			font:20px/40px 'HelveticaNeue-UltraLight','Helvetica Neue UltraLight','HelveticaNeue-Light','Helvetica Neue Light','Helvetica-Light','Helvetica Light','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-weight:100
		"><a style="color:#404040" href="<?php echo $enchufe->home_url.'?utm_source='.
				$enchufe->blog_name_lower.'+Verify&utm_medium=email&utm_content=logo&utm_campaign=Remind+Subscription';?>" title="<?php echo $enchufe->blog_name;?>" rel="home"><?php
			echo $enchufe->blog_name_lower;?>
		</a></h1> 
		<h2 style="
			float:right;
			color:#404040;
			margin:0 10px;
			font:14px/40px 'HelveticaNeue-CondensedBlack','Helvetica Neue Condensed Black','Helvetica-Condensed-Black','Helvetica Condensed Black','Helvetica Neue Bold','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-weight:bold;
			text-transform:uppercase">Activate Subscription</h2> 
	</header>
	<article style="
		z-index:1;
		clear:both;
		padding:40px 10px 1em 10px">
		<section style="margin:1em 0;font-size:1em;line-height:1.5">	
			<p>Hello, </p>
			<p>Some time ago you requested an email subscription to <?php echo $enchufe->blog_name_lower;?> but never verified your email address. This email is just a friendly reminder to verify your free subscription. </p>
			<p>Updates are sent whenever there’s new content on <?php echo $enchufe->blog_name_lower;?> including blog posts, upcoming events and charts. In 2015, subscribers received around <em>one email per month</em> and never more than one per week. You can easily unsubscribe at any time if you change your mind. And if you don’t want to verify your subscription that’s OK too, <strong>this will be the last email you receive.</strong></p>
			<p>Please click the following link to verify your email address and activate your subscription:</p>
		</section>
		<p class="button" style="width:225px;margin:1em auto">
			<a href="<?php echo $enchufe->home_url;?>subscribe/?verify=<?php echo $guid.'&utm_source='.
				$enchufe->blog_name_lower.'+Verify&utm_medium=email&utm_content=button&utm_campaign=Remind+Subscription';?>" class="button" style="
				display:inline-block;
				zoom:1;
				*display:inline;
				vertical-align:baseline;
				margin:0.5em auto;
				padding:1em 0;
				width:100%;
				outline:none;
				cursor:pointer;
				text-align:center;
				text-decoration:none;
				text-shadow:0 1px 1px rgba(0,0,0,.3);
				border-radius:0.5em;
				color:#d7f0fa;
				box-shadow:0 1px 2px rgba(0,0,0,0.2);
				border:1px solid rgb(0,100,175);
				background:rgb(0,120,200);
				background:-webkit-gradient(linear,left top,left bottom,from(rgb(0,140,235)),to(rgb(0,102,177)));
				background:-moz-linear-gradient(top,rgb(0,140,235),rgb(0,102,177));
				filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#008ceb',endColorstr='#0066b1');
			">Activate my subscription!</a>
		</p>
	</article>
	<footer style="
		margin:0;
		padding:10px;
		color:#fff;
		background:#404040;
		text-shadow:1px 1px 1px rgba(0,0,0,0.25)">
		<hr style="
			border:0;
			height:1px;
			margin:-10px -10px 1.25em -10px;
			border-top:2px solid #333;
			padding:0;
			box-shadow:0 10px 10px rgba(0,0,0,0.5)" />
		<small>
			<p>
				<strong><em>Can't click the link?</em></strong><br />
				If the link above doesn't appear clickable or doesn't open a browser window when you click it, copy and paste this link into your web browser's address bar: <?php
					echo $enchufe->home_url;?>subscribe/?verify=<?php echo $guid.'&utm_source='.
				$enchufe->blog_name_lower.'+Verify&utm_medium=email&utm_content=button&utm_campaign=Remind+Subscription';?> 
			</p>
			<p>
				You received this message because you requested an email subscription to <?php echo $enchufe->blog_name_lower;?>. If you received this in error, please disregard.
			</p>
		</small>
	</footer>
	<style>
		a.button:hover{
			background:rgb(0,95,170) !important;
			background:-webkit-gradient(linear,left top,left bottom,from(rgb(0,120,200)),to(rgb(0,70,140))) !important;
			background:-moz-linear-gradient(top,rgb(0,120,200),rgb(0,70,140)) !important;
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0078c8',endColorstr='#00468c') !important;
		}

		a.button:active{
			position:relative;
			top:1px;
			color:rgb(150,200,250) !important;
			background:rgb(0,120,200) !important;
			background:-webkit-gradient(linear,left top,left bottom,from(rgb(0,102,177)),to(rgb(0,140,235))) !important;
			background:-moz-linear-gradient(top,rgb(0,102,177),rgb(0,140,235)) !important;
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00468c',endColorstr='#0078c8') !important;
		}

		@media only screen and (min-width:480px){
			h1,h2,article{
				margin:0 20px !important;
			}
		}
	</style>
</body>
</html><?php

/**
 * End of file verify.php
 * Location: ./wp-content/plugins/enchufe/templates/email/verify.php
 **/