<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no,width=device-width" />
	<style>
		a{
			color:#0088cc;
			text-decoration:none;
		}
	</style>
</head>
<body style="
	margin:0;
	padding:0;
	font:100% 'HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue','Helvetica-Light','Helvetica Light',Helvetica,Arial,sans-serif;
	-webkit-text-size-adjust:100%;
	-ms-text-size-adjust:100%;
	color:#000;
	background:#fff">
	<header style="
		margin:0;
		padding:0;
		width:100%;
		z-index:9999;
		position:absolute">
		<h1 style="
			float:left;
			margin:0 10px;
			font:20px/40px 'HelveticaNeue-UltraLight','Helvetica Neue UltraLight','HelveticaNeue-Light','Helvetica Neue Light','Helvetica-Light','Helvetica Light','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-weight:100">
			<a style="color:#404040" href="<?php echo $enchufe->home_url.'?utm_source='.
				str_replace(' ','+',$enchufe->blog_name).'+Updates&utm_medium=email&utm_content=logo&utm_campaign='.
				$article->post_name();?>" title="<?php echo $enchufe->blog_name;?>"><?php
			echo $enchufe->blog_name_lower;?>
		</a></h1> 
		<h2 style="
			float:right;
			color:#404040;
			margin:0 10px;
			font:14px/40px 'HelveticaNeue-CondensedBlack','Helvetica Neue Condensed Black','Helvetica-Condensed-Black','Helvetica Condensed Black','Helvetica Neue Bold','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-weight:bold;
			text-transform:uppercase"><?php echo $enchufe->blog_name;?> Updates</h2> 
	</header>
	<article style="
		z-index:1;
		clear:both;
		padding:40px 10px 2em 10px">
		<header style="margin:1em 0">
			<h3 style="
				margin:1em 0 0.125em 0;
				font:1.25em/1.125em 'HelveticaNeue-CondensedBlack','Helvetica Neue Condensed Black','Helvetica-Condensed-Black','Helvetica Condensed Black', 'Helvetica Neue Bold','Helvetica Neue',Helvetica,Arial,sans-serif;
				font-weight:bold">
				<a href="<?php echo $article->permalink().'?utm_source='.
				str_replace(' ','+',$enchufe->blog_name).'+Updates&utm_medium=email&utm_content=title&utm_campaign='.
				$article->post_name();?>" title="<?php echo esc_attr($article->post_title());?>" style="text-decoration:none;color:#404040"><?php echo $article->post_title();?></a>
			</h3> 
			<span style="display:inline-block;color:#999;text-transform:uppercase;font-size:0.75em;line-height:1.25">
				<time datetime="<?php echo $article->post_date()->format('c');?>">
					<span id="weekday" style="display:none"><?php echo $article->post_date()->format('l');?>, </span><?php

					echo $article->post_date()->format('F j, Y');?>

				</time><?php

			// only display the geodata if we have it
			if ($article->is_geotagged()) :?>
				<span> ✻ <?php echo $article->post_meta('geo_address');?></span><?php
			endif;

			/*
			// display the author ?>
			 ✻ by <a href="<?php echo $article->author_url().'?utm_source='.
				str_replace(' ','+',$enchufe->blog_name).'+Updates&utm_medium=email&utm_content=author&utm_campaign='.
				$article->post_name();?>" title="View all articles by <?php
					echo $article->author_name();?>" style="color:#999;text-decoration:none"><?php
					echo $article->author_name('display');?></a> 
			*/?>
			</span>
		</header>
		<section style="margin:1em 0">
			<a href="<?php echo $article->permalink().'?utm_source='.
				str_replace(' ','+',$enchufe->blog_name).'+Updates&utm_medium=email&utm_content=thumbnail&utm_campaign='.
				$article->post_name();?>" title="<?php echo esc_attr($article->post_title());?>">
				<img src="<?php echo $article->thumbnail();?>" alt="<?php echo esc_attr($article->post_title());
					?> – Article Thumbnail" style="
						width:24%;
						height:24%;
						max-width:128px;
						max-height:128px;
						margin:0 0 0.25em 0.5em;
						float:right;
						padding:4px;
						border:1px solid #999"/>
			</a>
			<p style="font-size:1em;line-height:1.5"><?php echo strip_tags($article->post_excerpt());?></p>
		</section>
		<p class="button" style="clear:both;width:225px;margin:1em auto">
			<a href="<?php echo $article->permalink().'?utm_source='.
				str_replace(' ','+',$enchufe->blog_name).'+Updates&utm_medium=email&utm_content=button&utm_campaign='.
				$article->post_name();?>" title="<?php echo esc_attr($article->post_title());?>" style="
				display:inline-block;
				zoom:1;
				*display:inline;
				vertical-align:baseline;
				margin:1em auto 0 auto;
				padding:1em 0;
				width:100%;
				outline:none;
				cursor:pointer;
				text-align:center;
				text-decoration:none;
				text-shadow:0 1px 1px rgba(0,0,0,.3);
				border-radius:0.5em;
				color:#d7f0fa;
				box-shadow:0 1px 2px rgba(0,0,0,0.2);
				border:1px solid rgb(0,100,175);
				background:rgb(0,120,200);
				background:-webkit-gradient(linear,left top,left bottom,from(rgb(0,140,235)),to(rgb(0,102,177)));
				background:-moz-linear-gradient(top,rgb(0,140,235),rgb(0,102,177));
				filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#008ceb',endColorstr='#0066b1')
			">Continue reading… &rarr;</a>
		</p>
		<img width="1" height="1" border="0" alt="<?php echo $enchufe->blog_name;
			?>" src="<?php echo $enchufe->home_url.'u/'.$article->ID().'/'.$guid.'/b.gif';
			?>" style="
				border:0;
				clip:rect(0,0,0,0);
				height:1px;
				width:1px;
				margin:-1px;
				overflow:hidden;
				padding:0;
				position:absolute !important"/>
	</article>
	<footer style="
		margin:0;
		padding:10px;
		color:#fff;
		background:#404040;
		text-shadow:1px 1px 1px rgba(0,0,0,0.25)">
		<hr style="
			border:0;
			height:1px;
			margin:-10px -10px 1.25em -10px;
			border-top:2px solid #333;
			padding:0;
			box-shadow:0 10px 10px rgba(0,0,0,0.5)"/>
		<small>
			<p>
				You received this message because you are subscribed to email updates from <?php echo $enchufe->blog_name_lower;?>. 
				To stop receiving these emails, you can <a href="<?php echo $enchufe->home_url;?>subscribe/?cancel=<?php echo $guid.'&utm_source='.
				str_replace(' ','+',$enchufe->blog_name).'+Updates&utm_medium=email&utm_content=cancel&utm_campaign='.
				$article->post_name();?>" style="text-decoration:none;color:#00c3ff">unsubscribe</a> now.
			</p>
		</small>
	</footer>
	<style>
		a.button:hover{
			background:rgb(0,95,170) !important;
			background:-webkit-gradient(linear,left top,left bottom,from(rgb(0,120,200)),to(rgb(0,70,140))) !important;
			background:-moz-linear-gradient(top,rgb(0,120,200),rgb(0,70,140)) !important;
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0078c8',endColorstr='#00468c') !important;
		}

		a.button:active{
			position:relative;
			top:1px;
			color:rgb(150,200,250) !important;
			background:rgb(0,120,200) !important;
			background:-webkit-gradient(linear,left top,left bottom,from(rgb(0,102,177)),to(rgb(0,140,235))) !important;
			background:-moz-linear-gradient(top,rgb(0,102,177),rgb(0,140,235)) !important;
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00468c',endColorstr='#0078c8') !important;
		}

		@media only screen and (min-width:480px){
			h3{
				font-size:1.5em !important;
			}
			h1,h2,article{
				margin:0 20px !important;
			}
		}

		@media only screen and (min-width:768px){
			#weekday{
				display:inline !important;
			}
			h3{
				font-size:2.5em !important;
			}
		}
	</style>
</body>
</html><?php

/**
 * End of file update.php
 * Location: ./wp-content/plugins/enchufe/templates/email/update.php
 **/