<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// rating name url address city state phone zomato_id dinesafe_id

// get our review as an article object
global $post; $article	= Article::factory($post);

// add the url to our restuarant name if we have one
$restaurant	= (empty($url))
			? '<strong class="fn org">'.$name.'</strong>'
			: '<a class="url fn org" href="'.$url.'" rel="external" title="'.$name.'"><strong>'.$name.'</strong></a>';?>

<div class="hreview">
	<span class="metadata">
		<span class="rating"><?php echo $rating;?></span> 
		<span class="reviewer vcard">
			<a class="url fn org" href="<?php echo $article->author_url();?>" title="<?php echo $article->author_name();?>">
				<?php echo $article->author_name('display');?>
			</a>
		</span> 
		<abbr class="dtreviewed" title="<?php echo $article->post_date()->format('c');?>"><?php echo $article->post_date()->format('F j, Y');?></abbr>
	</span>
	<p class="item vcard">
		<?php echo $restaurant;?><br />
		<span class="adr">
			<span class="street-address"><?php echo $address;?></span><br />
			<span class="locality"><?php echo $city;?></span>, <span class="region"><?php echo $state;?></span><br />
			<span class="tel"><?php echo $phone;?></span>
		</span>
	</p>
	<div class="links"><?php
		/*
			<span class="save-4sq">
				<a href="https://foursquare.com/intent/venue.html" class="fourSq-widget" data-variant="wide">Save to foursquare</a>
			</span>
		*/


		// if we have a zomato id
		if  ($zomato_id):
			// load our zomato redirects from cache
			$cache_file			= 'zomato.redirects';
			$zomato_redirects	= $enchufe->cache->cache($cache_file);

			// if we don't already have a URL for this restaurant, let's get it
			if (! array_key_exists($zomato_id,$zomato_redirects))
			{
				// get our zomato URL from the short url's redirect headers
				$short_url			= 'http://zoma.to/r/'.$zomato_id;
				$redirect_headers	= @get_headers($short_url,1);
				$zomato_url			= $redirect_headers['Location'];

				if (is_array($zomato_url))
				{
					$zomato_url		= $zomato_url[0];
				}

				// add our url to the array and save/cache it
				$zomato_redirects[$zomato_id]	= $zomato_url;
				$enchufe->cache->cache($cache_file,$zomato_redirects);
			// otherwise if we already have URL saved, just use the cached URL
			} else {
				$zomato_url			= $zomato_redirects[$zomato_id];
			}?>

			<span class="zomato">
				<a href="<?php echo $zomato_url;?>" rel="external">
					<img alt="<?php echo $name;?> on Zomato" src="//www.zomato.com/logo/<?php echo $zomato_id;
						?>/minilink" style="border:none;width:130px;height:36px" class="no-border" />
				</a>
			</span><?php
		endif;

		// if we have a dine safe url
		if ($dinesafe_id) :?>

			<span class="dinesafe">
				<a href="http://www.toronto.ca/health/dinesafe/index.htm?show=detail&amp;id=<?php echo $dinesafe_id;
					?>" class="button" rel="external">DineSafe</a>
			</span><?php
		endif;?>
	</div>
</div>

<?php

/**
 * End of file review.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/review.php
 **/