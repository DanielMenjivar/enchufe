<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// text?>
<div class="<?php echo $prefix;?>background">
	<p><?php echo $text;?></p>
	<form class="<?php echo $prefix;?>donate" action="https://www.paypal.com/cgi-bin/webscr" method="post">
		<input type="hidden" name="cmd" value="_s-xclick" />
		<input type="hidden" name="hosted_button_id" value="HOSTED-BUTTON-ID-HERE" />
		<input type="hidden" name="return" value="<?php echo $enchufe->home_url . substr($_SERVER['REQUEST_URI'],6);?>" />
		<input type="hidden" name="cancel_return" value="<?php echo $enchufe->home_url . substr($_SERVER['REQUEST_URI'],6);?>" />
		<input type="hidden" name="cbt" value="Return to <?php echo $echufe->blog_name;?>" />
		<input type="image" src="//www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!" />
		<img class="no-border" alt="" src="//www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" />
	</form>
</div>

<?php
/**
 * End of file donate.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/donate.php
 **/