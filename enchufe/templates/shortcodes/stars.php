<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// see https://dmblog.com/dm-rating-stars-wordpress-plugin

// rating type

// let's generate our rating as a fraction
$fraction	 = floor($rating);										// first just the whole number part
$fraction	.= (($fraction - $rating) != 0) ? '&frac12;' : NULL;	// now append the "1/2" fraction if necessary

// initialize some variables
$plural		 = ($rating == 1) ? ' star' : ' stars';
$star_names	 = array(0 => 'empty', 5 => 'half', 10 => 'full');
$i			 = 1;
$stars		 = $rating * 10;
$images		 = NULL;

// loop through 5 times and create the span containers for our stars (concatenating them onto $stars)
while ($i <=5)
{
	$value	 = ($stars > 10) ? 10 : $stars;
	$images .= '<span class="'.$star_names[$value].'"></span>';
	$stars	 = $stars - $value;
	$i++;
}?>

<figure class="<?php echo $prefix;?>stars">
	<?php echo $images;?>
	<figcaption>
		<?php echo $type. ' Rating: '.$fraction.$plural;?>
	</figcaption>
</figure>

<?php

/**
 * End of file stars.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/stars.php
 **/