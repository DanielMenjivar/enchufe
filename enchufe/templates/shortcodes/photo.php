<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// file extension align title caption
$id			= $prefix.$tag.'-'.basename($file);
$caption	= (empty($caption)) ? NULL : '<figcaption>'.$caption.'</figcaption>';
$src		= $enchufe->upload_url_path.'/'.$file.'.'.$extension;

// if we should be lazy loading this photo, then add some attributes and change the src
$lazy_class	= ($lazy_load) ? ' lazy-load'			: '';
$lazy_atts	= ($lazy_load) ? ' data-src="'.$src.'"' : '';
$src		= ($lazy_load) ? '/a/u/b.png' : $src;

// if we have a no-border class in align, we need to add that to the image, not the figure
$no_border	= (strpos($align,'no-border') > -1) ? ' class="no-border"' : '';?>

<figure id="<?php echo $id;?>" class="<?php echo $prefix.'align-'.$align.' '.$prefix.$tag.$lazy_class;?>">
	<a href="#<?php echo $id;?>" title="<?php echo $title;?>">
		<img src="<?php echo $src;?>" alt="<?php echo $title;?>"<?php echo $lazy_atts.$no_border;?> />
	</a><?php echo $caption;?>
</figure>

<?php

/**
 * End of file photo.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/photo.php
 **/