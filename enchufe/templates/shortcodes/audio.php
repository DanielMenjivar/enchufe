<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// file title link_text
$id	= $prefix.basename($file);?>

<a id="<?php echo $id;?>" href="#<?php echo $id;?>" title="<?php echo $title;?>" class="<?php echo $prefix.$tag;?>" data-file="<?php echo $file;?>">
	<?php echo $link_text;?>
</a>

<?php
/**
 * End of file audio.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/audio.php
 **/