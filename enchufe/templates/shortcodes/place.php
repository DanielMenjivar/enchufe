<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// name url address city state phone lat long zoom

// add the url to our place name if we have one
$place		= (empty($url))
			? '<strong class="fn org">'.$name.'</strong>'
			: '<a class="url fn org" href="'.$url.'" rel="external" title="'.$name.'"><strong>'.$name.'</strong></a>';
$address	= '<span class="street-address">'.$address.'</span>';
$maplink	= (empty($lat) OR empty($long))
			? $address
			: '<a class="dm-maplink" href="//a.tiles.mapbox.com/v3/MAPBOX-MAPID-HERE/page.html#'.$zoom.'/'.$lat.'/'.$long.'" title="'.$name.'" rel="external">'.$address.'</a>';?>

<div class="<?php echo $prefix.$tag;?>">
	<p class="item vcard">
		<?php echo $place;?><br />
		<span class="adr">
			<?php echo $maplink;?><br />
			<span class="locality"><?php echo $city;?></span>, <span class="region"><?php echo $state;?></span><br />
			<span class="tel"><?php echo $phone;?></span>
		</span>
	</p>
</div>

<?php

/**
 * End of file place.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/place.php
 **/