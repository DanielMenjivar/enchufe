<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// id align title width(to embed) height caption
$thumbnail = '//img.youtube.com/vi/'.$id.'/0.jpg';

// only include a figcaption if we have a caption
$caption	= (empty($caption))
			? NULL
			: '<figcaption>'.$caption.'</figcaption>';

// get a higher resolution thumbnail from YouTube if there is one
// 0.jpg = 480 width, but we're only going to get larger thumbnails (potentially for retina) if the video is embeded at the full width
if ($align == 'full') {
	// the path to our high resolution thumbnail
	$high_rez		= str_replace('0.jpg','maxresdefault.jpg',$thumbnail);

	// load our youtube thumbnails from cache
	$cache_file			= 'youtube.thumbs';
	$youtube_thumbs		= $enchufe->cache->cache($cache_file);

	// if we don't already have a cached thumbnail, let's get it
	if (! array_key_exists($id,$youtube_thumbs))
	{
		// get the http headers for the file
		$file_headers	= @get_headers('https:'.$high_rez);

		// does our high resolution thumbnail exist? (response code 200)
		$high_rez_exists= (boolean) strpos($file_headers[0],'200');

		// add our thumbnail to the array and save/cache it
		$youtube_thumbs[$id]	= $high_rez_exists;
		$enchufe->cache->cache($cache_file,$youtube_thumbs);
	// otherwise if we have it saved already
	} else {
		$high_rez_exists=$youtube_thumbs[$id];
	}

	// set our thumbnail to use the high res one if it exists
	$thumbnail	= ($high_rez_exists) ? $high_rez : $thumbnail;

	// change the aspect ratio of the embedded thumbnail
	$height		= ($high_rez_exists) ? round($width/16*9) : $height;
}?>

<figure id="<?php echo $prefix.$tag.'-'.$id;?>" class="<?php echo $prefix.'align-'.$align.' '.$prefix.$tag;?>">
	<a href="#<?php echo $prefix.$tag.'-'.$id;?>" title="<?php echo $title;?>">
		<img src="<?php echo $thumbnail;?>" alt="<?php echo $title;?>" width="<?php echo $width;?>" height="<?php echo $height;?>" />
	</a>
	<?php echo $caption;?>
</figure>

<?php
/**
 * End of file youtube.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/youtube.php
 **/