<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// file align title width height caption
$id			= $prefix.$tag.'-'.basename($file);
// only include a figcaption if we have a caption
$caption	= (empty($caption))	? NULL : '<figcaption>'.$caption.'</figcaption>';?>

<figure id="<?php echo $id;?>" class="<?php echo $prefix.'align-'.$align.' '.$prefix.$tag;?>">
	<a href="#<?php echo $id;?>" title="<?php echo $title;?>" data-width="<?php echo $width;?>" data-height="<?php echo $height;?>">
		<img src="<?php echo $enchufe->upload_url_path.'/'.$file;?>.jpg" alt="<?php echo $title;?>" />
	</a>
	<?php echo $caption;?>
</figure>

<?php

/**
 * End of file video.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/video.php
 **/