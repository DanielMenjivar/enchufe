<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// [dm_code language highlight offset block]… …[/dm_code]

$highlight	= (empty($highlight))	? NULL : ' data-line="'.$highlight.'"';
$offset		= (empty($offset))		? NULL : ' data-line-offset="'.$offset.'"';

// generate our content
if ($block) :
	?><pre><?php
endif;
	?><code class="language-<?php echo $language;?>"<?php echo $highlight.$offset;?>><?php
		// we can’t use htmlspecialchars cause it’s messed up
		$replacements	= array(
			'<'			=> '&lt;',
			'>'			=> '&gt;',
			'&#038;&'	=> '&amp;&amp;',
			'	'		=> '&#09;',					// get rid of tabs
			"\r"		=> '&#10;',
			"\r\n"		=> '&#10;',
			"\n"		=> '&#10;',
			PHP_EOL		=> '&#10;'					// get rid of new lines
		);

		echo strtr(trim($content),$replacements);?></code><?php

if ($block) :
	?></pre><?php
endif;

/**
 * End of file code.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/code.php
 **/