<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// lat long zoom title link_text ?>

<a href="https://maps.apple.com/?q=<?php echo urlencode($title).'&amp;z='.$zoom.'&amp;sll='.$lat.','.$long;?>" title="<?php echo $title;?>" class="<?php
	echo $prefix.$tag;?>" rel="external"><?php
	echo $link_text;?>
</a>

<?php
/**
 * End of file maplink.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/maplink.php
 **/