<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

// file align alt caption url
$id			= $prefix.$tag.'-'.basename($file,'.'.pathinfo($file, PATHINFO_EXTENSION));

// if we have a no-border class in align, we need to add that to the image, not the figure element
$no_border	= (strpos($align,'no-border') > -1) ? ' class="no-border"' : '';

$caption	= (empty($caption)) ? NULL : '<figcaption>'.$caption.'</figcaption>';?>

<figure id="<?php echo $id;?>" class="<?php echo $prefix.'align-'.$align.' '.$prefix.$tag;?>"><?php
	// if we have a url, add an anchor tag
	if (! empty($url)) :?>
		<a href="<?php echo $url;?>" rel="external"><?php
	endif;?>

			<img src="<?php echo $enchufe->upload_url_path.'/'.$file;?>" alt="<?php echo $alt;?>"<?php echo $no_border;?> /><?php
	if (!empty($url)) :?>
		</a><?php
	endif;

	echo $caption;?>
</figure>

<?php

/**
 * End of file photo.php
 * Location: ./wp-content/plugins/enchufe/templates/shortcodes/image.php
 **/