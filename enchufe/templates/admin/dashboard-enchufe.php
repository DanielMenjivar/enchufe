<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }?>

<div class="wrap">
	<div id="icon-plugins" class="icon32"><br /></div>

	<h2><?php echo get_admin_page_title();?></h2>

	<h3>Article Metadata</h3><?php

	if (! empty($no_thumbnail)):?>
		<h4>Articles without a Thumbnail</h4><?php
			echo $enchufe->dashboards->list_posts($no_thumbnail);
	endif;

	if (! empty($not_geotagged)):?>
		<h4>Articles Not Geotagged</h4><?php
			echo $enchufe->dashboards->list_posts($not_geotagged);
	endif;

	if (! empty($has_disclaimer)):?>
		<h4>Articles With a Disclaimer</h4><?php
			echo $enchufe->dashboards->list_posts($has_disclaimer);
	endif;

	if (! empty($has_tweet)):?>
		<h4>Articles With Embedded Tweets</h4><?php
			echo $enchufe->dashboards->list_posts($has_tweet);
	endif;?>

	<h3>Subscribers</h3>

	<h4>Subscribers List</h4>
	<p>List of ALL subscribers (active, unverified, cancelled)</p>
	<p>min, avg & max values of 'time to read' (how long after publishing was the article read)</p>
	<p>Includes links to manually verify or cancel a subscriber</p>

	<h4>Manually add a new subscriber without verification</h4>
	<p>Do it!</p>

	<h4>Read Subscriptions</h4>

	<p>Recently read subscriptions (in the last month? latest 10?)</p>

	<h5>Most active subscribers</h5>
	<p>List of top 10 subscribers by amount of articles read including min, avg & max values of 'time to read' (how long after publishing was the article read)</p>

	<h5>Most viewed updates</h5>
	<p>List of most viewed updates (link to articles) including time to read</p>

	<h5>Email Statistics</h5>
	<p>how long after publishing was the article read (min, max, average)</p>

	<div class="description"><p>Help text here.</p></div><?php

	echo Debug::vars($enchufe->settings, $enchufe->settings->get('twitter.twitter_id'));

	// if we have a request for debug_vars in our query string, show it
	if (isset($_GET['debug_vars'])) :
		$vars	= explode(',',urldecode($_GET['debug_vars']));

		foreach ($vars as $key => $variable) :
			echo "<strong>$variable</strong>".Debug::vars($$variable);
		endforeach;
	endif;?>

</div>

<?php
/**
 * End of file dashboard-enchufe.php
 * Location: ./wp-content/plugins/enchufe/templates/admin/dashboard-enchufe.php
 **/