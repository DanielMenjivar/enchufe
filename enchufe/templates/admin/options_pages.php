<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }?>

<div class="wrap">
	<?php screen_icon(); ?>

	<h2><?php echo get_admin_page_title();?></h2>

	<div class="description"><p><?php echo $help_text;?></p></div>

	<form method="post" action="options.php"><?php
		// print out all hidden setting fields
		settings_fields($group);

		// print out all of our sections for this page
		do_settings_sections($page);

		// add a submit button to save changes
		submit_button();?>
	</form>
</div>

<?php
/**
 * End of file options_pages.php
 * Location: ./wp-content/plugins/enchufe/templates/admin/options_pages.php
 **/