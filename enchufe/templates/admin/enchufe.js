(function($) {
	/* for instructions on this, see wp-includes/js/quicktags.dev.js */

	/* add our Audio button */
	QTags.addButton('dm_audio','Audio','[dm_audio file="2013/" title="required" link_text="play the song"]');

	/* add our Background button */
	QTags.addButton('dm_background','Background','<div class="dm-background">\n\n</div>');

	/* add our CDN button */
	QTags.addButton('dm_cdn','CDN','<strong><a href="[dm_cdn]/2013/10/FILENAME.EXT">LINK TEXT</a></strong>');

	/* add our Code button */
	QTags.addButton('dm_code','Code','[dm_code language="php" highlight="1-2, 5, 9-20" offset="40" block="true"]\n\n[/dm_code]');

	/* add our Donate button */
	QTags.addButton('dm_donate','Donate','[dm_donate text="optional – html is allowed"]');

	/* add our Image button */
	QTags.addButton('dm_image','Image','[dm_image file="2013/.jpg" align="left-inline left-2 center no-border" alt="" caption="optional" url="optional"]');

	/* add our Map button */
	QTags.addButton('dm_maplink','Map Link','[dm_maplink lat="required" long="required" zoom="17" title="required" link_text="a map of the area"]\n\n//a.tiles.mapbox.com/v3/MAPBOX-MAPID-HERE/page.html');

	/* add our Photo button */
	QTags.addButton('dm_photo','Photo','[dm_photo file="2013/" extension="optional - defaults to jpg" align="left-inline left-2 center no-border" title="required" caption="optional"]');

	/* add our Place button */
// 	QTags.addButton('dm_place','Place','[dm_place name="" url="optional" address="" city="Toronto" state="ON" phone="+1 (416)" lat="optional" long="optional" zoom="17"]');

	/* add our Review button */
	QTags.addButton('dm_review','Review','[dm_review rating="1.0 - 5.0" name="" url="optional" address="" city="Toronto" state="ON" phone="+1 (416)" zomato_id="optional" dinesafe_id="optional"]');

	/* add our Stars button — see https://dmblog.com/dm-rating-stars-wordpress-plugin */
	QTags.addButton('dm_stars','Stars','[dm_stars rating="5.0" type="optional – defaults to Overall"]');

	/* add our Tweet button */
	QTags.addButton('dm_tweet','Tweet',function() {
		// check out tweet button first
		jQuery('#dm_metadata_hastweet').attr('checked','checked');

		var output = "\n\n***Just paste the HTML embed code from Twitter but remove the <script> tag. Don’t use align=\"center\" but add a tw-align-center class to the blockquote element… You’ll also need to check the \"Tweet embedded in article\" option in the Custom Metadata Box.***\n\n";
		QTags.insertContent(output);
	});

	/* add our Video button */
	QTags.addButton('dm_video','Video','[dm_video file="2013/" align="full" title="required" width="1280" height="720" caption="optional"]');

	/* add our YouTube button */
	QTags.addButton('dm_youtube','YouTube','[dm_youtube id="" align="full" title="required" width="600" height="450" caption="optional"]');

	// set some variables that we can use anywhere
	var prefix	= $('#enchufe').data('prefix'),
		enchufe = {
			prefix				: prefix,
			metabox				: '#'+prefix,
			geoInput			: '#'+prefix+'_coordinates',
			mapID				: 'map',
			mapImage			: '#'+prefix+'_map_container',
			mapPresets			: '.preset-geo',
			showMap				: '.select-geo-map',
			mapboxCSS			: '//api.tiles.mapbox.com/mapbox.js/v0.6.6/mapbox.css',
			mapboxJS			: '//api.tiles.mapbox.com/mapbox.js/v0.6.6/mapbox.js',
			mapboxID			: 'MAPBOX-ID-HERE',
			defaultCoordinates	: $('.toronto').data('coordinates'),
			mapboxStyle			: {
				'position'	: 'absolute',
				'margin'	: 0,
				'padding'	: 0,
				'width'		: 640,
				'top'		: 12,
				'bottom'	: 15
			}
		};

	// hide/show the reminders
	$(enchufe.metabox).on('click','.hide-reminders',function(event) {
		event.preventDefault();

		// toggle our reminders list
		$('.reminders-list').toggle();

		// change the text of our link
		var text = ($(this).text() == 'hide') ? 'show' : 'hide';
		$(this).text(text);
	});

	// populate our geo fields when clicking a preset
	$(enchufe.metabox).on('click',enchufe.mapPresets,function(event) {
		event.preventDefault();

		// get the values of the preset from the element
		var values		= {
			coordinates : $(this).data('coordinates'),
			address		: $(this).data('address')
		};

		// apply the values to the fields
		$.each(values, function(key, value) {
			$('#'+enchufe.prefix+'_'+key).val(value);
		});

		// remove the existing map image if it exists
		$(enchufe.mapImage).empty();
	});

	// when we click our map link…
	$(enchufe.metabox).on('click',enchufe.showMap,function(event) {
		event.preventDefault();

		// create the tag for the mapbox css stylesheet and load it by appending it to the head of the document
		var stylesheet = $('<link rel="stylesheet" href="'+enchufe.mapboxCSS+'" />');
		$('head').append(stylesheet);

		// load our mapbox script
		$.ajax({
			type		: 'GET',
			url			: enchufe.mapboxJS,
			dataType	: 'script',
			context		: $(this),
			cache		: true // allow the script to be cached
		}).done(function() {
			// set some variables
			var tb_map_title	= $(this).attr('title'),
				$map_element	= $('<div id="'+enchufe.mapID+'">'),
				coordinates		= $(enchufe.geoInput).val(),
				have_geo		= (coordinates.length > 0),
				zoom			= (have_geo) ? 15 : 12,		// only if we have coordinates, zoom in all the way
				lat_lon			= (have_geo) ? coordinates.split(',') : enchufe.defaultCoordinates.split(','),	// if have coordinates, use them, otherwise center on default
				location		= {lat: lat_lon[0], lon: lat_lon[1]};

			// show a thickbox with no content
			tb_show(tb_map_title,'#TB_inlinemodalContent?inlineId=hiddenDiv');

			// apply some settings to our map container
			$map_element.css(enchufe.mapboxStyle);

			// add our map containing element to the page
			$('#TB_ajaxContent').append($map_element);

			// load our map
			mapbox.auto(enchufe.mapID,enchufe.mapboxID,function(map,o) {
				// center the map on the supplied coordinates and zoom in
				map.centerzoom(location,zoom);

				// make the map fullscreen and add some padding for the admin bar
				map.ui.fullscreen.add().full();
				$('.map-fullscreen-view #'+enchufe.mapID).css({'margin-top':'30px'});

				// we have to refresh to get accurate points
				map.ui.refresh();

				// enable the point selector UI and notify us of what locations are used
				map.ui.pointselector.addCallback('change',function(drawn,locations) {
					// make double sure that we're going to get accurate points
					map.ui.refresh();

					// show only the last point on the map
					$('.map-point-div').slice(0,-1).remove();

					// use the last location clicked for our geodata
					location = locations[locations.length-1];
					$(enchufe.geoInput).val(location.lat+','+location.lon);

					// remove the existing map image if it exists
					$(enchufe.mapImage).empty();
				}).add();

				// add our location to the map if it was set
				if (have_geo) {
					map.ui.pointselector.addLocation(location);
				}
			});
		});
	});

	// add our button to the media uploader
	wp.media.view.Toolbar.prototype.options = {
		items	: {
			title_thumbs : {
				text		: 'Title Thumbnails',
				priority	: 600,
				requires	: false,
				click		: function() {
					// change our selection to only attachments uploaded to this post
					$('.attachment-filters').val('uploaded').trigger('change');

					// get our attachments as models
					var models	= this.controller.state().get('library').models;

					// since we need ajax to load only the attachments that are attached to this post, it may not have finished loading yet
					if (models.length == 0) {
						alert("Sorry, the attachments weren’t loaded yet, please try again.");
						return;
					}

					// loop through each of our models
					$.each(models, function (index, value) {
						// if this is a large attachment
						if (value.attributes.url.indexOf('_large') > -1) {
							var thumbnail	= value.attributes.url.replace('_large',''),
								title		= value.attributes.title + ' – Thumbnail';

							// loop through all of our models again
							$.each(models, function(index,value) {
								// if our this is our thumbnail then save the title
								if (value.attributes.url == thumbnail)
								{
									value.save('title',title);
								}
							});
						} else {
							// if this is a thumbnail
							if (value.attributes.url.indexOf('_thumb') > -1)
							{
								// mark the thumbnail image as selected (check mark) so we know to title it
								$('.attachment img[src="'+value.attributes.url+'"]').parents('li.attachment').addClass('selected');
							}
						}
					});
				}
			}
		}
	};
}(jQuery));