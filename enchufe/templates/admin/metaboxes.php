<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }?>
<div id="enchufe" data-prefix="<?php echo $box_id;?>"></div>
<p><?php
	if (! $metadata->update_sent())
	{
		echo "An email update hasn’t been sent to subscribers yet.";
	} elseif ($metadata->update_sent() === 'sending') {
		echo "<strong>An email update is currently being sent to subscribers…</strong>";
	} elseif ($metadata->update_sent() === true) {
		echo "An email update was sent to subscribers.";
	} else {
		echo "An email update was sent to <strong>".$metadata->update_sent()."</strong> confirmed subscribers.";
	}?></p>
<p style="text-align:center"><strong>Geodata</strong></p>
<div id="<?php echo $box_id;?>_map_container"><?php
	// only include an image of our map if we have values for the coordinates
	if ($article->is_geotagged())
	{ ?>
		<img src="//api.tiles.mapbox.com/v3/MAPBOX-MAPID-HERE/pin-m-star+5b8dd3(<?php
			echo $article->post_meta('geo_longitude').','.$article->post_meta('geo_latitude');?>)/<?php
			echo $article->post_meta('geo_longitude').','.$article->post_meta('geo_latitude');?>,15/512x256.png" width="256" height="128" /><?php
	} ?>
</div>
<p>
	<label for="<?php echo $box_id;?>_address">Place name: 
		<small style="float:right" data-prefix="<?php echo $box_id;?>">
			<a class="preset-geo calgary" href="#" data-coordinates="51.048233684823494,-114.06316520402525" data-address="Calgary, Alberta">Calgary</a> or 
			<a class="preset-geo toronto" href="#" data-coordinates="43.670295,-79.391525" data-address="Toronto, Ontario">Toronto</a>
		</small>
	</label>
	<input class="widefat" type="text" name="<?php echo $box_id;?>_address" id="<?php echo $box_id;?>_address" value="<?php
		echo $article->post_meta('geo_address');?>" size="30" />
</p>
<p>
	<label for="<?php echo $box_id;?>_coordinates">Coordinates: 
		<span style="float:right"><a href="#" class="select-geo-map" title="Select a location for this article">Show map</a></span>
	</label>
	<input class="widefat" type="text" name="<?php echo $box_id;?>_coordinates" id="<?php echo $box_id;?>_coordinates" value="<?php
		// only output our coodinates if we have both values
		if ($article->is_geotagged())
		{
			echo $article->post_meta('geo_latitude').','.$article->post_meta('geo_longitude');
		} ?>" size="30" />
</p>
<p style="text-align:center"><strong>Metadata</strong></p>
<p>
	<input type="checkbox" name="<?php echo $box_id;?>_featured" id="<?php echo $box_id;?>_featured" value="1" <?php echo $featured;?> /> 
	<label for="<?php echo $box_id;?>_featured" class="selectit">Featured article</label>
</p>
<p>
	<input type="checkbox" name="<?php echo $box_id;?>_classes[]" id="<?php echo $box_id;?>_hastweet" value="has-tweet" <?php echo $tweet_status;?> /> 
	<label for="<?php echo $box_id;?>_hastweet" class="selectit">Tweet embedded in article</label>
</p><?php
/*
<p>
	<input type="checkbox" name="<?php echo $box_id;?>_classes[]" id="<?php echo $box_id;?>_narrow" value="narrow" <?php echo $narrow_status;?> /> 
	<label for="<?php echo $box_id;?>_narrow" class="selectit">Narrow post (old-format images)</label>
</p>
*/
?><p>
	<input type="checkbox" name="<?php echo $box_id;?>_classes[]" id="<?php echo $box_id;?>_disclaimer" value="show-disclaimer" <?php echo $disclaimer_status;?> /> 
	<label for="<?php echo $box_id;?>_disclaimer" class="selectit">Show disclaimer</label>
</p>
<p>
	<label for="<?php echo $box_id;?>_tweet">Tweet ID for this article:</label>
	<input class="widefat" type="text" name="<?php echo $box_id;?>_tweet" id="<?php echo $box_id;?>_tweet" value="<?php
		echo $metadata->tweet();?>" size="30" />
</p>
<p>
	<label for="<?php echo $box_id;?>_timezone">Article’s timezone:</label>
	<input class="widefat" type="text" name="<?php echo $box_id;?>_timezone" id="<?php echo $box_id;?>_timezone" value="<?php
		echo $metadata->timezone()->getName();?>" size="30" />
</p>
<p>
	<label for="<?php echo $box_id;?>_redirect_url">Redirect URL:</label>
	<input class="widefat" type="text" name="<?php echo $box_id;?>_redirect_url" id="<?php echo $box_id;?>_redirect_url" value="<?php
		echo $metadata->redirect_url();?>" size="30" />
</p>
<?php
	/* dont display the reminders anymore, not necessary
		<p style="text-align:center"><strong>Reminders</strong> <small>(<a href="#" class="hide-reminders">hide</a>)</small></p>
		<ul style="list-style-type: disc; list-style-position:inside; margin-bottom:20px;" class="reminders-list">
			<li>don’t use <em>any</em> HTML tags in post excerpts</li>
			<li>use <code>&lt;q&gt;</code> tags for quotes</li>
			<li>use <code>&lt;time&gt;</code> tags for times</li>
			<li>all internal links should be relative</li>
			<li>all external links have <code>rel="external"</code></li>
			<li>most links should have <code>title=""</code> attributes</li>
			<li>use <code>right-inline</code> etc. classes for inline images</li>
			<li>validate markup using W3C’s validator</li>
		</ul>
	*/

/**
 * End of file metaboxes.php
 * Location: ./wp-content/plugins/enchufe/templates/admin/metaboxes.php
 **/