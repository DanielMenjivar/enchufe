<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * Template for displaying a Google Sitemap
 */

echo '<?xml version="1.0" encoding="UTF-8"?>';?>

<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" 
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"><?php

foreach ($posts as $post) :?>

	<url>
		<loc><?php echo $enchufe->home_url.$post->slug;?></loc>
		<lastmod><?php echo $post->last_modified;?></lastmod>
		<priority><?php echo $post->priority;?></priority>
	</url><?php
endforeach;?>

</urlset>