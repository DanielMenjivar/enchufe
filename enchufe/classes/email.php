<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * Build and send email messages.
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */
class Email {

	/**
	 * @var array	A list of headers to send with the message
	 **/
	private $headers = array();

	/**
	 * @var array	A list of mime boundaries to use with the message
	 **/
	private $mime_boundaries = array();

	/**
	 * @var array	Who the email message is from - see the $this->from() method for more info
	 **/
	private $from = array();

	/**
	 * @var array	Who the email message should be sent to - see the $this->to() method for more info
	 **/
	private $to = array();

	/**
	 * @var string	The subject of the email message.
	 **/
	private $subject;

	/**
	 * @var string	The plain text version of the message to send.
	 **/
	private $plain_message;

	/**
	 * @var string	The HTML version of the message to send.
	 **/
	private $html_message;

	/**
	 * @var boolean	Send this message with a high priority header?
	 **/
	private $high_priority = FALSE;

	/**
	 * @var boolean	Send this message with a header identifying it as spam?
	 **/
	private $spam = FALSE;

	/**
	 * @var array	The attachment for the email message - see the $this->attachment() method for more info
	 **/
	private $attachment = array();

	/**
	 * Initialize some properties for our email.
	 *
	 *		$email = new DM_Email;
	 *
	 * @return	void
	 **/
	function __construct()
	{
		$enchufe				= Enchufe::get_instance();

		// set the default “from” for emails to the admin email
		$this->from['name']		= $enchufe->blog_name;
		$this->from['email']	= get_option('admin_email');

		// set the default “to” for emails to the admin email
		$this->to['name']		= $enchufe->blog_name;
		$this->to['email']		= get_option('admin_email');

		$this->subject			= "Online Message from " . $enchufe->blog_name;

		// set some default headers for our message (these can be overriden)
		$this->headers			=  array(
			'Date'				=> date('r'),
			'X-Mailer'			=> $enchufe->blog_name
		);

		// set our main mime boundary
		$this->mime_boundaries	= array(
			'alternative'		=> $this->generate_mime_boundary()
		);
	}

	/**
	 * Create an instance of our email object. This way, we can call our static method and chain properties all in one line.
	 *
	 *		$email = DM_Email::factory();
	 *
	 * @return	$this
	 **/
	public static function factory()
	{
		$class = get_called_class();
		return new $class;
	}

	/**
	 * A magic method to set the values of properties, but only if they exist
	 *
	 *		DM_Email::factory()->subject('My new message subject');
	 *		DM_Email::factory()->high_priority(TRUE);
	 *
	 * @param	string	The name of the method that was called (subject, etc)
	 * @param	string	The arguments that were sent with the requested method
	 * @return	$this
	 **/
	public function __call($name, $arguments)
	{
		// check if the property exists
		 if (property_exists($this, $name))
		 {
			// if there are no arguments/parameters then we should throw an exception
		 	if (empty($arguments))
		 	{
		 		$object = get_class($this);
			 	throw new Exception("You must specify a value for the $name property in $object.");
			// otherwise if we have arguments, SET the value
		 	} else {
		 		$this->$name	= $arguments[0];

				// return $this to allow chaining
		 		return $this;
		 	}
		 // if the property doesn’t exist, we should throw an exception
		 } else {
		 	$object	= get_class($this);
		 	throw new Exception("The property $name does not exist in $object.");
		 }
	}

	/**
	 * Set who the message is from
	 *
	 *		DM_Email::factory()->from('test@example.com','My Name');
	 *
	 * @param	string	The email address of the sender of the message.
	 * @param	string	The name of the sender of the message.
	 * @return	$this
	 **/
	public function from($email,$name)
	{
		$this->from['email']	= $email;
		$this->from['name']		= $name;

		return $this;
	}

	/**
	 * Set who to send the message to
	 *
	 *		DM_Email::factory()->to('test@example.com');
	 *		DM_Email::factory()->to('test@example.com', 'Receipient Name');
	 *
	 * @param	string	The email address for the recipient of the message
	 * @param	string	The name for the recipient of the message (optional)
	 * @return	$this
	 **/
	public function to($email, $name = NULL)
	{
		$this->to['email']		= $email;
		$this->to['name']		= $name;

		return $this;
	}

	/**
	 * Add a header to the message
	 *
	 *		DM_Email::factory()->set_header('X-Mailer','Apple Mail');
	 *
	 * @param	string	The name of the header to set
	 * @param	string	The value to set for the header
	 * @return	$this
	 **/
	public function set_header($name, $value)
	{
		$this->headers[$name]	= $value;

		return $this;
	}

	/**
	 * Set the attachment to include with the message
	 *
	 *		DM_Email::factory()->attachment('/assets/resumes/resume.pdf','application/pdf');
	 *		DM_Email::factory()->attachment('/assets/resumes/resume.pdf','application/pdf','My Resume.pdf');
	 *
	 * @param	string	The relative or complete path to the file that we want to attach.
	 * @param	string	A specfic mime type to use for the attachment
	 * @param	string	A new name to call the file once it’s attached to the email (optional)
	 * @return	$this
	 **/
	public function attachment($file, $mime, $name = NULL)
	{
		if ($file == NULL)
		{
			$this->attachment = NULL;
		} else {
			$this->attachment['file']	= $file;
			$this->attachment['mime']	= $mime;
			$this->attachment['name']	= (empty($name)) ? pathinfo($file, PATHINFO_BASENAME) : $name;
		}

		return $this;
	}

	/**
	 * Generate a string from our array of headers
	 *
	 *		$headers = $this->generate_headers();
	 *
	 * @return	string	A properly formatted string of headers.
	 **/
	private function generate_headers()
	{
		// first add/replace some headers
		$this->headers['From']				= $this->from['name'].' <'.$this->from['email'].'>';
		$this->headers['Message-Id']		= '<'.sha1(microtime(true)).strstr($this->from['email'],'@').'>';
		$this->headers['Mime-Version']		= '1.0';
		$this->headers['Content-Type']		= "multipart/alternative;\n\tboundary=\"".$this->mime_boundaries['alternative'].'"';

		// if this is a high priority email, include an X-Priority header
		if ($this->high_priority === TRUE)
		{
			$this->headers['X-Priority']	= 1;
		}

		// let’s mark this message as spam if that’s what it is
		if ($this->spam === TRUE)
		{
			$this->headers['X-Spam-Status']	= 'Yes, score=6.0';
			$this->headers['X-Spam-Score']	= '60';
			$this->headers['X-Spam-Flag']	= 'YES';
			$this->headers['X-Spam-Bar']	= '++++++';
		}

		// convert our headers into a string
		$headers = '';
		foreach ($this->headers as $key => $value)
		{
			$headers .= $key.': '.$value."\r\n";
		}

		// our last header has to be followed by an empty line
		return $headers."\r\n";
	}

	/**
	 * A helper method to generate a unique mime boundary.
	 *
	 * @return	string	An md5 hash of a unique string
	 **/
	private function generate_mime_boundary()
	{
		return md5(time().uniqid(mt_rand(),true));
	}

	/**
	 * Generate all the mime boundaries and content types for our email message
	 *
	 * @return	string	The mime-encoded body of the email
	 **/
	private function generate_mime_message()
	{
		// include plain text alternative
		$message  = "--".$this->mime_boundaries['alternative']."\r\n";
		$message .= "Content-Transfer-Encoding: quoted-printable\r\n";
		$message .= "Content-Type: text/plain;\n\tcharset=utf-8\r\n";
		$message .= "\r\n";
		$message .= quoted_printable_encode($this->plain_message)."\r\n";
		$message .= "\r\n";
		$message .= "--".$this->mime_boundaries['alternative']."\r\n";

		// if we have an attachment, create a new multipart/mixed content type
		if (! empty($this->attachment))
		{
			// generate a mixed boundary first
			$this->mime_boundaries['mixed']	= $this->generate_mime_boundary();
			$message .= "Content-Type: multipart/mixed;\n\tboundary=\"".$this->mime_boundaries['mixed']."\"\r\n";
			$message .= "\r\n";
			$message .= "--".$this->mime_boundaries['mixed']."\r\n";
		}

		// include html text alternative
		$message .= "Content-Transfer-Encoding: quoted-printable\r\n";
		$message .= "Content-Type: text/html;\n\tcharset=utf-8\r\n";
		$message .= "\r\n";
		$message .= quoted_printable_encode($this->html_message)."\r\n";
		$message .= "\r\n";

		// include our attachment here if we have one
		if (! empty($this->attachment))
		{
			$message .= "--".$this->mime_boundaries['mixed']."\r\n";
			$message .= "Content-Disposition: attachment;\n\tfilename=\"".$this->attachment['name']."\"\r\n";
			$message .= "Content-Type: ".$this->attachment['mime'].";\n\tname=\"".$this->attachment['name']."\"\r\n";
			$message .= "Content-Transfer-Encoding: base64\r\n";
			$message .= "\r\n";
			$message .= chunk_split(base64_encode(implode("", file($this->attachment['file']))))."\r\n";
			$message .= "\r\n";
			// close our multipart/mixed mime boundary here
			$message .= "--".$this->mime_boundaries['mixed']."--\r\n";
			$message .= "\r\n";
		}

		// close our multipart/alternative mime boundary here
		$message .= "--".$this->mime_boundaries['alternative']."--\r\n";
		$message .= "\r\n";

		return $message;
	}

	/**
	 * Send our email and the attachment (if applicable)
	 *
	 *		DM_Email::factory()->send();
	 *
	 * @return	boolean	TRUE or FALSE if the message was accepted for delivery.
	 **/
	public function send()
	{
		// check to make sure we have both plain and html messages to send, otherwise, abort
		if (empty($this->plain_message) OR empty($this->html_message))
		{
			throw new Exception("You must specify both plain text and HTML version of the message to send!");
		} else {
			// set who our message should be sent to
			$to	= (empty($this->to['name'])) ? $this->to['email'] : $this->to['name'].' <'.$this->to['email'].'>';

			// send the message now
			return mail($to, $this->subject, $this->generate_mime_message(), $this->generate_headers(),'-f'.$this->from['email']);
		}
	}
}

/**
 * End of file email.php
 * Location: ./wp-content/plugins/enchufe/classes/email.php
 **/