<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to define our metadata object
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Metadata extends Base_ValueObjects {
	// our object must have all these properties
	public $article_id;
	public $classes;
	public $timezone;
	public $tweet;
	public $update_sent = false;
	public $redirect_url;

	// when serializing this object, only include these variables
	public function __sleep()
    {
        return array('article_id','classes','timezone','tweet','update_sent','redirect_url');
    }

	// get or set our timezone
	public function timezone($timezone = NULL)
	{
		if ($timezone == NULL)
		{
			// if our timezone is empty, use the default timezone
			$timezone = (empty($this->timezone)) ? date_default_timezone_get() : $this->timezone;

			// return our timezone as an object
			return new DateTimeZone($timezone);
		} else {
			// set our timezone - if it’s an object, just get the name
			$this->timezone = (is_object($timezone)) ? $timezone->getName() : $timezone;

			return $this; // allow method chaining
		}
	}

	// create a new tweet
	public function create_tweet(Article $article)
	{
		// get access to our main plugin
		$enchufe		= Enchufe::get_instance();

		// check if we HAD an existing tweet ID already
		$old_tweet_id	= $this->tweet();

		// if we HAD a tweet ID, then delete that tweet
		if (! empty($old_tweet_id))
		{
			$twitter	= Twitter::factory()->delete($old_tweet_id);

			// if delete was unsuccessful, notify the admin
			if (intval($twitter['status']) != 200)
			{
				$enchufe->admin_notice('Unable to delete tweet with id: '.$old_tweet_id,false);
			}
		}

		// now create a new tweet for this post
		/* $status			= $article->post_title()." \r\n\r\n".$article->shortlink(); */
		$status			= $article->post_title()." \r\n\r\n".$article->permalink();

		$twitter		= Twitter::factory()->tweet($status,$article->post_meta('geo_latitude'),$article->post_meta('geo_longitude'));
		$twitter_object	= json_decode($twitter['json']);

		// notify the admin…
		if (intval($twitter['status']) == 200)
		{
			$tweet_id	= $twitter_object->id;
			$this->tweet($tweet_id);
			$article->metadata($this);

			$tweet_link	= 'https://twitter.com/statuses/'.$tweet_id;
			$enchufe->admin_notice('A new tweet was created for <a href="'.$article->permalink().'" target="_blank">'.$article->post_title().'</a>! Here it is: <a href="'.$tweet_link.'" target="_blank">'.$tweet_id.'</a>');
			return $tweet_id;
		} else {
			$enchufe->admin_notice('Unable to create a new tweet for <a href="'.$article->permalink().'" target="_blank">'.$article->post_title().'</a>! Status code <strong>'.$twitter['status'].'</strong> was returned with: '.Debug::vars($twitter_object),false);
			return false;
		}
	}
}

/**
 * End of file metadata.php
 * Location: ./wp-content/plugins/enchufe/classes/metadata.php
 **/