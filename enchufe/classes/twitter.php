<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to automatically send tweets for our posts
 *
 * see also https://dmblog.com/php-code-twitter-cron-job and https://dmblog.com/how-to-use-tweet-entities
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Twitter extends Base_TwitterAPI {
	/**
	 * Override our parent class’ factory method
	 *
	 * @return	$this
	 **/
	public static function factory(array $settings = array())
	{
		$settings = Enchufe::get_instance()->settings;
		$twitter_settings	= array(
			'oauth_access_token'		=> $settings->get('twitter.access_token'),
			'oauth_access_token_secret'	=> $settings->get('twitter.access_token_secret'),
			'consumer_key'				=> $settings->get('twitter.consumer_key'),
			'consumer_secret'			=> $settings->get('twitter.consumer_secret')
		);

		$class = get_called_class();
		return new $class($twitter_settings);
	}

	// send a new tweet
	public function tweet($status,$lat = NULL, $long = NULL)
	{
		$url	= 'https://api.twitter.com/1.1/statuses/update.json';
		$parameters = array(
			'status'	=> html_entity_decode($status),
			'trim_user'	=> true
		);

		if (!empty($lat) AND ! empty($long))
		{
			$parameters['lat']					= $lat;
			$parameters['long']					= $long;
			$parameters['display_coordinates']	= true;
		}

		return $this->request($url,$parameters);
	}

	// delete a tweet
	public function delete($id)
	{
		$url	= 'https://api.twitter.com/1.1/statuses/destroy/'.$id.'.json';
		$parameters = array(
			'trim_user'	=> true
		);

		return $this->request($url,$parameters);
	}
}

/**
 * End of file twitter.php
 * Location: ./wp-content/plugins/enchufe/classes/twitter.php
 **/