<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }
/**
 * Similar to Kohana Views, but more simplistic – capture output from a template
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */
class Assets {

	// render a template with the data provided
	public static function template($filename, array $data = array(), $minify = true)
	{
		// get access to our main class – this will also make it available to the template
		$enchufe			= Enchufe::get_instance();

		// import the template variables to local namespace
		extract($data, EXTR_SKIP);

		// Capture the template output
		ob_start();

		try {
			// Load the template within the current scope
			include $enchufe->plugin_path.'/templates/'.$filename.'.php';
		} catch (Exception $e) {
			// Delete the output buffer
			ob_end_clean();

			// Re-throw the exception
			throw $e;
		}

		// get the captured output and close the buffer
		$output = ob_get_clean();

		// minify our output if requested
		$output	= ($minify) ? $enchufe->minify->minify_html($output) : $output;

		return $output;
	}

	// load a configuration file (a php file with a return value)
	public static function configs($filename)
	{
		$filename	= Enchufe::get_instance()->plugin_path.'/configs/'.$filename.'.php';

		$configs	= include($filename);

		return $configs;
	}
}

/**
 * End of file assets.php
 * Location: ./wp-content/plugins/enchufe/classes/assets.php
 **/