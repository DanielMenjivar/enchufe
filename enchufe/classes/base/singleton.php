<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A base singleton class. All plugin classes should extend this class.
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

abstract class Base_Singleton {
	// all object instances will be stored in this array
	private static $instance = array();

	// make our constructor protected so that new objects can’t be created externally
	protected function __construct() {}

	// provide a method to get a single instance of our class
	public static function get_instance()
	{
		$class = get_called_class();

		// generate a new instance only if one doesn’t already exist
		if ( ! isset(self::$instance[$class]))
		{
			self::$instance[$class] = new $class();
			self::$instance[$class]->initialize();		// now initialize our object
		}

		return self::$instance[$class];
	}

	// all child classes have to include an initialize() function!
	abstract public function initialize();

	// all child classes have an activate and deactivate method, but they can be overridden
	public function activate() {}
	public function deactivate() {}

	// all child classes can access the main plugin object
	protected function enchufe()
	{
		// the main plugin object will always be the first object in the array
		return reset(self::$instance);
	}
}

/**
 * End of file singleton.php
 * Location: ./wp-content/plugins/enchufe/classes/base/singleton.php
 **/