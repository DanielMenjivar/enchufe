<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * An abstract class to add a json feed to WordPress
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

abstract class Base_JsonFeed extends Base_Singleton {
	// store the name of our json file
	public $json_file = '';

	// the priority at which to generate our json file after modifying a post
	public $priority = 99997;

	// all child classes have to include a generate_json function!
	abstract public function generate_json();

	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// set the name of our json file
		$this->json_file = strtolower(get_called_class());

		if ($this->enchufe()->is_backend())
		{
			// make sure our json file is generated everytime we modify a post
			foreach (array('delete_post','publish_post') as $hook)
			{
				add_action($hook,array($this,'save_json'),$this->priority);
			}
		} else {
			// send the name of our feed to our anonymous function, as well as the main plugin object
			$feed			= $this->json_file;
			$enchufe		= $this->enchufe();

			// add our new geojson feed to WordPress
			add_action('init',function() use ($enchufe, $feed) {
				add_feed($feed,array($enchufe->{$feed},'return_json'));

				// add our callback as a valid query string
				add_filter('query_vars',function($vars) {
					$vars[]	= 'callback';
					return $vars;
				});
			});
		}
	}

	// anything here is only run once when the plugin is initially activated
	public function activate()
	{
		// first add our feed right away (don’t wait for the init hook)
		add_feed($this->json_file,array($this,'return_json'));

		// now flush the rewrite rules or it won’t work!
		global $wp_rewrite;
		$wp_rewrite->flush_rules();

		// generate our json file the very first time
		$this->generate_json();
	}

	// anything here is only run once when the plugin is deactivated
	public function deactivate()
	{
		// remove our headlines feed from the rewrite rules since it won’t work with this plugin deactivated
		global $wp_rewrite;
		$feeds = array();

		// loop through our feeds rewrite rules
		foreach ($wp_rewrite->feeds as $feed)
		{
			// add feeds to our array, except for this one
			if ($feed !== $this->json_file)
			{
				$feeds[] = $feed;
			}
		}

		$wp_rewrite->feeds = $feeds;
		$wp_rewrite->flush_rules();
	}

	// get our posts and convert them into json format
	public function save_json()
	{
		// get our data
		$data		= $this->generate_json();

		// make this into a json object now
		$json		= json_encode($data,JSON_NUMERIC_CHECK);

		// cache our json contents
		$success	= $this->enchufe()->cache->cache($this->json_file,$json);

		// only notify the admin in the event of an error
		if (! $success)
		{
			$this->enchufe()->admin_notice('There was an error creating the '.$this->json_file.' JSON feed!',$success);
		}

		return $success;
	}

	// to allow jsonp, we will hook into our feeds to return our json content
	public function return_json()
	{
		// get our cached json and the last modified time of the file
		$cache			= $this->enchufe()->cache;
		$json			= $cache->cache($this->json_file);
		$last_modified	= $cache->last_modified($this->json_file)->setTimezone(new DateTimeZone('GMT'));

		// set our max age for the cache to 7 days (60 seconds * 60 minutes * 24 hours = 1 day)
		$max_age		= 60 * 60 * 24 * 7;

		$cache->send_headers($last_modified,$max_age);

		$callback		= trim(esc_html(get_query_var('callback')));

		if (!empty($callback))
		{
			header("Content-Type: application/x-javascript; charset=utf-8");
			echo "$callback($json);";
		} else {
			header("Content-Type: application/json; charset=utf-8");
			echo $json;
		}
	}
}

/**
 * End of file jsonfeed.php
 * Location: ./wp-content/plugins/enchufe/classes/base/jsonfeed.php
 **/