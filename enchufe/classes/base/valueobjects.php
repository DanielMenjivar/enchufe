<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * An abstract class for value objects. All value object classes should extend this class.
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

abstract class Base_ValueObjects {
	// store a reference to the object we’re extending
	protected $_value_object;

	// make our constructor protected so that new objects can’t be created externally – use the factory() method to create new objects instead
	protected function __construct($value_object)
	{
		/*
			if our object is null, create a new stdClass object
			otherwise type cast our value object into a PHP object
			this will work for arrays and objects, but not anything else
		*/
		$value_object = ($value_object === NULL) ? new StdClass : (object) $value_object;

		// now set our $_value_object variable
		$this->_value_object = $value_object;
	}

	/**
	 * Create an instance of our object. This way, we can call our static method and chain properties all in one line.
	 *
	 *		$object = Object::factory();
	 *
	 * @return	$this
	 **/
	public static function factory($value_object = NULL)
	{
		$class = get_called_class();
		return new $class($value_object);
	}

	/**
	 * A magic method to SET or GET the values of existing properties
	 *
	 *		$this->id();						// get the id of the object
	 *		$this->id(55);						// set the id of the object
	 *
	 * @param	string	The name of the method/property we called
	 * @param	mixed	The value to set the property to. (Optional.)
	 * @return	mixed	Returns $this when setting values to allow chaining, or returns the value when getting.
	 **/
	public function __call($name, $arguments)
	{
		// if there are no arguments/parameters then just GET the value
	 	if (empty($arguments))
	 	{
	 		// if the property exists in this object, then use that value, otherwise get the value from our $_value_object
	 		$value_object = (property_exists($this,$name)) ? $this : $this->_value_object;

	 		return $value_object->$name;
	 	// otherwise if we have arguments, SET the value (locally)
	 	} else {
	 		$this->$name	= $arguments[0];

			// return $this to allow chaining
	 		return $this;
	 	}
	}

	/**
	 * Either SET or GET a date value.
	 *
	 *		$this->date('date');							// get the ‘date’ value as a PHP DateTime object
	 *		$this->date('date',NULL,'America/Toronto')		// get the ‘date’ value as a PHP DateTime object in the ‘America/Toronto’ timezone
	 *		$this->date('date','1980-10-11');				// set the ‘date’ value
	 *		$this->date('date',new DateTime);				// set the ‘date’ value
	 *
	 * @param	string	The name of the property that holds our date value
	 * @param	mixed	Either a DateTime object or a date/time string
	 * @param	string	The name of the timezone to use when create our DateTime object
	 * @return	mixed	Returns $this when setting values to allow chaining, or returns the date value as a PHP DateTime object when getting.
	 **/
	public function date($name, $date = NULL, $timezone = NULL)
	{
		// check if the property exists
		if (property_exists($this, $name) OR property_exists($this->_value_object, $name))
		{
			// if there is no value for $date then just GET the value
		 	if (empty($date))
		 	{
		 		// if our date is already a DateTime object then just return that
		 		// we use ‘@’ here to supress the warning incase it isn’t an object (returns false)
		 		if (@get_class($this->$name()) == 'DateTime')
		 		{
		 			return $this->$name(); // let the _call method decide which object to use for the property (don’t call the property directly)
				// otherwise, let’s convert this to a DateTime object
		 		} else {
		 			// first, let’s create our timezone object
		 			$timezone	= ($timezone == NULL)
		 						? new DateTimeZone(date_default_timezone_get())
		 						: new DateTimeZone($timezone);

		 			return new DateTime($this->$name(),$timezone);
		 		}
			// otherwise if we have arguments, SET the value
		 	} else {
				// if we have a timezone, create a DateTime object, otherwise just use the $date specified
		 		$this->$name	= ($timezone == NULL)
		 						? $date
		 						: new DateTime($date,new DateTimeZone($timezone));

				// return $this to allow chaining
		 		return $this;
		 	}
		// if the property doesn’t exist, we should throw an exception
		} else {
			$object_class = get_class($this);
			throw new Exception("The property ‘$name’ does not exist in $object_class");
		}
	}

	/**
	 * GET a length value
	 *
	 *		$this->length('length');			// get the ‘length’ value as a PHP DateTime object
	 *
	 * @param	string	The name of the property that holds our length value
	 * @return	DateInterval	A PHP DateInterval object representing the length
	 **/
	public function length($name)
	{
		// check if the property exists
		if (property_exists($this, $name) OR property_exists($this->_value_object, $name))
		{
			return new DateInterval('P0000-00-00T'.$this->$name());
		// if the property doesn’t exist, we should throw an exception
		} else {
			$object_class = get_class($this);
			throw new Exception("The property ‘$name’ doesn’t exist in $object_class");
		}
	}
}

/**
 * End of file valueobjects.php
 * Location: ./wp-content/plugins/enchufe/classes/base/valueobjects.php
 **/