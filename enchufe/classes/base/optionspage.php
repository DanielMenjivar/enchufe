<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * An abstract class to add a settings/options page to the WordPress admin.
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

abstract class Base_OptionsPage extends Base_Singleton {
	// store the name of our page
	protected $page;

	// these are our all of our settings configurations
	protected $configs;

	// a suffix to add to our settings pages’ names
	protected $suffix = '_settings';

	// the name of our settings to store in the database
	protected $settings_name;

	// a delimiter to split our sections and field names
	protected $delimiter	= '.';

	// save our settings
	protected $settings = array();

	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// get an array of our configurations
		$this->configs = Assets::configs('options_pages')[$this->page];

		// set the name that we’ll use to store our settings in the db
		$this->settings_name = $this->page.$this->suffix;

		// check to make sure our settings exist, if not, create them
		$saved_settings		= get_option($this->settings_name);
		$default_settings	= array();

		// loop through our sections for each page
		foreach ($this->configs['sections'] as $section_id => $section_content)
		{
			// loop through the fields and add them to our array
			foreach ($section_content['fields'] as $field_id => $values)
			{
				$field_name = $this->generate_field_name($section_id,$field_id);
				$default_settings[$field_name]	= $values['default_value'];
			}
		}

		// if our arrays aren’t the same, merge them and save it to the database
		if ($saved_settings != $default_settings)
		{
			$saved_settings = (empty($saved_settings)) ? array() : $saved_settings;
			// merge our two arrays and only keep values that exist in the configs – keep the new settings though, not the defaults
			$saved_settings	= array_intersect_key($saved_settings,$default_settings) + $default_settings;
			update_option($this->settings_name,$saved_settings);
		}

		// give other classes access to our settings
		$this->settings = $saved_settings;

		if ($this->enchufe()->is_backend())
		{
			// hook into the admin menus to add our settings page
			add_action('admin_menu',function() {
				add_options_page($this->configs['page_title'],$this->configs['menu_title'],'manage_options',$this->page,array($this,'generate_page'));
			});

			// register our settings
			add_action('admin_init',array($this,'register_settings'));
		}
	}

	// generate our options page
	public function generate_page()
	{
		// send some data to our template
		$data['group']		= $this->settings_name;
		$data['page']		= $this->page;
		$data['help_text']	= $this->configs['help_text'];

		// include the template with the actual fields to display
		$output			= Assets::template('admin/options_pages',$data,false);
		echo $output;
	}

	// register our settings and create our fields for the page
	public function register_settings()
	{
		// register just one setting per page – each page is one row in the database (an array of settings)
		register_setting($this->settings_name,$this->settings_name,array($this,'sanitize_input'));	// all inputs use the sanitize_input method

		// register our sections
		foreach ($this->configs['sections'] as $section_id => $section_content)
		{
			$help_text	= $section_content['help_text'];

			// add the required sections to our page
			add_settings_section($section_id,$section_content['title'],function() use ($help_text) {
				echo '<p>'.$help_text.'</p>';
			},$this->page);

			// create our fields
			foreach ($section_content['fields'] as $field_id => $values)
			{
				// we also need to pass the section name to the input sanitizer callback
				$field_name	= $this->generate_field_name($section_id,$field_id);

				// add_settings_field( $id, $title, $callback, $page, $section, $args );
				add_settings_field($field_id,$values['title'],array($this,'print_fields'),$this->page,$section_id,array(
					'name'	=> $this->settings_name.'['.$field_name.']',
					'value'	=> get_option($this->settings_name)[$field_name],
					'type'	=> $values['type']
				));
			}
		}
	}

	// a helper method to generate our field name
	private function generate_field_name($section,$field)
	{
		return $section . $this->delimiter . $field;
	}

	// all of our fields go through this function
	public function sanitize_input($input)
	{
		// loop through all values in our input array
		foreach ($input as $key => $value)
		{
			list($section,$field_id)	= explode($this->delimiter,$key);
			$callback	= $this->configs['sections'][$section]['fields'][$field_id]['callback'];

			// run our callback on the input value
			$input[$key]= call_user_func($callback, $value);
		}
		return $input;
	}

	// all of our fields are created using this method
	public function print_fields($args)
	{
		extract($args);

		switch ($type)
		{
			default:
				echo "<input type='$type' name='$name' value='$value' size='48' />";
		}
	}

	// give other classes access to our settings
	public function get($field_id)
	{
		return $this->settings[$field_id];
	}
}

/**
 * End of file optionspage.php
 * Location: ./wp-content/plugins/enchufe/classes/base/optionspage.php
 **/