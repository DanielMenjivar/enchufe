<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to interact with Twitter’s API
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Base_TwitterAPI {
	private $oauth_access_token;
	private $oauth_access_token_secret;
	private $consumer_key;
	private $consumer_secret;
	private $resource_url;
	private $parameters = array();
	private $method = 'POST';
	protected $oauth;
	public $response = array();

	// make our constructor protected so that new objects can’t be created externally
	protected function __construct(array $settings)
	{
		if (!isset($settings['oauth_access_token'])
				OR !isset($settings['oauth_access_token_secret'])
				OR !isset($settings['consumer_key'])
				OR !isset($settings['consumer_secret']))
		{
			throw new Exception('Make sure you are passing in the correct parameters!');
		}

		$this->oauth_access_token			= $settings['oauth_access_token'];
		$this->oauth_access_token_secret	= $settings['oauth_access_token_secret'];
		$this->consumer_key					= $settings['consumer_key'];
		$this->consumer_secret				= $settings['consumer_secret'];
	}

	/**
	 * Create an instance of our twitter object. This way, we can call our static method and chain properties all in one line.
	 *
	 *		$twitter = Twitter::factory($settings);
	 *
	 * @return	$this
	 **/
	public static function factory(array $settings)
	{
		$class = get_called_class();
		return new $class($settings);
	}

	// set our parameters (GET/POST fields)
	private function parameters(array $values)
	{
		// if we have a status…
		if (isset($values['status']) && substr($values['status'],0,1) === '@') {
				// if we have a status beginning with '@', we have to add a null character to the begining to make curl happy
				// see stackoverflow.com/questions/17497363/twitter-api-giving-blank-response-if-tweet-starts-wil-username
				$values['status'] = sprintf("\0%s", $array['status']);
			}
		}

		// set our parameters now
		$this->parameters	= $values;

		// allow method chaining
		return $this;
	}

	/**
	 * Private method to generate the base string used by cURL
	 * 
	 * @return string Built base string
	 */
	private function build_base_string()
	{
		// for get requests, we have to merge in our parameters too
		$base_values	= ($this->method == 'POST') ? $this->oauth : array_merge($this->oauth,$this->parameters);
		$values			= array();

		// sort our array by key
		ksort($base_values);

		foreach($base_values as $key => $value) {
			$values[]	= "$key=".rawurlencode($value);
		}

		return $this->method.'&'.rawurlencode($this->resource_url).'&'.rawurlencode(implode('&',$values));
	}

    /**
     * Build the Oauth object using params set in construct and additionals
     * passed to this method. For v1.1, see: https://dev.twitter.com/docs/api/1.1
     * 
     * @return Instance of self for method chaining
     */
	private function build_oauth()
	{
		$this->oauth	= array(
			'oauth_consumer_key'		=> $this->consumer_key,
			'oauth_nonce'				=> time(),
			'oauth_signature_method'	=> 'HMAC-SHA1',
			'oauth_token'				=> $this->oauth_access_token,
			'oauth_timestamp'			=> time(),
			'oauth_version'				=> '1.0'
		);

		$base_info						= $this->build_base_string();

		// build our oauth signature
		$composite_key					= rawurlencode($this->consumer_secret).'&'.rawurlencode($this->oauth_access_token_secret);
		$this->oauth['oauth_signature']	= base64_encode(hash_hmac('sha1',$base_info,$composite_key,true));
		ksort($this->oauth);

		// allow method chaining
        return $this;
	}

	/**
	 * Private method to generate authorization header used by cURL
	 * 
	 * @return string $return Header used by cURL for request
	 */
	private function build_auth_header()
	{
		$auth_header	 = 'Authorization: OAuth ';
		$values			 = array();

		foreach($this->oauth as $key => $value) {
			$values[]	 = "$key=\"".rawurlencode($value)."\"";
		}

		return $auth_header . implode(', ',$values);
	}

    /**
     * Perform the actual data retrieval from the API
     * 
     * @return string json Returns json data.
     */
    public function request($url, array $parameters = array(),$method = NULL)
    {
		// set our properties
		$this->resource_url	= $url;
		$this->method		= (empty($method)) ? $this->method : $method;

		// set our parameters and create our oauth
		$this->parameters($parameters)->build_oauth();

		$headers			= array($this->build_auth_header(),'Expect:');
		$options			= array(
			CURLOPT_HTTPHEADER		=> $headers,
			CURLOPT_HEADER			=> false,
			CURLOPT_URL				=> $this->resource_url,
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_SSL_VERIFYPEER	=> true,
			CURLOPT_SSL_VERIFYHOST	=> 2,
/* 			CURLOPT_VERBOSE			=> true, */
			CURLOPT_FOLLOWLOCATION	=> true,
			CURLOPT_NOBODY			=> false,
			CURLOPT_TIMEOUT			=> 30
		);

		// just send post fields for POST requests
		if ($this->method == 'POST')
		{
			$options[CURLOPT_POSTFIELDS]	 = $this->parameters;
		} else {
			// append a query string for GET requests, if we have parameters
			if (! empty($this->parameters))
			{
				$options[CURLOPT_URL]		.= '?'.http_build_query($this->parameters);
			}
		}

		$feed	= curl_init();
		curl_setopt_array($feed,$options);

		// try getting the data from twitter
		try {
			$this->response['json']		= curl_exec($feed);
			$this->response['info']		= curl_getinfo($feed);
			$this->response['status']	= $this->response['info']['http_code'];
			curl_close($feed);
		} catch (Exception $e) {
			$this->response				= false;
		}

		return $this->response;
	}
}

/**
 * End of file twitterapi.php
 * Location: ./wp-content/plugins/enchufe/classes/base/twitterapi.php
 **/