<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * An extension of the post class…
 *
 * see https://dmblog.com/extending-the-wp_post-class
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Article extends Base_ValueObjects {
	// get the url for our author
	public function author_url()
	{
		// set the user id
		$id		= $this->post_author();

		$url	= get_author_posts_url($id);
		return $url;
	}

	// get the name of our author
	public function author_name($display = NULL)
	{
		// set the user id
		$id		= $this->post_author();

		if ($display == 'display')
		{
			// use the display name
			$name	= get_the_author_meta('display_name',$this->post_author());
		} else {
			// use the full name regardless of what is set as the display name - DEFAULT
			$name	= get_the_author_meta('first_name',$id).' '.get_the_author_meta('last_name',$id);
		}
		return $name;
	}

	/**
	 * Either SET or GET a post meta value
	 *
	 *		$this->post_meta('class');			// get the 'dm_class' post meta
	 *		$this->post_meta('class',$value)	// set the 'dm_class' post meta
	 *
	 * @param	string	The name of the post meta value
	 * @param	string	The value to set our post meta to
	 * @return	mixed	Returns $this when setting values to allow chaining, or returns the post meta value when getting.
	 **/
	public function post_meta($name, $value = NULL)
	{
		// if there is no value then just GET the value
		if (empty($value))
		{
			$post_meta = get_post_meta($this->ID(),$name,true);

			return $post_meta;
		// otherwise if we have a value, let’s set it
		} else {
			// update our post meta (or set it if it doesn’t yet exist)
			update_post_meta($this->ID(),$name,$value);

			// return $this to allow chaining
			return $this;
		}
	}

	// get or set our metadata
	public function metadata(Metadata $data = NULL)
	{
		// append our prefix to the name. Also, by appending '_', it makes sure that this field isn’t visible as a dropdown in the ‘custom meta’ box on the post edit screen
		$name		= '_'.Enchufe::get_instance()->prefix.'metadata';

		// if we have no values to set
		if($data == NULL)
		{
			// first get our metadata
			$metadata	= $this->post_meta($name);

			// unserialize our metadata, but if none exists, create a new Metadata object
			$metadata	= (empty($metadata)) ? Metadata::factory() : unserialize($metadata);

			return $metadata;
		} else {
			// serialize then set our metadata
			$data		= serialize($data);
			$this->post_meta($name,$data);
			return $this;	// allow method chaining
		}
	}

	// get our post date in the post’s specified timezone
	public function post_date()
	{
		// first get our date in GMT
		$date		= $this->date('post_date_gmt',NULL,'GMT');

		// get the timezone of the post
		$timezone	= $this->metadata()->timezone();

		// now let’s get our date in the post’s timezone
		$date		= $date->setTimezone($timezone);

		return $date;
	}

	// get our modified date (always in GMT)
	public function modified_date()
	{
		// get our modified date in GMT
		return $this->date('post_modified_gmt',NULL,'GMT');
	}

	// get our post’s thumbnail image — see https://dmblog.com/custom-iphone-icons
	public function thumbnail()
	{
		$thumb_id	= get_post_thumbnail_id($this->ID());

		// if there isn’t an article thumbnail then use the default one
		$thumb		= (! empty($thumb_id))
					? wp_get_attachment_url($thumb_id)
					: '/a/u/dm.png';
		return $thumb;
	}

	// get the permalink to our post
	public function permalink()
	{
		return get_permalink($this->ID());
	}

	// get the shortlink to our post
	public function shortlink()
	{
		return Enchufe::get_instance()->home_url.'a/'.$this->metadata()->article_id();
	}

	// determine whether this post is geo-tagged or not
	public function is_geotagged()
	{
		$is_geotagged = ($this->post_meta('geo_latitude') !='' AND $this->post_meta('geo_longitude') !='');
		return $is_geotagged;
	}

	// get the category for this post
	public function category()
	{
		return get_the_category($this->ID())[0]->slug;
	}
}

/**
 * End of file article.php
 * Location: ./wp-content/plugins/enchufe/classes/article.php
 **/