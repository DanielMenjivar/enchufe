<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to generate our comments as a standalone HTML file
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Comments extends Base_Singleton {
	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		if (! $this->enchufe()->is_backend())
		{
			// we want to provide our anonymous function with access to our main plugin object
			$enchufe		= $this->enchufe();

			// add our new comments feed to WordPress (get access to $enchufe from inside the anonymous function)
			add_action('init',function() use ($enchufe) {
				add_feed('comments',array($enchufe->comments,'return_comments'));
			});

			// intercept our comment when it’s posted to do some stuff
			add_action('comment_post',array($this,'process_comment'),20, 2);
		}
	}

	// anything here is only run once when the plugin is initially activated
	public function activate()
	{
		// first add our feed right away (don’t wait for the init hook)
		add_feed('comments',array($this,'return_comments'));

		// now flush the rewrite rules or it won’t work!
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
	}

	// anything here is only run once when the plugin is deactivated
	public function deactivate()
	{
		// remove our comments feed from the rewrite rules since it won’t work with this plugin deactivated
		global $wp_rewrite;
		$feeds = array();

		// loop through our feeds rewrite rules
		foreach ($wp_rewrite->feeds as $feed)
		{
			// add feeds to our array, except for the comments feed
			if ($feed !== 'comments')
			{
				$feeds[] = $feed;
			}
		}

		$wp_rewrite->feeds = $feeds;
		$wp_rewrite->flush_rules();
	}

	// return our comments as minified HTML
	public function return_comments()
	{
		// only allow this feed on single posts and only for ajax requests
		if ( ! (is_single() AND $this->enchufe()->is_ajax()))
		{
			wp_redirect('..');
		}

		while (have_posts())
		{
			the_post();

			// start buffering to minify our HTML and make our links relative
			$this->enchufe()->minify->start_buffer();
			$this->enchufe()->minify->start_buffer('relative_links');

			// load up the comment template - we can’t cache this since there’s too many variables involved (just getting comments, posted and pending moderation, etc.)
			comments_template(NULL,true);
		}
	}

	// process our comments when they’re submitted
	public function process_comment($comment_ID, $comment_status)
	{
		// if our commenter wants to subscribe to email updates…
		if (isset($_POST['subscribe']) AND $comment_status !== 'spam')
		{
			$email	= (isset($_POST['email'])) ? is_email(trim($_POST['email'])) : false;

			if ($email)
			{
				// add them to the subscribers table and send them the confirmation email
				$this->enchufe()->subscriptions->add($email);
			}
		}

		// if this comment was submitted using ajax
		if ($this->enchufe()->is_ajax())
		{
			switch ($comment_status)
			{
				case '0':	// awaiting moderation
					// modify moderator of unapproved comment
					wp_notify_moderator($comment_ID);
				case '1':	// approved comment – this should never happen really, all comments require moderation
					echo 'success';	// in both cases (no break) we echo success
					break;
				default:	// also used for spam comments
					echo 'error';
					break;
			}
			exit;
		}
	}
}

/**
 * End of file comments.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/comments.php
 **/