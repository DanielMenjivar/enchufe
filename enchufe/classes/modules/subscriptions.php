<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to handle our email subscribers…
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Subscriptions extends Base_Singleton {
	// store a reference to wpdb
	private $wpdb;

	// the name of our subscribers table
	private $subscribers_table;

	// the name of our read subscriptions table
	private $read_subscriptions_table;

	// the name of our custom action hook
	private $action_hook = 'enchufe_subscriptions';

	// the name of the query string to look for that has our subscriber
	private $read_subscription_query = 'track_subscriber';

	// initialize our subscriptions, this is called in get_instance()
	public function initialize()
	{
		// register our action with wordpress so wp_cron can find it
		add_action($this->action_hook,array($this,'send_update'));

		// make wpdb available to this class
		global $wpdb;
		$this->wpdb		= $wpdb;

		// set the name of our subscribers table
		$this->subscribers_table		= $this->wpdb->prefix.'subscribers';

		// set the name of our read subscriptions table
		$this->read_subscriptions_table	= $this->wpdb->prefix.'read_subscriptions';

		if ($this->enchufe()->is_backend())
		{
			// make sure we email subscribers whenever we publish a new post
			add_action('publish_post',array($this,'schedule_delivery'),99999);
		}

		// if this is a request for a blank png to track a subscriber, then run the track method
		if (isset($_GET[$this->read_subscription_query]))
		{
			$this->track();
		}
	}

	// anything here is only run once when the plugin is initially activated
	public function activate()
	{
		// use dbdelta to create our subscribers table if it doesn’t exist, or modify it to use this structure if it does
		$sql		= "CREATE TABLE $this->subscribers_table (
							ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
							email varchar(100) NOT NULL,
							guid varchar (32) NOT NULL,
							status int(2) NOT NULL DEFAULT '0',
							last_modified timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
							date_added timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
							PRIMARY KEY (ID),
							UNIQUE KEY email (email),
							UNIQUE KEY guid (guid),
							KEY status (status)
						);";
		require_once(ABSPATH.'wp-admin/includes/upgrade.php');
		dbDelta($sql);

		// use dbdelta to create our read subscriptions table if it doesn’t exist, or modify it to use this structure if it does
		$sql2		= "CREATE TABLE $this->read_subscriptions_table (
							ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
							subscriber_id bigint(20) UNSIGNED NOT NULL,
							post_id bigint(20) UNSIGNED NOT NULL,
							date_added timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
							PRIMARY KEY (ID),
							CONSTRAINT rs_post_fk FOREIGN KEY (post_id) REFERENCES wp_posts (ID) ON UPDATE CASCADE ON DELETE NO ACTION,
							CONSTRAINT rs_subscriber_fk FOREIGN KEY (subscriber_id) REFERENCES wp_subscribers (ID) ON UPDATE CASCADE,
							UNIQUE post_subscriber (subscriber_id, post_id),
							INDEX post_id (post_id),
							INDEX subscriber_id (subscriber_id)
						) ENGINE=InnoDB;";

		// check to see if the database has already been updated
		$already_updated = (bool) get_option('enchufe_db_updated');

		// if we’ve already updated the database then we have nothing more to do
		if ($already_updated) { return; }

		// add our initial subscribers
		$subscribers	= array(
			'foo@example.com'	=> '2007-12-05 00:00:00',
			'bar@example.com'	=> '2008-07-27 00:00:00'
		);
		foreach ($subscribers as $email => $date_added)
		{
			// insert the records
			$this->insert(array('email' => $email, 'status' => 1, 'date_added' => $date_added));
		}

		// if this is not the production server, then remind the admin to change the status of subscribers for testing
		if (defined(WP_DEBUG) AND WP_DEBUG)
		{
			$message	= "Don’t forget to change the status of subscribers to unverified before beginning your testing!";
			$this->enchufe()->admin_notice($message,false);
		}
	}

	// schedule our email subscription email to go out five minutes after posting
	public function schedule_delivery($post_ID)
	{
		if($_POST['post_status'] == 'publish' AND $_POST['original_post_status'] == 'publish')
		{
			// this is just an edited post, not a new one, so just return
			return;
		}

		// set the time for our email to be sent to five minutes from now
		$scheduled_time = time() + (5 * 60);

		// first make sure there are no other email updates pending (FOR THIS POST) by clearing them out
		wp_clear_scheduled_hook($this->action_hook,array($post_ID));

		// now schedule our email to be sent in five minutes from now
		wp_schedule_single_event($scheduled_time,$this->action_hook,array($post_ID));

		// now queue an admin message verifying what time the email will be sent at
		$message = 'An update will be sent to email subscribers at '.date('g:i:sa T \o\n l, F j, Y',$scheduled_time);
		$this->enchufe()->admin_notice($message);
	}

	public function send_update($post_ID)
	{
		// get access to our main plugin object
		$enchufe	= $this->enchufe();

		// get our post and extend it as an article
		$post		= get_post($post_ID); $article = Article::factory($post);

		// get our custom metadata for this post
		$metadata	= $article->metadata();

		// if an update has already been sent (or is currently being sent)
		if ($metadata->update_sent())
		{
			// send a message to the admin saying that the subscription wasn’t re-sent
			$reason	= ($metadata->update_sent() == 'sending') ? 'was already in the process of being sent.' : 'had already been sent to '.$metadata->update_sent().' email subscribers in the past.';
			$admin_message = 'An email update for <strong><em>'.$article->post_title().'</em></strong> was <strong>not</strong> re-sent because it '.$reason;
			$enchufe->admin_notice($admin_message,false);
			return;
		} else {
			// prevent a duplicate cron job from sending it again by setting a flag
			$metadata->update_sent('sending');
			$article->metadata($metadata);	// save to the db
		}

		// send our article to the template
		$data		= array('article' => $article);

		// get our list of verified subscribers
		$subscribers = $this->wpdb->get_results($this->wpdb->prepare("SELECT email, guid FROM $this->subscribers_table WHERE status = %d",1));

		// keep track of how many emails were actually successfully sent
		$sent_emails	= 0;
		$failed_emails	= array();

		//loop through our subscribers sending emails one at a time
		foreach ($subscribers as $subscriber)
		{
			// add our guid to our template variables
			$data['guid']		= $subscriber->guid;

			// generate an unsubscribe URL for the email header
			$unsubscribe_url	= $enchufe->home_url . 'subscribe/?cancel=' . $subscriber->guid . '&utm_source=' . str_replace(' ','+',$enchufe->blog_name) . '+Updates&utm_medium=email&utm_content=cancel&utm_campaign=' . $article->post_name();

			// load our template and send the email
			$html_message		= Assets::template('email/update',$data);
			$plain_message		= Assets::template('email/update-plain',$data,false);
			$email_sent			= Email::factory()
									->subject($enchufe->blog_name.' Updates')
									->to($subscriber->email)
									->set_header('List-Unsubscribe','<'.$unsubscribe_url.'>')
									->plain_message($plain_message)
									->html_message($html_message)
									->send();

			// if the email failed, we should keep track of the email address
			if ($email_sent === true)
			{
				$sent_emails++;
			} else {
				$failed_emails[]= $subscriber->email;
			}
		}

		// send a message to the admin saying how many people got the email
		$update_sent	= $sent_emails.' of '.count($subscribers);
		$admin_message	= 'An email update for <strong><em>'.$article->post_title().'</em></strong> was sent to <strong>'.$update_sent
							.'</strong> confirmed email subscribers on '.date('l, F j, Y \a\t g:i:sa T');
		$admin_message	= (empty($failed_emails)) ? $admin_message : $admin_message.'. The following email addresses failed: <strong>'
							.implode(', ',$failed_emails).'</strong>';
		$success		= (empty($failed_emails));
		$enchufe->admin_notice($admin_message,$success);

		// update our metadata to keep track of how many subscribers got this update
		$metadata->update_sent($update_sent);
		$article->metadata($metadata);	// save to the db

		return;
	}

	public function add($email)
	{
		// clean the email and check if it’s valid
		$email = is_email($email);

		// if it’s not a valid email just quit now
		if ($email == false)
		{
			return false;
		}

		// check if we have this subscriber in the table already
		$subscriber = $this->wpdb->get_row($this->wpdb->prepare("SELECT * FROM $this->subscribers_table WHERE email = %s;",$email));

		if ($subscriber != NULL)
		{
			switch ($subscriber->status)
			{
				case 1:
					// if our subscriber is already verified, no need to do anything. This may have been triggered when posting a comment
					return $subscriber->email;
					break;
				case -1:
					// if our subscriber is cancelled, then we’ll change their status to pending and continue
					$this->update_status($subscriber->guid,0);
					break;
				default:
					// if our subscriber is pending they may have added themselves months ago and forgot to verify, but will now follow through if we resend the email…
					$last_modified = strtotime($subscriber->last_modified);
					if (time()-$last_modified < 60 * 60 * 12)
					{
						// if this was last modified in the last twelve hours then don’t send the verification message again (to reduce email abuse)
						return $subscriber->email;
					} else {
						// we’re going to continue sending the email, but first let’s update the last modified time of the subscriber so we’re not sending emails too often
						$this->wpdb->update($this->subscribers_table, array('last_modified' => date('Y-m-d H:i:s')), array('guid' => $subscriber->guid), '%s', '%s');
					}
			}
		} else {
			// they’re not in the database so we should add them now
			$this->insert(array('email' => $email,'date_added' => date('Y-m-d H:i:s')));

			// get the record we just added
			$subscriber	= $this->wpdb->get_row($this->wpdb->prepare("SELECT * FROM $this->subscribers_table WHERE email = %s;",$email));
		}

		// if we’ve gotten this far, then we can now send the email asking them to verify their address
		$data			= array('guid' => $subscriber->guid);
		$html_message	= Assets::template('email/verify',$data);
		$plain_message	= Assets::template('email/verify-plain',$data,false);
		$email			= Email::factory()
							->subject('Please activate your subscription to '.$this->enchufe()->blog_name)
							->to($subscriber->email)
							->plain_message($plain_message)
							->html_message($html_message)
							->send();

		return $subscriber->email;
	}

	public function cancel($guid)
	{
		// first make sure this is a valid record
		$subscriber	= $this->get_subscriber_by_guid($guid);

		// if we have a valid subscriber
		if ($subscriber != NULL)
		{
			// if our subscriber is not already cancelled
			if ($subscriber->status != -1)
			{
				// update our subscriber’s status
				$this->update_status($subscriber->guid,-1);

				// email them to say goodbye and to make sure they meant to cancel (in case they forwarded the email to someone and they cancelled them…)
				$data			= array();
				$html_message	= Assets::template('email/cancel',$data);
				$plain_message	= Assets::template('email/cancel-plain',$data,false);
				$email			= Email::factory()
									->subject($this->enchufe()->blog_name.' Subscription Cancelled')
									->to($subscriber->email)
									->html_message($html_message)
									->plain_message($plain_message)
									->send();

				// send a message to the admin to notify them that someone cancelled
				$admin_message = 'Hey, just thought you should know that <strong>'.$subscriber->email.'</strong> cancelled their email subscription on '
									.date('l, F j, Y \a\t g:i:sa T').". Don’t cry or anything…";
				$this->enchufe()->admin_notice($admin_message,false);
			}
			return $subscriber->email;
		}

		// otherwise this isn’t a valid subscriber, so verification failed
		return false;
	}

	// verify our subscriber
	public function verify($guid)
	{
		// first make sure this is a valid record
		$subscriber	= $this->get_subscriber_by_guid($guid);

		// if we have a valid subscriber
		if ($subscriber != NULL)
		{
			// if our subscriber is not already verified
			if ($subscriber->status != 1)
			{
				// update our subscriber’s status
				$this->update_status($subscriber->guid,1);

				// send a message to the admin to notify them that we have a new subscriber
				$admin_message = 'Hey, just thought you should know that <strong>'.$subscriber->email.'</strong> verified their email subscription on '
									.date('l, F j, Y \a\t g:i:sa T').". <em>Sticky wicked!</em>";
				$this->enchufe()->admin_notice($admin_message);
			}

			return $subscriber->email;
		}

		// otherwise this isn’t a valid subscriber, so verification failed
		return false;
	}

	// get a subscriber by guid
	public function get_subscriber_by_guid($guid)
	{
		// clean our guid and limit it to 32 characters
		$guid 		= substr(esc_attr(sanitize_text_field($guid)),0,32);

		// get our subscriber from the database
		$subscriber	= $this->wpdb->get_row($this->wpdb->prepare("SELECT * FROM $this->subscribers_table WHERE guid = %s;",$guid));

		return $subscriber;
	}

	// update the status of our subscriber
	private function update_status($guid, $status)
	{
		// clean our guid and limit it to 32 characters
		$guid 		= substr(esc_attr(sanitize_text_field($guid)),0,32);

		// make sure our status is an integer
		$status		= intval($status);

		// update our subscriber $wpdb->update( $table, $data, $where, $format = null, $where_format = null );
		$updated = $this->wpdb->update($this->subscribers_table, array('status' => $status), array('guid' => $guid), '%d', '%s');

		return (bool) $updated;
	}

	// insert our record into the database
	private function insert(array $fields)
	{
		// generate our guid and add it to our query – 128bytes * 8 bit = 1024 bits, pretty random indeed
		$fields['guid']		= md5(openssl_random_pseudo_bytes(128));

		// now insert the record
		$this->wpdb->insert($this->subscribers_table, $fields);
	}

	// track an email subscriber (email update)
	private function track()
	{
		// get the values from our query string
		$guid		= sanitize_text_field($_GET[$this->read_subscription_query]);
		$post_ID	= sanitize_text_field($_GET['p']);

		// first make sure this is a valid record
		$subscriber	= $this->get_subscriber_by_guid($guid);

		// if we have a valid subscriber
		if ($subscriber != NULL)
		{
			// check if we have this subscriber in the table already
			$already_read = $this->wpdb->get_row($this->wpdb->prepare("SELECT * FROM $this->read_subscriptions_table WHERE subscriber_id = %d AND post_id = %d;",$subscriber->ID,$post_ID));

			//  if we’ve already logged that this subscriber read the email…
			if ($already_read != NULL)
			{
				// set $date_added to the time when the email was first read
				$date_added	= new DateTime($already_read->date_added);
			} else {
				// set $date_added to now
				$date_added = new DateTime('now');

				// insert our record into the database
				$this->wpdb->insert(
					$this->read_subscriptions_table,
					array(
						'subscriber_id'	=> $subscriber->ID,
						'post_id'		=> $post_ID,
						'date_added'	=> $date_added->format('Y-m-d H:i:s')
					),
					array(
						'%d',
						'%d',
						'%s'
					)
				);
			}
		}

		// send headers to allow caching for up to one day - expires one day from now and last_modified is set to FIRST time the email was read (maybe now?)
		$max_age	= 60 * 60 * 24; // 60 seconds x 60 minutes x 24 hours = 1 day
		$this->enchufe()->cache->send_headers($date_added,$max_age);

		// send a blank gif
		header('Accept-Ranges: bytes');
		header('Content-Type: image/gif');
		echo base64_decode('R0lGODlhAQABAHAAACH5BAUAAAAALAAAAAABAAEAAAICRAEAOw==');
		exit;
	}
}

/**
 * End of file subscriptions.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/subscriptions.php
 **/