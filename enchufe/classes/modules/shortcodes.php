<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class for our shortcodes…
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Shortcodes extends Base_Singleton {
	// these are the only valid shortcodes
	private $shortcodes;

	// initialize our shortcodes, this is called in get_instance()
	public function initialize()
	{
		// get an array of our shortcodes and their defaults
		$this->shortcodes = Assets::configs('shortcodes');

		// make sure all shortcodes are processed on RSS feeds and comments
		/* add_filter('the_excerpt_rss', 'do_shortcode'); */
		/* add_filter('the_content_rss', 'do_shortcode'); */
		add_filter('comment_text', 'shortcode_unautop',31);		// wpautop is run at level 30, so now we need to remove the p tags around our shortcodes
		add_filter('comment_text', 'do_shortcode',32);			// now we can process our shortcodes (after wpautop, and after shortcode_unautop)

		// loop through our shortcodes and register them
		foreach ($this->shortcodes as $shortcode => $defaults)
		{
			// append our prefix to the shortcode tag name
			$shortcode = $this->enchufe()->prefix.$shortcode;

			// register this shortcode with WordPress and direct it to our process() method
			add_shortcode($shortcode,array($this,'process'));
		}
	}

	// all of our shortcodes go through this function
	public function process($atts,$content,$tag)
	{
		// remove our prefix from our shortcode name
		$tag		= str_replace($this->enchufe()->prefix,'',$tag);

		// get the defaults for our shortcode
		$defaults	= $this->shortcodes[$tag];

		// update our count of occurences of this shortcode
		$this->shortcodes[$tag]['occurrences']++;

		/* Take the attributes of our shortcode and merge them with our defaults.
		 * Any array key not in our defaults (but present in the shortcode) will be ignored
		 *
		 * All of our attributes will be available in the template as variables
		 */
		$data		= shortcode_atts($defaults,$atts);

		// add some additional variables to the template
		$data['tag']		= $tag;
		$data['content']	= $content;

		// don’t lazy load the first couple images – set on the settings page – (and never lazy load images if an admin is logged in
		$lazy_load_threshold = $this->enchufe()->settings->get('media.lazy_load');
		$data['lazy_load']	= ($this->shortcodes[$tag]['occurrences'] > $lazy_load_threshold AND ! current_user_can('administrator'));
		$data['prefix']		= str_replace('_','-',$this->enchufe()->prefix);

		// load our template
		$template	= 'shortcodes/'.$tag;
		$html		= Assets::template($template,$data,false);

		return $html;
	}
}

/**
 * End of file shortcodes.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/shortcodes.php
 **/