<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to generate our archives in json format
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Archives extends Base_JsonFeed {
	// the priority at which to generate our json file after modifying a post
	public $priority = 99997;

	// get our posts and convert them into json format
	public function generate_json()
	{
		// how many posts to retrieve
		$number_posts	= 999;

		// use the wordpress get_posts() function to get the number of posts specified above
		$posts			= get_posts(array('numberposts' => $number_posts, 'offset' => 10));
		$index			= 0;
		$json			= array();

		// make each post an object within our $json array
		foreach ($posts as $post)
		{
			// use our article object…
			$article	= Article::factory($post);

			// add our post as an object to our json array
			$json[$index]		= new StdClass;
			$json[$index]->t	= $article->post_title();
			$json[$index]->d	= $article->post_date()->format('Y-m-d');
			$json[$index]->p	= str_ireplace('/',':',str_ireplace($this->enchufe()->upload_url_path.'/','',$article->thumbnail()));
			$json[$index]->s	= str_ireplace($this->enchufe()->home_url,'',$article->permalink());
			$index++;
		}

		return $json;
	}
}

/**
 * End of file archives.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/archives.php
 **/