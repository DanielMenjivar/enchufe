<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class for caching our content and sending cache headers…
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Cache extends Base_Singleton {
	// store the path to our cache folder
	private $folder;

	// include a constructor function so that we can use Cache::cache() and not have PHP think it’s a constructor
	protected function __construct() {}

	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// set the path to our cache folder
		$enchufe		= $this->enchufe();
		$this->folder	= $enchufe->plugin_path.'/cache/';

		if (! $enchufe->is_backend())
		{
			// initialize output buffering (get access to our main plugin’s class from inside the anonymous function)
			add_action('wp',function() use ($enchufe) {
				// only do this for single posts, not ajax requests, and NOT WHEN A USER IS LOGGED IN
				if (is_single() AND ! $enchufe->is_ajax() AND ! is_user_logged_in() AND ! is_attachment() AND ! is_feed())
				{
					// start buffering
					ob_start(array($enchufe->cache,'article_headers'));
				}
			},2,0);
		}
	}

	// send additional headers for our single articles to allow client-side caching
	public function article_headers($content)
	{
		// get access to our article class for this article
		global $post; $article = Article::factory($post);

		$last_modified	= $article->modified_date();
		$max_age		= 60 * 60 * 24 * 30; // one month

		// send our headers
		$this->send_headers($last_modified,$max_age);

		return $content;
	}

	// generate our headers for caching
	public function send_headers($last_modified, $max_age = NULL)
	{
		// make sure our last modified is in GMT if it isn’t already
		$last_modified	= $last_modified->setTimezone(new DateTimeZone('GMT'));

		// if no max_age is specified, then set it to one day (60 seconds x 60 minutes x 24 hours)
		$max_age		= ($max_age == NULL) ? 60 * 60 * 24 : $max_age;

		// set our expiry date and make sure it’s in GMT
		$expires		= new DateTime('+'.$max_age.' seconds');
		$expires		= $expires->setTimezone(new DateTimeZone('GMT'));

		$date_format	= 'D, d M Y H:i:s \G\M\T';

		header("Last-Modified: {$last_modified->format($date_format)}");
		header("Cache-Control: public, max-age=$max_age");
		header("Expires: {$expires->format($date_format)}");

		return $this;
	}

	// get or set our cache
	public function cache($name,$data = NULL)
	{
		$filename		= $this->folder.$name.'.txt';

		// if we have no content, then just GET the cache
		if ($data == NULL)
		{
			if(is_file($filename))
			{
				// Return the cache
				try
				{
					return unserialize(file_get_contents($filename));
				} catch (Exception $e) {
					// Cache is corrupt, let return happen normally.
				}
			}
			// Cache not found
			return NULL;
		}

		// Force the data to be a string
		$data = serialize($data);

		try
		{
			// Write the cache
			return (bool) file_put_contents($filename, $data, LOCK_EX);
		} catch (Exception $e) {
			// Failed to write cache
			return FALSE;
		}
	}

	// delete our cache file
	public function delete($name)
	{
		$filename		= $this->folder.$name.'.txt';

		if (is_file($filename))
		{
			return unlink($filename);
		}
		// if not a file, then just return false
		return false;
	}

	// get or set the last modified time of the cache file
	public function last_modified($name,$time = NULL)
	{
		$filename		= $this->folder.$name.'.txt';

		// if we don’t have a time, then just get the date
		if ($time == NULL)
		{
			if (is_file($filename))
			{
				// return the time as a DateTime object
				try
				{
					$timezone		= new DateTimeZone(date_default_timezone_get());
					$last_modified	= new DateTime(NULL,$timezone);
					$last_modified	= $last_modified->setTimestamp(filemtime($filename));

					return $last_modified;
				} catch (Exception $e) {
					// file is corrupt let return happen normally
				}
			}

			// cache not found
			return NULL;
		}

		// otherwise let’s set the time
		try
		{
			// if this is a datetime object, convert it to a timestamp
			$time = (is_object($time)) ? $time->getTimestamp() : $time;

			// set the last modified time
			return touch($filename,$time);
		} catch (Exception $e) {
			// failed to update the time
			return false;
		}
	}
}

/**
 * End of file cache.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/cache.php
 **/