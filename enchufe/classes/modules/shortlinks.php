<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class for handling shortlinks
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class shortlinks extends Base_Singleton {
	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// if this is a shortlink request, then redirect to the correct post
		if (isset($_GET['article_id']))
		{
			$article_id = $_GET['article_id'];

			// get our shortlinks from cache
			$shortlinks	= $this->get_shortlinks();

			// get our permalink
			$permalink = (isset($shortlinks[$article_id])) ? $shortlinks[$article_id] : $this->enchufe()->home_url;

			header('HTTP/1.1 301 Moved Permanently');
			header('Location: '.$permalink);
			exit;
		}

		// make sure our shortlinks are cached everytime we modify a post
		if ($this->enchufe()->is_backend())
		{
			foreach (array('delete_post','publish_post') as $hook)
			{
				add_action($hook,array($this,'cache_shortlinks'),99996);
			}
		}

		// change our shortlinks to use a a/article_id instead of a ?p= query string
		add_filter('get_shortlink',function($shortlink)
		{
			global $post; $article = Article::factory($post);

			$shortlink = ($article->post_status() == 'publish' AND ! is_page()) ? $article->shortlink() : str_replace('?p=','p/',$shortlink);

			return $shortlink;
		});
	}

	// cache our shortlink mappings
	public function cache_shortlinks()
	{
		// use the wordpress get_posts() function to get all our posts
		$posts		= get_posts('numberposts=999999');
		$shortlinks = array();

		foreach($posts as $post)
		{
			// get our article ID for the post
			$article	= Article::factory($post);
			$article_id	= $article->metadata()->article_id();

			// add the article to our array
			$shortlinks[$article_id]	= $article->permalink();
		}

		// now save/cache the shortlinks
		$this->enchufe()->cache->cache('shortlinks',$shortlinks);
	}

	// get the next available ID for a shortlink
	public function get_next_id()
	{
		$shortlinks = $this->get_shortlinks();
		$max_id		= max(array_keys($shortlinks));

		return $max_id + 1;
	}

	// get an array of all our shortlinks
	public function get_shortlinks()
	{
		$shortlinks	= $this->enchufe()->cache->cache('shortlinks');
		return $shortlinks;
	}
}

/**
 * End of file shortlinks.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/shortlinks.php
 **/