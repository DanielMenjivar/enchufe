<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to add a settings/options page for this plugin to the WordPress admin.
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Settings extends Base_OptionsPage {
	// store the name of our page
	protected $page = 'enchufe';

	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// call our parent’s initialize method
		parent::initialize();

		// anything we want to add, we can do here
	}
}

/**
 * End of file settings.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/settings.php
 **/