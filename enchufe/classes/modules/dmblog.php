<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * Optimizations for WordPress
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class dmblog extends Base_Singleton {
	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// set the default timezone to Toronto – we have to do this since WordPress’ functions ignore timezones altogether
		$default_timezone	= get_option('timezone_string');
		date_default_timezone_set($default_timezone);

		// perform our wordpress optimizations once everything else is loaded
		add_action('plugins_loaded',array($this,'optimize_wordpress'));
	}

	// this is run only once when the plugin is initially activated
	public function activate()
	{
		// create a new Moderator role that allows comment editing
		remove_role('moderator');
		$capabilities = array(
			'level_2'				=> true,  // we just keep this for backwards compatibility and to show these users in the posts screen dropdown
			'read'					=> true,
			'delete_posts'			=> true,
			'edit_posts'			=> true,
		//	'publish_posts'			=> true,
			'upload_files'			=> true,
			'edit_published_posts'	=> true,
			'edit_others_posts'		=> true,
			'unfiltered_html'		=> true,
			'moderate_comments'		=> true,
			'unfiltered_upload'		=> true,
			'edit_comment'			=> true
		);
		add_role('moderator','Moderator', $capabilities);

		// remove embeds rewrite rules on plugin activation
		add_filter('rewrite_rules_array',array($this,'disable_embeds_rewrites'));
		flush_rewrite_rules();
	}

	// this is run only once when the plugin is deactivated
	public function deactivate()
	{
		// re-add embeds rewrite rules on plugin deactivation
		remove_filter('rewrite_rules_array',array($this,'disable_embeds_rewrites'));
		flush_rewrite_rules();
	}

	// apply our site-wide optimizations
	public function optimize_wordpress()
	{
		// increase the memory limit for WordPress
		/*
			define('WP_MEMORY_LIMIT','128M');
			define('WP_MAX_MEMORY_LIMIT','256M');
		*/

		// register our own custom theme directory
		$theme_directory = $this->enchufe()->plugin_path.'/themes';
		register_theme_directory($theme_directory);

		// remove certain filters from WordPress so we can code posts cleanly in HTML
		$filters = array('the_content','the_content_feed','the_excerpt','wp_title','the_title','single_post_title','the_title_attribute','comment_text');

		foreach ($filters as $hook)
		{
			// remove wpautop from everything, except from comment text
			if ($hook != 'comment_text')
			{
				remove_filter($hook,'wpautop');
			}

			// remove wptexturize from everything to stop changing quotation marks and other characters to &# entities
			remove_filter($hook,'wptexturize');
		}

		// hook into our footer to print our database queries… (only works on development servers, and only for administrators)
		add_action('wp_footer',array($this,'print_queries'));
		add_action('admin_footer',array($this,'print_queries'));

		// add a link to the admin bar to validate the page via W3’s validator
		add_action('admin_bar_menu',function($wp_admin_bar)
		{
			global $wp;

			$wp_admin_bar->add_node(array(
				'id'	=> 'dm-validator',
				'title'	=> 'W3 Validator',
				/*  'href'	=> '//validator.w3.org/check?uri=referer'	// https doesn’t work and referrer isn’t sent from https to http */
				'href'	=> 'http://validator.w3.org/nu/?showoutline=yes&showimagereport=yes&doc='.home_url($wp->request)
			));

			// if this is a single post, put a link to our shortlink
			if (is_single())
			{
				$wp_admin_bar->add_node(array(
					'id'	=> 'dm-shortlink',
					'title'	=> 'Shortlink',
					'href'	=> wp_get_shortlink()
				));
			}
		},2000);

		// remove the shortlink in the header
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

		// Remove the embed query var.
		global $wp;
		if (is_array($wp->public_query_vars))
		{
			$wp->public_query_vars = array_diff($wp->public_query_vars, array('embed'));
		}

		// remove the REST API endpoint
		remove_action('rest_api_init','wp_oembed_register_route');

		// turn off oEmbed auto discovery
		add_filter('embed_oembed_discover','__return_false');

		// don’t filter oEmbed results
		remove_filter('oembed_dataparse','wp_filter_oembed_result',10);

		// remove oEmbed discovery links
		remove_action('wp_head','wp_oembed_add_discovery_links');

		// remove oEmbed-specific JavaScript from the front-end and back-end
		remove_action('wp_head','wp_oembed_add_host_js');
		add_filter('tiny_mce_plugins',function($plugins) {
			return array_diff($plugins,array('wpembed'));
		});

		// remove all embeds rewrite rules
		add_filter('rewrite_rules_array',array($this,'disable_embeds_rewrites'));

		// apply our other optimizations depending on if it’s a backend or frontend request…
		if ($this->enchufe()->is_backend())
		{
			$this->optimize_backend();
		} else {
			$this->optimize_frontend();
		}
		return $this; // allow method chaining
	}

	// apply our backend optimizations
	private function optimize_backend()
	{
		// PCRE *has to* be increased or we get tons of errors in the apache logs about WordPress’ ajax.php stuff
		ini_set('pcre.recursion_limit',1000000);

		// turn off post revisions to keep our database slim
		if (! defined('WP_POST_REVISIONS')) { define('WP_POST_REVISIONS',false); }

		// increase the autosave interval to five minutes, up from the default of 60 seconds
		if (! defined('AUTOSAVE_INTERVAL')) { define('AUTOSAVE_INTERVAL',300); }

		// add contact methods for users
		add_filter('user_contactmethods',function($contact_methods) {
			$contact_methods['googleplus']	= 'Google Plus';
			$contact_methods['twitter']		= 'Twitter';
			return $contact_methods;
		},10,1);

		// allow HTML in profile bios
		remove_filter('pre_user_description','wp_filter_kses');

		// remove the buttons already on the quicktags bar
		add_filter('quicktags_settings',function($qtInit)
		{
			$qtInit['buttons'] = 'fullscreen'; // only include these buttons (The default buttons are “strong,em,link,block,del,ins,img,ul,ol,li,code,more,spell,close”)
			return $qtInit;
		},10,1);

		// add our scripts to the admin pages
		add_action('admin_enqueue_scripts',function($hook)
		{
			// add our scripts to the “new post” and “edit post” pages
			if ($hook == 'post-new.php' OR $hook == 'post.php') {
				// add our scripts only to the “new post” and “edit post” pages
				wp_enqueue_script('enchufe',$this->enchufe()->plugin_url.'/templates/admin/enchufe.js',array('jquery','quicktags'),NULL,true);
			}
		},10,1);

		// fix our uploads by moving the caption to the proper field
		add_action('add_attachment', function ($post_ID) {
			// this is triggered after an attachment is uploaded and before the attachment form gets displayed
			$post = get_post($post_ID);
			$post->post_excerpt = $post->post_content;
			$post->post_content	= NULL;

			wp_update_post($post);
		});

		return $this; // allow method chaining
	}

	// apply our frontend optimizations
	private function optimize_frontend()
	{
		// remove some wordpress headers
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('wp_print_styles', 'print_emoji_styles');
		remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
		remove_action('wp-head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
		remove_action('wp_head', 'rel_canonical');
		add_filter( 'wpseo_canonical', '__return_false' );

		// remove pages from search results (actually, just allow ‘post’ post types
		add_filter('pre_get_posts',function($query) {
			if ($query->is_search())
			{
				$query->set('post_type','post');
			}

			return $query;
		});

		// calculate our page title
		add_filter('wp_title',function($title,$sep) {
			global $paged, $page;

			if (! is_feed())
			{
				// uppercase our first character (mostly used for tags)
				$title = ucfirst($title);

				// add a page number if necessary
				if ($paged >= 2 OR $page >= 2)
				{
					$title .= 'Page '.max($paged,$page)." $sep ";
				}

				// add the site name
				//$title .= $this->enchufe()->blog_name;	// as of PHP 5.4 we have access to $this inside anonymous functions
				$suffix = (is_home()) ? $this->enchufe()->blog_name : 'DM';
				$title .= $suffix;
			}
			return $title;
		},10,2);

		return $this; // allow method chaining
	}

	// print our queries used (requires define('SAVEQUERIES',true) in wp-config.php, which is the default on development servers (as modified by DM)
	public function print_queries()
	{
		if (current_user_can('administrator') AND SAVEQUERIES === TRUE AND WP_DEBUG === TRUE)
		{
			global $wpdb;
			echo '<div style="text-align:center"><p>'.get_num_queries().' queries in <strong>'.timer_stop(0,3).' seconds</strong></p></div>';
		} else {
			echo '<!-- '.get_num_queries(). ' queries in '.timer_stop(0,3).' seconds -->';
		}
	}

	// remove all rewrite rules related to embeds
	function disable_embeds_rewrites($rules)
	{
		foreach ($rules as $rule => $rewrite)
		{
			if (false !== strpos($rewrite,'embed=true'))
			{
				unset($rules[$rule]);
			}
		}

		return $rules;
	}
}

/**
 * End of file dmblog.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/dmblog.php
 **/