<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to add dashboards for this plugin to the WordPress admin.
 *
 * @package		dmblog.com
 * @author		Daniel Menjivar
 */

class Dashboards extends Base_Singleton {
	// these are our all of our dashboard configurations
	private $configs;

	// save our dashboards
	private $dashboards = array();

	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// get an array of our dashboard configurations
		$this->configs = Assets::configs('dashboards');

		if ($this->enchufe()->is_backend())
		{
			// hook into the admin menus to add our settings pages
			add_action('admin_menu',function() {
				foreach ($this->configs as $page => $defaults)
				{
					add_dashboard_page($defaults['page_title'],$defaults['menu_title'],$defaults['capability'],$page,function() use ($page)
					{
						echo $this->generate_page($page);
					});
				}
				$this->dashboards[$page]	= $defaults;
			});
		}
	}

	// generate our options page
	public function generate_page($page)
	{
		// generate the data for our template
		$callback	= $this->dashboards[$page]['data_callback'];
		$data		= $this->{$callback}();

		// send some data to our template
		$data['page']		= $page;
		$template			= 'admin/dashboard-'.$page;

		// include the template with the actual fields to display – don’t minify the output
		$output			= Assets::template($template,$data,false);
		return $output;
	}

	// a helper function for our main dashboard
	public function main_dashboard()
	{
		$metadata_field			= '_'.$this->enchufe()->metabox->box_id;

		$filters = array (
			// data_name => $conditions
			'no_thumbnail'		=> array("meta_key = '_thumbnail_id'"),
			'not_geotagged'		=> array("meta_key = 'geo_latitude'","meta_value != ''"),
			'has_disclaimer'	=> array("meta_key = '$metadata_field'","meta_value NOT LIKE '%show-disclaimer%'"),
			'has_tweet'			=> array("meta_key = '$metadata_field'","meta_value NOT LIKE '%has-tweet%'")
		);

		foreach ($filters as $key => $conditions)
		{
			$data[$key]			= $this->missing_metadata($conditions);
		}

		return $data;
	}

	// a helper function to find posts with missing metadata
	public function missing_metadata($args)
	{
		global $wpdb;
		$and	= '';
		foreach ($args as $conditions)
		{
			$where	.= $and.$conditions;
			$and	= ' AND ';
		}
		$query			= "SELECT post_id FROM $wpdb->postmeta WHERE ".$where;

		$exclude_posts	= $wpdb->get_col($query);

		$posts		= get_posts(array('post__not_in'=>$exclude_posts,'numberposts'=>999));

		return $posts;
	}

	// a helper function to list posts
	public function list_posts($posts)
	{
		$output	= '<ol>';

		foreach ($posts as $post)
		{
			$article	= Article::factory($post);
			$edit_link	= get_edit_post_link($article->ID());
			$title		= $article->post_title();
			$date		= $article->post_date()->format('F j, Y');
			
			$output		.= '<li><a href="'.$edit_link.'">'.$title.'</a> ('.$date.')</li>';
		}
		
		$output	.= '</ol>';

		return $output;
	}
}

/**
 * End of file dashboards.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/dashboards.php
 **/