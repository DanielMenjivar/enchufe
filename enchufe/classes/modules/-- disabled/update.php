<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * This module updates the database ONLY the very first time the plugin is activated
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Update extends Base_Singleton {
	// initialize our plugin, this is called in get_instance()
	public function initialize() { }

	// this is run only once when the plugin is initially activated
	public function activate()
	{
		// check to see if the database has already been updated
		$already_updated = (bool) get_option('enchufe_db_updated');

		// if we’ve already updated the database then we have nothing to do
		if ($already_updated) { return; }

		// initialize some variables
		global $wpdb;
		$enchufe				= Enchufe::get_instance();
		$upload_path			= wp_upload_dir();
		$upload_path			= $upload_path['basedir'].'/'; // get an absolute path to our uploads folder
		$old_prefix				= '[dm';
		$new_prefix				= '['.$enchufe->prefix;
		$old_meta_key			= 'dm_class';
		$moved_toronto			= new DateTime('January 5th, 2010'); // the day we moved to Toronto - all posts before this will get an Alberta timezone
		$this->shortcode_files	= array(); // set up an empty array to collect the files used in the dm_photo shortcode

		// add a function temporarily to our dm_photo shortcode to get a list of the thumbnail files we need to process
		remove_all_shortcodes(); // since our shortcodes are loaded by the shortcode class’ initialize method, we want to remove them for now
		add_shortcode('dm_photo',function($atts,$content,$tag) use ($enchufe) {
			$defaults['file']		= NULL;
			$defaults['extension']	= 'jpg';
			$attributes = shortcode_atts($defaults,$atts);

			$enchufe->update->shortcode_files[] = $attributes['file'].'.'.$attributes['extension']; // store our file with extension
		});

		// update all of our shortcodes in the database - both posts and comments
		$rows_updated	 = $wpdb->query($wpdb->prepare("UPDATE wp_posts SET post_content = REPLACE(post_content,%s,%s);",$old_prefix,$new_prefix));
		$rows_updated	+= $wpdb->query($wpdb->prepare("UPDATE wp_comments SET comment_content = REPLACE(comment_content,%s,%s);",$old_prefix,$new_prefix));

		// now load all of our posts to do some stuff
		$posts			= get_posts('numberposts=9999');
		$old_disclaimer	= $new_prefix.'disclaimer]';		// remember, our SQL query above will have already changed this to the new format

		// loop through our posts
		foreach($posts as $post)
		{
			// initialize some variables
			$article	= Article::factory($post);
			$timezone	= '';
			$metadata	= $article->metadata(); // gets our existing metadata if it exists, or if not, a new Metadata object is setup…
			$classes	= $article->post_meta($old_meta_key); // get our existing classes

			// any posts with a disclaimer shortcode should use the new method of adding a class
			if (strpos($article->post_content(),$old_disclaimer) !== false)
			{
				// add this to our list of classes
				$classes = $classes.' show-disclaimer';
			}

			// if we have classes, add them to our metadata
			if (! empty($classes))
			{
				$metadata	= $metadata->classes(trim($classes));

				// delete our old metadata
				delete_post_meta($article->ID(),$old_meta_key);
			}

			// every post should have a timezone – if our article date is before we moved to toronto, use an Alberta timezone
			$timezone	= ($article->post_date() < $moved_toronto) ? 'America/Edmonton' : 'America/Toronto';
			$metadata	= $metadata->timezone($timezone);

			// now don’t forget to save our metadata
			$article->metadata($metadata);

			// run our shortcode function on the post content to append to our list of files
			if (strpos($article->post_content(),'[dm_photo') !== false)
			{
				do_shortcode($article->post_content());
			}
		}

		// now that every post has been processed, we can safely remove all the disclaimer shortcodes
		$rows_updated	+= $wpdb->query($wpdb->prepare("UPDATE wp_posts SET post_content = REPLACE(post_content,%s,%s);",$old_disclaimer,''));

		// make all our internal links relative
		$rows_updated	+= $wpdb->query($wpdb->prepare("UPDATE wp_posts SET post_content = REPLACE(post_content,%s,%s);",'href="'.$enchufe->home_url.'/','href="/'));

		/*** modify our filenames to work with the new photo shortcodes ***/
		$files = array_unique($this->shortcode_files);

		// try to increase our memory limit
		@ini_set("memory_limit",'256M');

		foreach ($files as $file)					// file = 2012/10/omnibot2000_2.jpg
		{
			// this can take a while, so lets remove our time limit
			set_time_limit(0);

			$pathinfo	= pathinfo($file);			// dirname = 2012/10, basename = omnibot2000_2.jpg, extension = jpg, filename = omnibot2000_2
			$name		= $pathinfo['filename'];
			$extension	= '.'.$pathinfo['extension'];
			$large_name	= $name.'_large';
			$thumb_name = $name.'_thumb';
			$path		= $upload_path.$pathinfo['dirname'].'/';
			$large_file	= $path.$large_name.$extension;
			$thumb_file	= $path.$thumb_name.$extension;
			$filename	= $path.$name.$extension;

			// rename our files in the file system
			rename($filename, $large_file);	// append _large to the big photo
			rename($thumb_file, $filename);	// remove _thumb from the small photo

			// get our post IDs for the two images
			$small_meta_value		= $pathinfo['dirname'].'/'.$name.$extension;
			$thumb_meta_value		= $pathinfo['dirname'].'/'.$thumb_name.$extension;
			$large_meta_value		= $pathinfo['dirname'].'/'.$large_name.$extension;
			$large_attachment_id	= $wpdb->get_var($wpdb->prepare("SELECT post_id FROM wp_postmeta WHERE meta_value = %s;",$small_meta_value));
			$small_attachment_id	= $wpdb->get_var($wpdb->prepare("SELECT post_id FROM wp_postmeta WHERE meta_value = %s;",$thumb_meta_value));

			// rename our large image in the database
			$rows_updated	+= $wpdb->query($wpdb->prepare("UPDATE wp_posts SET guid = REPLACE(guid,%s,%s) WHERE ID = %d;",
								$name,$large_name,$large_attachment_id));
			$rows_updated	+= $wpdb->query($wpdb->prepare("UPDATE wp_postmeta SET meta_value = %s WHERE post_id = %d AND meta_key = '_wp_attached_file'",
								$large_meta_value,$large_attachment_id));
			// rename our thumbnail image in the database
			$rows_updated	+= $wpdb->query($wpdb->prepare("UPDATE wp_posts SET post_name = %s, guid = REPLACE(guid,%s,%s) WHERE ID = %d;",
								$name,$thumb_name,$name,$small_attachment_id));
			$rows_updated	+= $wpdb->query($wpdb->prepare("UPDATE wp_postmeta SET meta_value = %s WHERE post_id = %d AND meta_key = '_wp_attached_file'",
								$small_meta_value,$small_attachment_id));

			// update our attachment metadata
			$small_attach_data		= wp_generate_attachment_metadata($small_attachment_id,$filename);
				wp_update_attachment_metadata($small_attachment_id,$small_attach_data);
			$large_attach_data		= wp_generate_attachment_metadata($large_attachment_id,$large_file);
				wp_update_attachment_metadata($large_attachment_id,$large_attach_data);
		}

		// all of our attachments should have a caption and not a description
		$rows_updated	+= $wpdb->query($wpdb->prepare("UPDATE wp_posts SET post_excerpt = post_content, post_content = %s WHERE post_type= %s;",'','attachment'));

		// now update our option in the database so that this isn’t run again
		update_option('enchufe_db_updated',$rows_updated);

		// and print an admin message letting us know how many rows were updated
		$admin_message = intval($rows_updated) . " records were updated in the database. <em>That’s why</em> you had to wait so long! It’s all good now though.";
		$enchufe->admin_notice($admin_message);

		return $this; // allow method chaining
	}
}

/**
 * End of file update.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/update.php
 **/