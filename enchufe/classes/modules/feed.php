<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class for changing the default RSS Feed
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class feed extends Base_Singleton {
	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// add our feed to WordPress
		$enchufe	= $this->enchufe();
		add_action('init',function() use ($enchufe) {
			add_feed('rss2',array($enchufe->feed,'customize_feed'));
		});

		// remove our feeds from the header mostly for the comments feed. we can add the main one manually
		remove_action('wp_head','feed_links',2);
		remove_action('wp_head','feed_links_extra',3);
	}

	// use our own template for the RSS feed
	public function customize_feed()
	{
		// load the template
		$data		= array();
		$feed_xml	= Assets::template('feed',$data);

		// output some headers
		header('Content-Type: ' . feed_content_type('rss2') . '; charset=' . get_option('blog_charset'), true);

		// return the output
		echo $feed_xml;
	}

	// anything here is only run once when the plugin is initially activated
	public function activate()
	{
		// first add our feed right away (don’t wait for the init hook)
		add_feed('rss2',array($this,'customize_feed'));

		// now flush the rewrite rules or it won’t work!
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
	}

	// anything here is only run once when the plugin is deactivated
	public function deactivate()
	{
		// remove our feed from the rewrite rules since it won’t work with this plugin deactivated
		global $wp_rewrite;
		$feeds = array();

		// loop through our feeds rewrite rules
		foreach ($wp_rewrite->feeds as $feed)
		{
			// add feeds to our array, except for this one
			if ($feed !== 'rss2')
			{
				$feeds[] = $feed;
			}
		}

		$wp_rewrite->feeds = $feeds;
		$wp_rewrite->flush_rules();
	}
}

/**
 * End of file feed.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/feed.php
 **/