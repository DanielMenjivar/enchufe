<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to minify our HTML.
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Minify extends Base_Singleton {
	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// initialize output buffering
		add_action('wp',function() {
			/*
			 * don’t filter rss feeds or a robots.txt request
			 * if an admin is logged in, we can still preview HTML nicely
			 * we can’t check if this is a feed earlier since our query hasn’t run yet, so it has to be here inside this action
			*/
			if (is_feed() OR is_robots() OR (current_user_can('administrator') AND ! $this->enchufe()->is_ajax()))
			{
				return;
			} else {
				// start buffering
				$this->enchufe()->minify->start_buffer();
			}
		},1,0);
	}

	// start a buffer to minify our HTML - other modules can also use this method to minify their HTML output
	public function start_buffer($callback = 'minify_html')
	{
		ob_start(array($this,$callback));
	}

	// make links relative
	public function relative_links($html)
	{
		$replacements	= array(
			'href="'.$this->enchufe()->home_url			=> 'href="/',
			'http:'.$this->enchufe()->upload_url_path	=> $this->enchufe()->upload_url_path,
			'https:'.$this->enchufe()->upload_url_path	=> $this->enchufe()->upload_url_path
		);

		return strtr($html,$replacements);
	}

	/**
	 * Buffer callback. Can also be used independently.
	 *
	 * @param string $html Current contents of the output buffer.
	 * @return string New buffer contents.
	 */
	public function minify_html($html)
	{
		$replacements	= array(
			'	'					=> '',					// get rid of tabs
			"\r"					=> '',
			"\r\n"					=> '',
			"\n"					=> '',
			PHP_EOL					=> '',					// get rid of new lines
			'/comments/?replytocom='=> '/?replytocom=',		// our ajax comments might have /comments/ appended, so remove it
			'/comments/#respond'	=> '#respond',			// our ajax comments might have /comments/ appended, so remove it
			'rel="category tag"'	=> 'rel="tag"'			// for validation reasons
		);

		return strtr($html,$replacements);
	}
}

/**
 * End of file minify.php
 * Location: ./wp-content/plugins/enchufe/classes/minify.php
 **/