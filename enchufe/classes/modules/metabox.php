<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to generate our metabox on the edit post screen
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Metabox extends Base_Singleton {
	// store the id of our box
	public $box_id;

	// store the action of our nonce
	private $nonce_action;

	// store the name of our nonce
	private $nonce_name;

	// store the name of our featured post meta field
	public $featured_meta;

	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// set some variables
		$this->box_id			= $this->enchufe()->prefix.'metadata';
		$this->nonce_action 	= get_class();
		$this->nonce_name		= $this->box_id.'_nonce';
		$this->featured_meta	= '_'.$this->enchufe()->prefix.'featured_post';

		/* add our script to the post edit screen –– we’re concatenating our scripts into enchufe.js instead since they’re all on the posts page
		add_action('admin_enqueue_scripts',function($hook)
		{
			if ($hook == 'post-new.php' OR $hook == 'post.php')
			{
				// add our geodata script to the new post and edit post pages
				wp_enqueue_script('enchufe_metabox',Enchufe::get_instance()->plugin_url.'/js/metabox.js',array('jquery'),NULL,true);
			}
		},10,1);
		*/

		// make sure our box gets added
		add_action('add_meta_boxes',function() {
			add_meta_box($this->enchufe()->metabox->box_id,'DM Blog Custom',array($this->enchufe()->metabox,'display_box'),'post','side');
		});

		// make sure our metadata is saved when we save a post
		add_action('save_post',array($this,'save_metadata'));
	}

	// callback to display the box contents
	public function display_box($post)
	{
		// use nonce for verification
		wp_nonce_field($this->nonce_action,$this->nonce_name);

		// get our existing metadata
		$article	= Article::factory($post);
		$metadata	= $article->metadata();
		
		// create an array of the fields we want to send our template
		$data		= array('article' => $article, 'metadata' => $metadata, 'box_id' => $this->box_id);

		// determine the status of our checkboxes and add them to our template
		$data['disclaimer_status']	= (strpos($metadata->classes(), 'show-disclaimer') !== false) ? ' checked="checked"' : '';
/* 		$data['narrow_status']		= (strpos($metadata->classes(), 'narrow') !== false) ? ' checked="checked"' : ''; */
		$data['tweet_status']		= (strpos($metadata->classes(), 'has-tweet') !== false) ? ' checked="checked"' : '';
		$data['featured']			= ($article->post_meta($this->featured_meta)) ? ' checked="checked"' : '';

		// include the template with the actual fields to display
		$output		= Assets::template('admin/metaboxes',$data);

		echo $output;
	}

	// callback to save our metadata
	public function save_metadata($post_id)
	{
		// check to see if we should save this metadata or not
		if (! $this->verify_save($post_id)) { return; }

		// get our post as an article object
		global $post; $article	= Article::factory($post);

		// if our featured box is now blank
		if (! isset($_POST[$this->box_id.'_featured']))
		{
			// delete our featured post meta
			delete_post_meta($article->ID(),$this->featured_meta);
		} else {
			// otherwise, update/add it to the postmeta table
			$article->post_meta($this->featured_meta,1);
		}

		// break apart our coordinates into an array
		$coordinates = explode(',',$_POST[$this->box_id.'_coordinates']);

		// if our geodata is now blank
		if (empty($_POST[$this->box_id.'_coordinates']) OR count($coordinates) < 2)
		{
			// delete all geo fields
			delete_post_meta($article->ID(),'geo_latitude');
			delete_post_meta($article->ID(),'geo_longitude');
			delete_post_meta($article->ID(),'geo_address');
		} else {
			// and now update/add it to the postmeta table
			$article
				->post_meta('geo_latitude',$coordinates[0])
				->post_meta('geo_longitude',$coordinates[1])
				->post_meta('geo_address',$_POST[$this->box_id.'_address']);
		}

		// get our existing metadata
		$metadata	= $article->metadata();

		// if this is a published post without an article id…
		if($_POST['post_status'] == 'publish' AND empty($metadata->article_id()))
		{
			// get the next available article id
			$article_id	= $this->enchufe()->shortlinks->get_next_id();
			$metadata->article_id($article_id);
		}

		// construct our classes based on the array returned
		$classes	= (isset($_POST[$this->box_id.'_classes'])) ? implode(' ',$_POST[$this->box_id.'_classes']) : '';

		// set the new values of our metadata and save it
		$metadata
			->classes(trim($classes))
			->timezone(trim($_POST[$this->box_id.'_timezone']))
			->redirect_url(trim($_POST[$this->box_id.'_redirect_url']));
		$article->metadata($metadata);

		// get our tweet id
		$tweet_id	= trim($_POST[$this->box_id.'_tweet']);

		// if this is a published post with an empty twitter ID…
		if ($_POST['post_status'] == 'publish' AND empty($tweet_id))
		{
			// $tweet_id	= $metadata->create_tweet($article);
		}
	}

	// check if we should save the metadata or not
	private function verify_save($post_id)
	{
		// verify if this is an auto save routine. If it is, our form has not been submitted, so we don’t want to do anything
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return false; }

		// verify this came from the our screen and with proper authorization, because save_post can be triggered at other times
		if (! isset($_POST[$this->nonce_name]) OR ! wp_verify_nonce($_POST[$this->nonce_name],$this->nonce_action)) { return false; }

		// check if the current user has permission to edit the post
		if (! current_user_can('edit_post',$post_id)) { return false; }

		// since everything else passed, we can return true now
		return true;
	}
}

/**
 * End of file metabox.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/metabox.php
 **/