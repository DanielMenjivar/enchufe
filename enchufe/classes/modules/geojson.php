<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class to generate our map data in geojson format
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class GeoJson extends Base_JsonFeed {
	// the priority at which to generate our json file after modifying a post
	public $priority = 99997;

	// get our posts and convert them into json format
	public function generate_json()
	{
		// get all the posts that have geo data
		$args	= array(
					'numberposts'	=> 99999,
					'meta_key'		=> 'geo_longitude',
					'orderby'		=> array(
						'meta_value_num'	=> 'ASC',
						'date'				=> 'DESC'),
					'meta_query'	=> array(
						array('key'	=> 'geo_longitude'),
						array('key'	=> 'geo_latitude')
					)
				);
		$posts	= get_posts($args);

		// process our posts to group them by coordinates
		$features = array();
		foreach ($posts as $post)
		{
			// let’s use our article object to get some values
			$article	= Article::factory($post);
			$id			= $article->ID();
			$latitude	= $article->post_meta('geo_latitude');
			$longitude	= $article->post_meta('geo_longitude');
			$address	= $article->post_meta('geo_address');
			$key		= $longitude.', '.$latitude;

			// append the post to our posts array
			if (! array_key_exists($key, $features))
			{
				$features[$key] = new StdClass;
			}

			// if we have a review here, then we should use that category and not let it be overridden
			$features[$key]->category				= (isset($features[$key]->category) AND $features[$key]->category == 'reviews') ? 'reviews' : $article->category();
			// only set the place name the first time (to use the most recent post’s name for the place, don’t replace it with older post’s name
			$features[$key]->place_name				= (isset($features[$key]->place_name)) ? $features[$key]->place_name : $address;
			$features[$key]->posts[$id]				= new StdClass;
			$features[$key]->posts[$id]->id			= $id;
			$features[$key]->posts[$id]->title		= $article->post_title();
			$features[$key]->posts[$id]->date		= $article->post_date()->format('F j, Y');
			$features[$key]->posts[$id]->thumbnail	= $article->thumbnail();
			$features[$key]->posts[$id]->permalink	= $article->permalink();
		}

		// now create our actual geojson file based on these features
		$geo_json			= new StdClass;
		$geo_json->type		= "FeatureCollection";
		$geo_json->features = array();
		$index				= 0;

		foreach ($features as $coordinates => $feature)
		{
			// our reviews will have a different color marker and a different icon too… (we only ever have one posts per location on reviews)
			$marker_color	= ($feature->category == 'reviews') ? '#00bcce'		: '#0088cc';
			$marker_symbol	= ($feature->category == 'reviews') ? 'restaurant'	: 'star';
			$geo_json->features[$index]									= new StdClass;
			$geo_json->features[$index]->type							= 'Feature';
			$geo_json->features[$index]->geometry						= new StdClass;
			$geo_json->features[$index]->geometry->type					= 'Point';
			$geo_json->features[$index]->geometry->coordinates			= explode(', ',$coordinates);
			$geo_json->features[$index]->properties						= new StdClass;
			$geo_json->features[$index]->properties->{'marker-color'}	= $marker_color;
			$geo_json->features[$index]->properties->{'marker-symbol'}	= $marker_symbol;
			$geo_json->features[$index]->properties->title				= $feature->place_name;
			$geo_json->features[$index]->properties->posts				= array_values($feature->posts);
			$index++;
		}

		return $geo_json;
	}
}

/**
 * End of file geojson.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/geojson.php
 **/