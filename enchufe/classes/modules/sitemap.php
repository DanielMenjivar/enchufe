<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * A class for creating a sitemap.xml file
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class sitemap extends Base_Singleton {
	// store a reference to wpdb
	private $wpdb;

	// the priority at which to generate our sitemap after modifying a post
	public $priority = 999999;

	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// make wpdb available to this class
		global $wpdb;
		$this->wpdb		= $wpdb;

		if ($this->enchufe()->is_backend())
		{
			// make sure our sitemap is generated everytime we modify a post
			foreach (array('delete_post','publish_post') as $hook)
			{
				add_action($hook,array($this,'generate_sitemap'),$this->priority);
			}
		}
	}

	// use our own template for the RSS feed
	public function generate_sitemap()
	{
		// get our list of posts, plus the main/home/index page for the blog
		$posts		= $this->wpdb->get_results("SELECT null as slug, DATE_FORMAT(max(post_modified_gmt),'%Y-%m-%dT%T-00:00') as last_modified,'0.4' as priority,post_date_gmt as post_date FROM dm_posts UNION (SELECT post_name, DATE_FORMAT(post_modified_gmt,'%Y-%m-%dT%T-00:00'),IF(post_type = 'page','0.4','0.1'),post_date_gmt FROM dm_posts WHERE post_status = 'publish' AND (post_type = 'post' OR post_type = 'page') AND (post_name NOT IN ('about','archives','disclaimer','subscribe'))) ORDER BY priority DESC, post_date DESC, last_modified DESC");

		// load the template
		$data				= array('posts' => $posts);
		$sitemap			= Assets::template('sitemap',$data);
		// save our sitemap
		$filename			= '../sitemap.xml';
		$highest_modified	= $posts[0]->last_modified;
		file_put_contents($filename,$sitemap);
		touch($filename,$highest_modified);
	}

	// anything here is only run once when the plugin is initially activated
	public function activate()
	{ }

	// anything here is only run once when the plugin is deactivated
	public function deactivate()
	{ }
}

/**
 * End of file sitemap.php
 * Location: ./wp-content/plugins/enchufe/classes/modules/sitemap.php
 **/