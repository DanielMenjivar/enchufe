<?php if( ! defined('ABSPATH') && ! defined('WPINC')) { header('Location: /'); }

/**
 * The main class for our plugin.
 *
 * @package		dmblog.com
 * @author		Daniel Menjívar
 */

class Enchufe extends Base_Singleton {
	// we’ll use this prefix whenever we need one
	public $prefix = 'dm_';

	// the file path to our plugin
	public $plugin_path;

	// the URL path to our plugin
	public $plugin_url;

	// the URL path to our uploads
	public $upload_url_path;

	// the name of our blog
	public $blog_name;

	// lowercase/formatted name of our blog
	public $blog_name_lower;

	// the home URL of our blog
	public $home_url;

	// is this a backend request? - use the is_backend() method to find out
	private $is_backend;

	// is this an AJAX request? - use the is_ajax() method to find out
	private $is_ajax;

	// is this a mobile request? - use the is_mobile() method to find out
	private $is_mobile;

	// the names of all our modules (but the actual instances are available as individual properties ($this->cache for example)
	private $modules = array();

	// initialize our plugin, this is called in get_instance()
	public function initialize()
	{
		// set the paths to our plugin – we use dirname to get our parent directory since we’re inside the classes folder
		$this->plugin_path		= dirname(dirname(__FILE__));
		$this->plugin_url		= plugins_url('',dirname(__FILE__));

		// set these other variables too so we can use them anywhere
		$this->upload_url_path	= get_option('upload_url_path');
		$this->blog_name		= esc_attr(get_bloginfo('name','display'));
		$this->blog_name_lower	= str_replace(' ','',strtolower($this->blog_name));
		$this->home_url			= home_url('/');

		// when we activate our plugin, the activate method should run, same for deactivating
		add_action('activate_enchufe/enchufe.php',array($this,'activate'));
		add_action('deactivate_enchufe/enchufe.php',array($this,'deactivate'));

		// hook into WordPress to make sure our admin notices get displayed
		add_action('admin_notices',array($this,'register_notices'));

		// get all of our modules and store an instance of them, as well as their names
		$filenames = glob($this->plugin_path.'/classes/modules/*.php');

		foreach ($filenames as $file)
		{
			// just get the name of our module
			$module					= strtolower(basename($file,'.php'));

			// add this module’s name to our array
			$this->modules[]	= $module;

			// store an instance of this module
			$this->$module		= $module::get_instance();
		}

		// if this is a request to do a real cron job, then just do that
		if (isset($_GET['do_real_cron']))
		{
			$this->do_cron();
		}
	}

	// this is run only once when the plugin is initially activated
	public function activate()
	{
		// run the activate method of our modules
		foreach ($this->modules as $module)
		{
			$this->$module->activate();
		}
	}

	// this is run only once when the plugin is deactivated
	public function deactivate()
	{
		// run the deactivate method of our modules
		foreach ($this->modules as $module)
		{
			$this->$module->deactivate();
		}
	}

	// determine if this is a backend request or not
	public function is_backend()
	{
		// only run our check if we haven’t already
		if ($this->is_backend === NULL)
		{
			// just use WordPress’ check for now, we can always refine this later if we need to
			$this->is_backend = is_admin();
		}

		return $this->is_backend;
	}

	// determine whether this is an ajax request or not
	public function is_ajax()
	{
		// only run our check if we haven’t already
		if ($this->is_ajax === NULL)
		{
			$this->is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
		}

		return $this->is_ajax;
	}

	// determine whether this is a mobile request or not
	public function is_mobile()
	{
		// only run our check if we haven’t already
		if ($this->is_mobile === NULL)
		{
			// load our config file and assign the return value to $mobile_agents
			$mobile_agents	= Assets::configs('mobile_agents');

			$user_agent		= $_SERVER['HTTP_USER_AGENT'];

			// loop through our mobile agents and see if we have a match
			foreach ($mobile_agents as $mobile_agent)
			{
				if (preg_match("#$mobile_agent#i", $user_agent))
				{
					$this->is_mobile = true;
					return true; // stop looping as soon as we get a match
				} else {
					$this->is_mobile = false;
				}
			}
		}

		return $this->is_mobile;
	}

	// add an admin notice to the dashboard
	public function admin_notice($message, $success = true)
	{
		// get our admin notices from cache, returns NULL if none found
		$admin_notices = $this->cache->cache('admin.notices');

		// if we don’t have any messages, create a new array
		$admin_notices = (empty($admin_notices)) ? array() : $admin_notices;

		// create a new notice and add it to our array
		$notice				= new StdClass;
		$notice->type		= ($success) ? 'updated' : 'error';
		$notice->message	= $message;
		$admin_notices[]	= $notice;

		// save our notices to cache
		$this->cache->cache('admin.notices',$admin_notices);

		return $this; // allow method chaining
	}

	// register our admin notices with WordPress (add them)
	public function register_notices()
	{
		// get our admin notices and create an empty string
		$admin_notices = $this->cache->cache('admin.notices');
		$all_messages = '';

		// if we don’t have any messages, just quit
		if (empty($admin_notices)) { return; }

		// loop through our messages
		foreach ($admin_notices as $notice)
		{
			// get all of our notices combined into a single string
			$all_messages	.= '<div class="'.$notice->type.'">'.wpautop($notice->message).'</div>';
		}

		// delete our messages from cache now
		$this->cache->delete('admin.notices');

		echo $all_messages;
	}

	// do a real cron job
	public function do_cron()
	{
		// wget -qO- https://example.com/blog/?do_real_cron=1 >/dev/null 2>&1

		if ($_SERVER['REMOTE_ADDR'] == '0.0.0.0')
		{
			// get our list of post ids remaining
			$post_ids	= $this->enchufe()->cache->cache('post_ids');

			// get our current post
			$post_id	= reset($post_ids);
			$post		= get_post($post_id);
			$article	= Article::factory($post);

			// try creating a new tweet
			$success	= $article->metadata()->create_tweet($article);

			if ($success)
			{
				// delete the post from the list
				if(($key = array_search($post_id, $post_ids)) !== false)
				{
					unset($post_ids[$key]);

					// re-save the list of post_ids
					$success	= $this->cache->cache('post_ids',$post_ids);
				}
			}
			exit;
		}
	}
}

/**
 * End of file enchufe.php
 * Location: ./wp-content/plugins/enchufe/classes/enchufe.php
 **/